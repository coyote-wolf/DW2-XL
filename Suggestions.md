## Population Management Suggestions
Seems like for population management, really, you need two things:

Policy for each race:
* Integrate
* Deport
* Enslave
* Devour / Exterminate

Policy for colonization:
* Stringent (Best tier race only)
* Moderate (Best two tiers of races only)
* Loose (Any race that is +20 PQ and above)

So, for Stringent and Moderate, tiers are assigned as:
> Within my empire, races are grouped by best affinities for a planet type (with ties counting as one tier)

So, if I have two races that both have +10 for a world type, and that's the best in the empire, then both of those races count as "best tier"

If I have two additional races that have +6 for that same world type, then for "Moderate" - which is "top two racial groups" - I'd get all 4 of those as "Best two tiers of races only"

If I only had one best race, and one second best race, then I'd only allow those two to settle there (each one would represent the top two tiers).

The automation would manage which races these were, and making sure that over time, only appropriate races settled there or were removed / replaced when better ones joined the empire.

At individual worlds, you could simply have a toggle: automated management of races, on/off.  (if off, you could offer the horrible old interface for those who scream they wish they still had it).

This is ONLY for races marked for integration.  All other races would ignore colonization policies as they simply don't apply.

For races set to deport - obviously they're to be deported.  Enslaved races aren't to be moved, but merely enslaved in situ, and devoured in situ.

Really, this is would handle everything, and I imagine it would be quite simple to have racial policies for this relative to other races.

It could be a pain to scale this to a very large number of races - but that's fundamental to any design...