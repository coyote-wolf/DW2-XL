using Stride.Engine;

namespace XL
{
    class XLApp
    {
        static void Main(string[] args)
        {
            using (var game = new Game())
            {
                game.Run();
            }
        }
    }
}
