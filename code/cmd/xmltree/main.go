package main

// trivial test harness for our custom xmltree lib
// simply want to read & write without scrambling the file or losing comments

import (
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"

	"lucky-wolf/DW2-XL/code/cmd/xmltree/algorithm"
)

func main() {

	var err error
	var function, folder string
	var scale float64
	var scope string
	var ids string

	flag.StringVar(&function, "algorithm", "", "algorithm to apply")
	flag.StringVar(&folder, "folder", "XL", "folder to apply changes to")
	flag.BoolVar(&algorithm.Quiet, "quiet", false, "set if you don't want debug output")
	flag.Float64Var(&scale, "scale", 1.0, "scale factor to apply")
	flag.StringVar(&scope, "scope", "all", "scope to apply changes to (Star, Planet, unspecified means both)")
	flag.StringVar(&ids, "ids", "7,8,9,10,11,12,17,18,19,20,21,22,23,27,29,30", "comma-separated list of Orb Type IDs to apply changes to")
	flag.Parse()

	if !algorithm.Quiet {
		cwd, err := os.Getwd()
		if err != nil {
			panic(err)
		}
		fmt.Println(fmt.Sprintf("Active Path=%s", cwd))
	}

	var algos = []struct {
		name string
		desc string
	}{
		{"All", "Runs (all) Components, ResearchCosts, Hulls, and Troops"},
		{"Components", "Runs all component algorithms"},
		{"  Engines", "Updates engine components to XL data table"},
		{"  HyperDrives", "Updates hyperdrive components to have 7 levels off of a common data table"},
		{"  IonShields", "Updates ion shield components off of a common data table (ship & ftr)"},
		{"  BeamWeapons", "Updates beam weapon components off of a common core data table"},
		{"  BlasterWeapons", "Updates blaster weapon components off of a common core data table"},
		{"  BombardWeapons", "Updates bombard weapon components off of a common core data table"},
		{"  GraviticWeapons", "Updates gravitic weapon components off of a common core data table"},
		{"  IonWeapons", "Updates ion weapon components off of a common core data table"},
		{"  KineticWeapons", "Updates kinetic weapon components off of a common core data table"},
		{"  MissileWeapons", "Updates missile weapon components off of a common core data table"},
		{"  TorpedoWeapons", "Updates torpedo weapon components off of a common core data table"},
		{"  DamageControl", "Updates damage control components off of a common core data table"},
		{"  FighterArmor", "Fighter armor components are derived from ship armors"},
		{"  FighterEngines", "Fighter engine components are derived from ship engines"},
		{"  FighterReactors", "Fighter reactor components are derived from ship reactors"},
		{"  FighterShields", "Fighter shield components are derived from ship shields"},
		{"  FighterWeaponsAndPD", "[F/B] and [PD] weapon components are derived from ship weapons"},
		{"ResearchCosts", "All Research costs are set to conform to their columnar position (with a few exceptions)"},
		{"HangarBays", "All ship hangarbay slots will be size-limited by role"},
		{"FighterHulls", "All strikecraft component slots will be adjusted to match desired schedule"},
		{"ShipHulls", "All ships will be adjusted to match desired schedule"},
		{"Troops", "All troops will be generated from standard data"},
		{"ScalePlanetFrequencies", "Planet frequencies will be scaled by your input"},
		{"PartialOrdering", "All xml objects will have their ID and name and a few other fields placed first"},
		{"RenumberHullComponentBays", "All component bay indexes will be fixed to a simple incremental index"},
		{"ExtractShipComponentBays", "Extracts component bay counts and basic hull data to csv files by the same names in temp folder"},
	}

	switch function {
	case "":
		flag.PrintDefaults()
		fmt.Println("Possible algorithms include:")
		for _, v := range algos {
			fmt.Println(fmt.Sprintf("  %-30s%s", v.name, v.desc))
		}
	case "All":
		err = algorithm.All(folder)
	case "Components":
		err = algorithm.Components(folder)
	case "FighterArmor":
		err = algorithm.FighterArmor(folder)
	case "FighterEngines":
		err = algorithm.FighterEngines(folder)
	case "FighterHulls":
		err = algorithm.FighterHulls(folder)
	case "FighterReactors":
		err = algorithm.FighterReactors(folder)
	case "FighterShields":
		err = algorithm.FighterShields(folder)
	case "FighterWeaponsAndPD":
		err = algorithm.FighterWeaponsAndPD(folder)
	case "HangarBays":
		err = algorithm.HangarBays(folder)
	case "Shields":
		err = algorithm.Shields(folder)
	case "Armors":
		err = algorithm.Armors(folder)
	case "Engines":
		err = algorithm.Engines(folder)
	case "Reactors":
		err = algorithm.Reactors(folder)
	case "HyperDrives":
		err = algorithm.HyperDrives(folder)
	case "IonShields":
		err = algorithm.IonShields(folder)
	case "BeamWeapons":
		err = algorithm.BeamWeapons(folder)
	case "BlasterWeapons":
		err = algorithm.BlasterWeapons(folder)
	case "BombardWeapons":
		err = algorithm.BombardWeapons(folder)
	case "GraviticWeapons":
		err = algorithm.GraviticWeapons(folder)
	case "IonWeapons":
		err = algorithm.IonWeapons(folder)
	case "KineticWeapons":
		err = algorithm.KineticWeapons(folder)
	case "MissileWeapons":
		err = algorithm.MissileWeapons(folder)
	case "TorpedoWeapons":
		err = algorithm.TorpedoWeapons(folder)
	case "DamageControl":
		err = algorithm.DamageControl(folder)
	case "ScalePlanetFrequencies":
		err = algorithm.ScalePlanetFrequencies(folder, scale, scope, CsvToIntSlice(ids))
	case "ResearchCosts":
		err = algorithm.ResearchCosts(folder)
	case "PartialOrdering":
		err = algorithm.PartialOrdering(folder)
	case "RenumberHullComponentBays":
		err = algorithm.RenumberHullComponentBays(folder)
	case "ShipHulls":
		err = algorithm.ShipHulls(folder)
	case "ExtractShipComponentBays":
		err = algorithm.ExtractShipComponentBays(folder)
	case "UpdateAndExtractShipHulls":
		err = algorithm.UpdateAndExtractShipHulls(folder)
	case "Troops":
		err = algorithm.Troops(folder)
	default:
		err = fmt.Errorf("unknown algorithm: %s", function)
	}

	// make sure we report errors clearly
	if err != nil {
		fmt.Println("\nERROR:", err)
		os.Exit(1)
	}
}

func CsvToIntSlice(ids string) (result []int) {
	for _, id := range strings.Split(ids, ",") {
		i, err := strconv.Atoi(id)
		if err == nil {
			result = append(result, i)
		}
	}
	return
}
