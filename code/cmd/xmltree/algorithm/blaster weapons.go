package algorithm

func BlasterWeapons(folder string) (err error) {

	// load all component definition files
	j, err := LoadJobFor(folder, "ComponentDefinitions*.xml")
	if err != nil {
		return
	}

	// update blaster weapons
	err = j.applyBlasterWeapons()
	if err != nil {
		return
	}

	// update derivatives
	err = j.applyFighterWeaponsAndPD()
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

func (j *Job) applyBlasterWeapons() (err error) {

	// apply stats for each component
	err = j.ApplyComponentAll(BlasterWeaponData)

	return
}

const (
	// fast without being a total blur (not quite 1/2 of 'light speed')
	BlasterSpeed          = 2200
	BlasterVolleyFireRate = .5

	// fall off is steep for blasters (they're knife fighting weapons)
	BlasterFallOff          = .25
	ImpactBlasterFalloff    = .225
	TitanBlasterFalloff     = .2
	PhasicBlasterFalloff    = .2
	PlasmaBlasterFalloff    = .3
	PulseWaveBlasterFalloff = .2

	// blasters are the best base raw damage of any direct fire weapon
	// warn: we hand these factors to MakeDamageFunc() which normalizes them to the standard weapon damage (i.e. it compensates for the fire rate, removing that from the factor)
	// warn: so these are ABSOLUTE damage factors, irrespective of rate of fire
	BlasterBaseDamageFactor      = 1.33333333333333                // 1.33333 normalized
	ImpactBlasterDamageFactor    = 1.1 * BlasterBaseDamageFactor   // 1.46667
	TitanBlasterDamageFactor     = 1.1 * ImpactBlasterDamageFactor // 1.61333
	PhasicBlasterDamageFactor    = 1.0 * ImpactBlasterDamageFactor // 1.46667
	PlasmaBlasterDamageFactor    = 1.2 * BlasterBaseDamageFactor   // 1.6
	PulseWaveBlasterDamageFactor = 1.8

	// blasters are seriously energy-hungry (less than beams, atm)
	BlasterEnergyRatio          = 1.
	ImpactBlasterEnergyRatio    = .9
	TitanBlasterEnergyRatio     = .8
	PhasicBlasterEnergyRatio    = .8
	PlasmaBlasterEnergyRatio    = 1.
	PulseWaveBlasterEnergyRatio = .8

	// Blasters are bit on the faster side
	BlasterFireRate          = .8 * WeaponFireRateBasis
	ImpactBlasterFireRate    = 1.25 * BlasterFireRate
	TitanBlasterFireRate     = ImpactBlasterFireRate
	PhasicBlasterFireRate    = ImpactBlasterFireRate
	PlasmaBlasterFireRate    = BlasterFireRate
	PulseWaveBlasterFireRate = WeaponFireRateBasis

	PulseWaveBlasterVolleyAmount = 2

	// plasma interacts with armor
	// todo: ideally it would burn through armor faster, but not bypass it
	PlasmaBlasterShieldBypass = 0
	PlasmaBlasterArmorBypass  = 0.33333333333333

	// phasic interacts with shields and armor
	// ~11.1% of all damage penetrates, minus reactive armor, shields, and hull
	PhasicBlasterShieldBypass = 0.33333333333333
	PhasicBlasterArmorBypass  = 0.33333333333333
)

var (
	BlasterWeaponData = ComponentLevelDataMap{

		"Blaster, Particle [S]": {
			minLevel:       0,
			maxLevel:       1,
			componentStats: BlasterWeaponSmall,
		},

		"Blaster, Maxos [S]": {
			minLevel:       2,
			maxLevel:       4,
			componentStats: BlasterWeaponSmall,
		},
		"Blaster, Maxos [M]": {
			minLevel:       2,
			maxLevel:       4,
			componentStats: BlasterWeaponMedium,
		},

		"Blaster, Assault [S]": {
			minLevel:       5,
			maxLevel:       7,
			componentStats: ImpactBlasterWeaponSmall,
		},
		"Blaster, Assault [M]": {
			minLevel:       5,
			maxLevel:       7,
			componentStats: ImpactBlasterWeaponMedium,
		},
		"Blaster, Assault [L]": {
			minLevel:       5,
			maxLevel:       7,
			componentStats: ImpactBlasterWeaponLarge,
		},

		"Blaster, Titan [S]": {
			minLevel:       8,
			maxLevel:       10,
			componentStats: TitanBlasterWeaponSmall,
		},
		"Blaster, Titan [M]": {
			minLevel:       8,
			maxLevel:       10,
			componentStats: TitanBlasterWeaponMedium,
		},
		"Blaster, Titan [L]": {
			minLevel:       8,
			maxLevel:       10,
			componentStats: TitanBlasterWeaponLarge,
		},

		"Blaster, Phasic [S]": {
			minLevel:       8,
			maxLevel:       10,
			componentStats: PhasicBlasterWeaponSmall,
		},
		"Blaster, Phasic [M]": {
			minLevel:       8,
			maxLevel:       10,
			componentStats: PhasicBlasterWeaponMedium,
		},
		"Blaster, Phasic [L]": {
			minLevel:       8,
			maxLevel:       10,
			componentStats: PhasicBlasterWeaponLarge,
		},

		// Boskara
		"Blaster, Plasma Cannon [S]": {
			minLevel:       2,
			maxLevel:       10,
			componentStats: PlasmaBlasterWeaponSmall,
		},
		"Blaster, Plasma Cannon [M]": {
			minLevel:       2,
			maxLevel:       10,
			componentStats: PlasmaBlasterWeaponMedium,
		},
		"Blaster, Plasma Cannon [L]": {
			minLevel:       2,
			maxLevel:       10,
			componentStats: PlasmaBlasterWeaponLarge,
		},

		// Wekkarus
		"Blaster, Pulse Wave Cannon [S]": {
			minLevel:       1,
			maxLevel:       10,
			componentStats: PulseWaveBlasterWeaponSmall,
		},
		"Blaster, Pulse Wave Cannon [M]": {
			minLevel:       2,
			maxLevel:       10,
			componentStats: PulseWaveBlasterWeaponMedium,
		},
		"Blaster, Pulse Wave Cannon [L]": {
			minLevel:       3,
			maxLevel:       10,
			componentStats: PulseWaveBlasterWeaponLarge,
		},

		// Facilities
		"Massive Blaster Cannon Battery": {
			minLevel:       3,
			maxLevel:       9,
			componentStats: PlanetaryBlasterComponentStats,
		},
	}

	StandardBlasterWeaponFallOff       = MakeFixedLevelFunc(BlasterFallOff)
	StandardBlasterWeaponSpeed         = MakeFixedLevelFunc(BlasterSpeed)
	StandardBlasterWeaponRange         = MakeExpLevelFunc(800, WeaponDamageIncreaseExp)
	StandardBlasterWeaponDamage        = MakeDamageFunc(BlasterFireRate, BlasterBaseDamageFactor)
	StandardBlasterWeaponEnergyPerShot = MakeEnergyPerShotFunc(BlasterEnergyRatio, StandardBlasterWeaponDamage)
	StandardBlasterWeaponFireRate      = MakeFixedLevelFunc(BlasterFireRate)

	// warn: blasters do NOT have bombard values because they're always just repeating small weapons

	BlasterWeaponSmall = ComposeComponentStats(
		SmallWeaponBaseStats,
		WeaponNoAOE,
		WeaponNoBombard,
		WeaponNoIon,
		ComponentStats{
			"ComponentCountermeasuresBonus": DirectFireComponentCountermeasuresBonus,
			"ComponentTargetingBonus":       MakeFixedLevelFunc(0),
			"CrewRequirement":               SmallCrewRequirements,
			"WeaponDamageFalloffRatio":      StandardBlasterWeaponFallOff,
			"WeaponArmorBypass":             MakeFixedLevelFunc(0),
			"WeaponShieldBypass":            MakeFixedLevelFunc(0),
			"WeaponRange":                   StandardBlasterWeaponRange,
			"WeaponSpeed":                   StandardBlasterWeaponSpeed,
			"WeaponFireRate":                StandardBlasterWeaponFireRate,
			"WeaponEnergyPerShot":           StandardBlasterWeaponEnergyPerShot,
			"WeaponRawDamage":               StandardBlasterWeaponDamage,
			"WeaponVolleyAmount":            MakeFixedLevelFunc(1),
			"WeaponVolleyFireRate":          MakeFixedLevelFunc(0),
		},
	)
	BlasterWeaponMedium = DeriveMediumWeaponUsing2xVolleyRate(BlasterWeaponSmall, BlasterVolleyFireRate)
	BlasterWeaponLarge  = DeriveLargeWeaponUsing4xVolleyRate(BlasterWeaponSmall, BlasterVolleyFireRate)

	// Assault are a 20% improvement
	ImpactBlasterFalloffRatio        = MakeFixedLevelFunc(ImpactBlasterFalloff)
	ImpactBlasterWeaponDamage        = MakeDamageFunc(ImpactBlasterFireRate, ImpactBlasterDamageFactor)
	ImpactBlasterWeaponEnergyPerShot = MakeEnergyPerShotFunc(ImpactBlasterEnergyRatio, ImpactBlasterWeaponDamage)
	ImpactBlasterWeaponRange         = StandardBlasterWeaponRange // it's already compounding
	ImpactBlasterWeaponROF           = MakeFixedLevelFunc(ImpactBlasterFireRate)

	ImpactBlasterWeaponSmall = ComposeComponentStats(
		BlasterWeaponSmall,
		ComponentStats{
			"WeaponDamageFalloffRatio": ImpactBlasterFalloffRatio,
			"WeaponRange":              ImpactBlasterWeaponRange,
			"WeaponFireRate":           ImpactBlasterWeaponROF,
			"WeaponEnergyPerShot":      ImpactBlasterWeaponEnergyPerShot,
			"WeaponRawDamage":          ImpactBlasterWeaponDamage,
		},
	)
	ImpactBlasterWeaponMedium = DeriveMediumWeaponUsing2xVolleyRate(ImpactBlasterWeaponSmall, BlasterVolleyFireRate)
	ImpactBlasterWeaponLarge  = DeriveLargeWeaponUsing4xVolleyRate(ImpactBlasterWeaponSmall, BlasterVolleyFireRate)

	// Titan Assault are a 20% improvement
	TitanBlasterFalloffRatio        = MakeFixedLevelFunc(TitanBlasterFalloff)
	TitanBlasterWeaponDamage        = MakeDamageFunc(TitanBlasterFireRate, TitanBlasterDamageFactor)
	TitanBlasterWeaponEnergyPerShot = MakeEnergyPerShotFunc(TitanBlasterEnergyRatio, TitanBlasterWeaponDamage)
	TitanBlasterWeaponRange         = StandardBlasterWeaponRange // it's already compounding
	TitanBlasterWeaponROF           = MakeFixedLevelFunc(TitanBlasterFireRate)

	TitanBlasterWeaponSmall = ComposeComponentStats(
		BlasterWeaponSmall,
		ComponentStats{
			"WeaponDamageFalloffRatio": TitanBlasterFalloffRatio,
			"WeaponRange":              TitanBlasterWeaponRange,
			"WeaponFireRate":           TitanBlasterWeaponROF,
			"WeaponEnergyPerShot":      TitanBlasterWeaponEnergyPerShot,
			"WeaponRawDamage":          TitanBlasterWeaponDamage,
		},
	)
	TitanBlasterWeaponMedium = DeriveMediumWeaponUsing2xVolleyRate(TitanBlasterWeaponSmall, BlasterVolleyFireRate)
	TitanBlasterWeaponLarge  = DeriveLargeWeaponUsing4xVolleyRate(TitanBlasterWeaponSmall, BlasterVolleyFireRate)

	// Phasic Assault are a 20% improvement
	PhasicBlasterFalloffRatio        = MakeFixedLevelFunc(PhasicBlasterFalloff)
	PhasicBlasterWeaponDamage        = MakeDamageFunc(PhasicBlasterFireRate, PhasicBlasterDamageFactor)
	PhasicBlasterWeaponEnergyPerShot = MakeEnergyPerShotFunc(PhasicBlasterEnergyRatio, PhasicBlasterWeaponDamage)
	PhasicBlasterWeaponRange         = StandardBlasterWeaponRange // it's already compounding
	PhasicBlasterWeaponROF           = MakeFixedLevelFunc(PhasicBlasterFireRate)

	PhasicBlasterWeaponSmall = ComposeComponentStats(
		BlasterWeaponSmall,
		ComponentStats{
			"WeaponDamageFalloffRatio": PhasicBlasterFalloffRatio,
			"WeaponRange":              PhasicBlasterWeaponRange,
			"WeaponFireRate":           PhasicBlasterWeaponROF,
			"WeaponEnergyPerShot":      PhasicBlasterWeaponEnergyPerShot,
			"WeaponRawDamage":          PhasicBlasterWeaponDamage,
			"WeaponShieldBypass":       MakeFixedLevelFunc(PhasicBlasterShieldBypass),
			"WeaponArmorBypass":        MakeFixedLevelFunc(PhasicBlasterArmorBypass),
		},
	)
	PhasicBlasterWeaponMedium = DeriveMediumWeaponUsing2xVolleyRate(PhasicBlasterWeaponSmall, BlasterVolleyFireRate)
	PhasicBlasterWeaponLarge  = DeriveLargeWeaponUsing4xVolleyRate(PhasicBlasterWeaponSmall, BlasterVolleyFireRate)

	// Boskara Blaster, Plasma Cannons
	PlasmaBlasterFalloffRatio        = MakeFixedLevelFunc(PlasmaBlasterFalloff)
	PlasmaBlasterWeaponDamage        = MakeDamageFunc(PlasmaBlasterFireRate, PlasmaBlasterDamageFactor)
	PlasmaBlasterWeaponEnergyPerShot = MakeEnergyPerShotFunc(PlasmaBlasterEnergyRatio, PlasmaBlasterWeaponDamage)
	PlasmaBlasterWeaponRange         = StandardBlasterWeaponRange // it's already compounding
	PlasmaBlasterWeaponROF           = MakeFixedLevelFunc(PlasmaBlasterFireRate)

	PlasmaBlasterWeaponSmall = ComposeComponentStats(
		BlasterWeaponSmall,
		ComponentStats{
			"WeaponDamageFalloffRatio": PlasmaBlasterFalloffRatio,
			"WeaponRange":              PlasmaBlasterWeaponRange,
			"WeaponFireRate":           PlasmaBlasterWeaponROF,
			"WeaponEnergyPerShot":      PlasmaBlasterWeaponEnergyPerShot,
			"WeaponRawDamage":          PlasmaBlasterWeaponDamage,
			"WeaponShieldBypass":       MakeFixedLevelFunc(PlasmaBlasterShieldBypass),
			"WeaponArmorBypass":        MakeFixedLevelFunc(PlasmaBlasterArmorBypass),
		},
	)
	PlasmaBlasterWeaponMedium = DeriveMediumWeaponUsing2xVolleyRate(PlasmaBlasterWeaponSmall, BlasterVolleyFireRate)
	PlasmaBlasterWeaponLarge  = DeriveLargeWeaponUsing4xVolleyRate(PlasmaBlasterWeaponSmall, BlasterVolleyFireRate)

	// Wekkarus Blaster, Pulse Wave Cannons
	PulseWaveBlasterFalloffRatio        = MakeFixedLevelFunc(PulseWaveBlasterFalloff)
	PulseWaveBlasterWeaponDamage        = MakeMultiSalvoWeaponDamageFunc(PulseWaveBlasterFireRate, PulseWaveBlasterVolleyAmount, PulseWaveBlasterDamageFactor)
	PulseWaveBlasterWeaponEnergyPerShot = MakeEnergyPerShotFunc(PulseWaveBlasterEnergyRatio, PulseWaveBlasterWeaponDamage)
	PulseWaveBlasterWeaponRange         = MakeScaledFuncLevelFunc(1.2, StandardBlasterWeaponRange)
	PulseWaveBlasterWeaponSpeed         = MakeScaledFuncLevelFunc(0.5, StandardBlasterWeaponSpeed)
	PulseWaveBlasterWeaponROF           = MakeFixedLevelFunc(PulseWaveBlasterFireRate)

	PulseWaveBlasterWeaponSmall = ComposeComponentStats(
		BlasterWeaponSmall,
		ComponentStats{
			"WeaponDamageFalloffRatio": PulseWaveBlasterFalloffRatio,
			"WeaponRange":              PulseWaveBlasterWeaponRange,
			"WeaponSpeed":              PulseWaveBlasterWeaponSpeed,
			"WeaponFireRate":           PulseWaveBlasterWeaponROF,
			"WeaponEnergyPerShot":      PulseWaveBlasterWeaponEnergyPerShot,
			"WeaponRawDamage":          PulseWaveBlasterWeaponDamage,
			"WeaponVolleyAmount":       MakeFixedLevelFunc(PulseWaveBlasterVolleyAmount),
			"WeaponVolleyFireRate":     MakeFixedLevelFunc(BlasterVolleyFireRate),
		},
	)
	PulseWaveBlasterWeaponMedium = DeriveMediumWeaponUsing2xDamage(PulseWaveBlasterWeaponSmall)
	PulseWaveBlasterWeaponLarge  = DeriveLargeWeaponUsing4xDamage(PulseWaveBlasterWeaponSmall)

	// we'll make these derivative of Assault blasters
	PlanetaryBlasterComponentStats = DerivePlanetaryFromLargeWeapon(ImpactBlasterWeaponLarge)
)
