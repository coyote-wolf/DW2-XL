package algorithm

// layer these in using ComposeComponentStats()
var (
	SmallWeaponBaseStats = ComponentStats{
		"CrewRequirement":  SmallCrewRequirements,
		"StaticEnergyUsed": SmallWeaponStaticEnergy,
	}
	MediumWeaponBaseStats = ComponentStats{
		"CrewRequirement":  MediumCrewRequirements,
		"StaticEnergyUsed": MediumWeaponStaticEnergy,
	}
	LargeWeaponBaseStats = ComponentStats{
		"CrewRequirement":  LargeCrewRequirements,
		"StaticEnergyUsed": LargeWeaponStaticEnergy,
	}
	PlanetWeaponBaseStats = ComponentStats{
		"CrewRequirement":  PlanetaryCrewRequirements,
		"StaticEnergyUsed": PlanetaryWeaponStaticEnergy,
	}

	WeaponNoAOE = ComponentStats{
		"WeaponAreaEffectRange":     MakeFixedLevelFunc(0),
		"WeaponAreaBlastWaveSpeed":  MakeFixedLevelFunc(0),
		"WeaponIonEngineDamage":     MakeFixedLevelFunc(0),
		"WeaponIonHyperDriveDamage": MakeFixedLevelFunc(0),
		"WeaponIonSensorDamage":     MakeFixedLevelFunc(0),
		"WeaponIonShieldDamage":     MakeFixedLevelFunc(0),
		"WeaponIonWeaponDamage":     MakeFixedLevelFunc(0),
		"WeaponIonGeneralDamage":    MakeFixedLevelFunc(0),
	}

	WeaponNoBombard = ComponentStats{
		"WeaponBombardDamageInfrastructure": MakeFixedLevelFunc(0),
		"WeaponBombardDamageMilitary":       MakeFixedLevelFunc(0),
		"WeaponBombardDamagePopulation":     MakeFixedLevelFunc(0),
		"WeaponBombardDamageQuality":        MakeFixedLevelFunc(0),
	}

	WeaponNoIon = ComponentStats{
		"WeaponIonEngineDamage":     MakeFixedLevelFunc(0),
		"WeaponIonHyperDriveDamage": MakeFixedLevelFunc(0),
		"WeaponIonSensorDamage":     MakeFixedLevelFunc(0),
		"WeaponIonShieldDamage":     MakeFixedLevelFunc(0),
		"WeaponIonWeaponDamage":     MakeFixedLevelFunc(0),
		"WeaponIonGeneralDamage":    MakeFixedLevelFunc(0),
	}
)

// simply derive a medium from by doubling the volley size
func DeriveMediumWeaponUsing2xVolley(base ComponentStats) ComponentStats {

	if base["WeaponVolleyFireRate"] == nil {
		panic("must have a volley rate")
	}

	return ComposeComponentStats(
		base,
		MediumWeaponBaseStats,
		ComponentStats{
			"WeaponVolleyAmount": MakeScaledFuncLevelFunc(2, base["WeaponVolleyAmount"]),
		},
	)
}

// simply derive a large from by quadrupling the volley size
func DeriveLargeWeaponUsing4xVolley(base ComponentStats) ComponentStats {

	if base["WeaponVolleyFireRate"] == nil {
		panic("must have a volley rate")
	}

	return ComposeComponentStats(
		base,
		LargeWeaponBaseStats,
		ComponentStats{
			"WeaponVolleyAmount": MakeScaledFuncLevelFunc(4, base["WeaponVolleyAmount"]),
		},
	)
}

// simply derive a medium from by doubling the volley size using the given volley fire rate
func DeriveMediumWeaponUsing2xVolleyRate(base ComponentStats, volleyFireRate float64) ComponentStats {
	return ComposeComponentStats(
		base,
		MediumWeaponBaseStats,
		ComponentStats{
			"WeaponVolleyAmount":   MakeScaledFuncLevelFunc(2, base["WeaponVolleyAmount"]),
			"WeaponVolleyFireRate": MakeFixedLevelFunc(volleyFireRate),
		},
	)
}

// simply derive a medium from by doubling the volley size using the given volley fire rate
func DeriveLargeWeaponUsing4xVolleyRate(base ComponentStats, volleyFireRate float64) ComponentStats {
	return ComposeComponentStats(
		base,
		LargeWeaponBaseStats,
		ComponentStats{
			"WeaponVolleyAmount":   MakeScaledFuncLevelFunc(4, base["WeaponVolleyAmount"]),
			"WeaponVolleyFireRate": MakeFixedLevelFunc(volleyFireRate),
		},
	)
}

// simply derive a medium by doubling the damage (and ergo, energy use) of the given base stats
func DeriveMediumWeaponUsing2xDamage(base ComponentStats) ComponentStats {
	return ComposeComponentStats(
		base,
		MediumWeaponBaseStats,
		ComponentStats{
			"WeaponBombardDamageInfrastructure": MakeScaledFuncLevelFunc(2, base["WeaponBombardDamageInfrastructure"]),
			"WeaponBombardDamageMilitary":       MakeScaledFuncLevelFunc(2, base["WeaponBombardDamageMilitary"]),
			"WeaponBombardDamagePopulation":     MakeScaledFuncLevelFunc(2, base["WeaponBombardDamagePopulation"]),
			"WeaponBombardDamageQuality":        MakeScaledFuncLevelFunc(2, base["WeaponBombardDamageQuality"]),
			"WeaponEnergyPerShot":               MakeScaledFuncLevelFunc(2, base["WeaponEnergyPerShot"]),
			"WeaponRawDamage":                   MakeScaledFuncLevelFunc(2, base["WeaponRawDamage"]),
		},
	)
}

// simply derive a large by double the damage (and ergo, energy use) of the given base stats (assumes base == medium)
func DeriveLargeWeaponUsing2xDamage(base ComponentStats) ComponentStats {
	return ComposeComponentStats(
		base,
		LargeWeaponBaseStats,
		ComponentStats{
			"WeaponBombardDamageInfrastructure": MakeScaledFuncLevelFunc(2, base["WeaponBombardDamageInfrastructure"]),
			"WeaponBombardDamageMilitary":       MakeScaledFuncLevelFunc(2, base["WeaponBombardDamageMilitary"]),
			"WeaponBombardDamagePopulation":     MakeScaledFuncLevelFunc(2, base["WeaponBombardDamagePopulation"]),
			"WeaponBombardDamageQuality":        MakeScaledFuncLevelFunc(2, base["WeaponBombardDamageQuality"]),
			"WeaponEnergyPerShot":               MakeScaledFuncLevelFunc(2, base["WeaponEnergyPerShot"]),
			"WeaponRawDamage":                   MakeScaledFuncLevelFunc(2, base["WeaponRawDamage"]),
		},
	)
}

// simply derive a large by quadrupling the damage (and ergo, energy use) of the given base stats
func DeriveLargeWeaponUsing4xDamage(base ComponentStats) ComponentStats {
	return ComposeComponentStats(
		base,
		LargeWeaponBaseStats,
		ComponentStats{
			"WeaponBombardDamageInfrastructure": MakeScaledFuncLevelFunc(4, base["WeaponBombardDamageInfrastructure"]),
			"WeaponBombardDamageMilitary":       MakeScaledFuncLevelFunc(4, base["WeaponBombardDamageMilitary"]),
			"WeaponBombardDamagePopulation":     MakeScaledFuncLevelFunc(4, base["WeaponBombardDamagePopulation"]),
			"WeaponBombardDamageQuality":        MakeScaledFuncLevelFunc(4, base["WeaponBombardDamageQuality"]),
			"WeaponEnergyPerShot":               MakeScaledFuncLevelFunc(4, base["WeaponEnergyPerShot"]),
			"WeaponRawDamage":                   MakeScaledFuncLevelFunc(4, base["WeaponRawDamage"]),
		},
	)
}

// simply derive a large by quadrupling the damage (and ergo, energy use) of the given base stats
func DeriveLargeWeaponUsingDamageMultiplier(base ComponentStats, multiplier float64) ComponentStats {
	return ComposeComponentStats(
		base,
		LargeWeaponBaseStats,
		ComponentStats{
			"WeaponBombardDamageInfrastructure": MakeScaledFuncLevelFunc(multiplier, base["WeaponBombardDamageInfrastructure"]),
			"WeaponBombardDamageMilitary":       MakeScaledFuncLevelFunc(multiplier, base["WeaponBombardDamageMilitary"]),
			"WeaponBombardDamagePopulation":     MakeScaledFuncLevelFunc(multiplier, base["WeaponBombardDamagePopulation"]),
			"WeaponBombardDamageQuality":        MakeScaledFuncLevelFunc(multiplier, base["WeaponBombardDamageQuality"]),
			"WeaponEnergyPerShot":               MakeScaledFuncLevelFunc(multiplier, base["WeaponEnergyPerShot"]),
			"WeaponRawDamage":                   MakeScaledFuncLevelFunc(multiplier, base["WeaponRawDamage"]),
		},
	)
}

// planetary need to have substantial range (minimum of say 5K)
// damage has to be substantial enough to make them make sense
const PlanetaryScalingFactor = 5.0

func DerivePlanetaryFromLargeWeapon(basis ComponentStats) ComponentStats {
	return ComposeComponentStats(
		basis,
		PlanetWeaponBaseStats,
		ComponentStats{
			"ComponentCountermeasuresBonus": MakeScaledFuncLevelFunc(PlanetaryScalingFactor, DirectFireComponentCountermeasuresBonus), // strong bonus so they're hard to dodge
			"ComponentTargetingBonus":       MakeScaledFuncLevelFunc(PlanetaryScalingFactor, DirectFireComponentCountermeasuresBonus), // strong targeting bonus since nothing else adds to hit (sensors, targeting computer, etc)
			"WeaponEnergyPerShot":           MakeScaledFuncLevelFunc(PlanetaryScalingFactor, basis["WeaponEnergyPerShot"]),            // huge energy draw (doesn't matter, but doesn't hurt)
			"WeaponRawDamage":               MakeScaledFuncLevelFunc(PlanetaryScalingFactor, basis["WeaponRawDamage"]),                // huge raw damage
			"WeaponRange":                   MakeScaledFuncLevelFunc(PlanetaryScalingFactor, basis["WeaponRange"]),                    // huge range (this is hoping the range is measurd from surface by engine)
			"WeaponDamageFalloffRatio":      MakeScaledFuncLevelFunc(1/PlanetaryScalingFactor, basis["WeaponDamageFalloffRatio"]),     // radically reduce falloff (fraction of zero is zero)
			"WeaponFireRate":                MakeScaledFuncLevelFunc(.5, basis["WeaponFireRate"]),                                     // double rof
		},
	)
}

// returns a raw damage level function that uses your rate of fire and damage scale normalized to the standards
func MakeDamageFunc(fireRate, damageScale float64) LevelFunc {
	rofScale := fireRate / WeaponFireRateBasis
	dmgScale := damageScale * WeaponDamageBasis
	return MakeExpLevelFunc(dmgScale*rofScale, WeaponDamageIncreaseExp)
}

// returns a raw damage level function that uses your rate of fire and damage scale normalized to the standards
func MakeMultiSalvoWeaponDamageFunc(fireRate, salvoSize, damageScale float64) LevelFunc {
	return MakeDamageFunc(fireRate/salvoSize, damageScale)
}

// returns an energy ratio that includes a global bias
func MakeEnergyPerShotFunc(efficiency float64, damage LevelFunc) LevelFunc {
	return MakeScaledFuncLevelFunc(efficiency*WeaponEnergyRatioBasis, damage)
}

// returns an energy ratio that includes a global bias
func MakeThrustEnergyUsageFunc(efficiency float64, thrust LevelFunc) LevelFunc {
	return MakeScaledFuncLevelFunc(efficiency*EngineEnergyRatioBasis, thrust)
}
