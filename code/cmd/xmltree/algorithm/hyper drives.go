package algorithm

func HyperDrives(folder string) (err error) {

	// load all component definition files
	j, err := LoadJobFor(folder, "ComponentDefinitions*.xml")
	if err != nil {
		return
	}

	// apply this transformation
	err = j.applyHyperDrives()
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

func (j *Job) applyHyperDrives() (err error) {

	// apply stats for each component
	err = j.ApplyComponentAll(HyperdriveData)

	return
}

const (
	HyperDriveSpeedExp = 4. / 3
	HyperDrivePowerExp = HyperDriveSpeedExp
)

var (
	HyperdriveData = ComponentLevelDataMap{
		"Hyperdrive, Skip": {
			minLevel:       0,
			maxLevel:       1,
			componentStats: SkipComponentStats,
		},
		"Hyperdrive, Snap": {
			minLevel:       2,
			maxLevel:       8,
			componentStats: SnapComponentStats,
		},
		"Hyperdrive, Sojourn": {
			minLevel:       2,
			maxLevel:       8,
			componentStats: SojournComponentStats,
		},
		"Hyperdrive, Hyperstream": {
			minLevel:       2,
			maxLevel:       8,
			componentStats: HyperstreamComponentStats,
		},
		"Hyperdrive, Efficiency": {
			minLevel:       2,
			maxLevel:       8,
			componentStats: EfficiencyComponentStats,
		},
		"Hyperdrive, Velocity": {
			minLevel:       2,
			maxLevel:       10,
			componentStats: VelocityComponentStats,
		},
		"Hyperdrive, Torrent": {
			minLevel:       9,
			maxLevel:       10,
			componentStats: TorrentComponentStats,
		},
		"Hyperdrive, Wormhole": {
			minLevel:       2,
			maxLevel:       10,
			componentStats: WormholeComponentStats,
		},
		"Hyperdrive, Hive": {
			minLevel:       3,
			maxLevel:       5,
			componentStats: SojournComponentStats,
		},
	}

	HyperDriveBlockingInsulation = MakeLinearLevelFunc(0, 5)

	// subtle: the energy use proportion between weapons and hyperdrive varies per hull class
	// subtle: with small hulls having proportionally more energy use for hyperdrive vs. large hulls have proportionally more energy use for weapons
	// subtle: this is also true for advanced hulls: II, III, IV each increases total slots which moves the energy budget away from hyperdrive towards other systems
	HyperdriveEnergyUsageStandard = MakeExpLevelFunc(80, HyperDrivePowerExp)
	HyperdriveEnergyUsageHigh     = MakeScaledFuncLevelFunc(4./3., HyperdriveEnergyUsageStandard)
	HyperdriveEnergyUsageGood     = MakeScaledFuncLevelFunc(3./4., HyperdriveEnergyUsageStandard)

	HyperdriveSpeedStandard = MakeOffsetFuncLevelFunc(-2, MakeExpLevelFunc(300e3, HyperDriveSpeedExp))
	HyperdriveSpeedBad      = MakeScaledFuncLevelFunc(.8, HyperdriveSpeedStandard)
	HyperdriveSpeedGood     = MakeScaledFuncLevelFunc(1.25, HyperdriveSpeedStandard)

	HyperdriveJumpRangeStandard = MakeExpLevelFunc(80e6, 1.25)
	HyperdriveJumpRangeBad      = MakeScaledFuncLevelFunc(1/1.5, HyperdriveJumpRangeStandard)
	HyperdriveJumpRangeGood     = MakeScaledFuncLevelFunc(1.5, HyperdriveJumpRangeStandard)

	HyperdriveInitializationTimeStandard = MakeExpLevelFunc(30, 0.85)
	HyperdriveInitializationTimeBad      = MakeScaledFuncLevelFunc(1.5, HyperdriveInitializationTimeStandard)
	HyperdriveInitializationTimeGood     = MakeScaledFuncLevelFunc(1/1.5, HyperdriveInitializationTimeStandard)

	HyperdriveRechargeTimeStandard = MakeExpLevelFunc(30, 0.85)
	HyperdriveRechargeTimeBad      = MakeScaledFuncLevelFunc(1.5, HyperdriveRechargeTimeStandard)
	HyperdriveRechargeTimeGood     = MakeScaledFuncLevelFunc(1/1.5, HyperdriveRechargeTimeStandard)

	// vanilla peaks out at 2K as best accuracy
	HyperDriveJumpAccuracy = func(level int) float64 { return 1000 + 200*float64(10-level) }

	HyperDriveCrewRequirement = MakeFixedLevelFunc(8)

	HyperDriveStaticEnergy = MakeLinearLevelFunc(1.0, 0.5)

	AvgComponentStats = ComponentStats{
		"ComponentIonDefense":          StandardComponentIonDefense,
		"CrewRequirement":              HyperDriveCrewRequirement,
		"HyperDriveBlockingInsulation": HyperDriveBlockingInsulation,
		"HyperDriveEnergyUsage":        HyperdriveEnergyUsageStandard,
		"HyperDriveSpeed":              HyperdriveSpeedStandard,
		"HyperDriveJumpRange":          HyperdriveJumpRangeStandard,
		"HyperDriveJumpInitiationTime": HyperdriveInitializationTimeStandard,
		"HyperDriveRechargeTime":       HyperdriveRechargeTimeStandard,
		"HyperDriveJumpAccuracy":       HyperDriveJumpAccuracy,
		"StaticEnergyUsed":             HyperDriveStaticEnergy,
	}

	// basic
	SkipComponentStats = ComposeComponentStats(
		AvgComponentStats,
		ComponentStats{
			"HyperDriveEnergyUsage":        HyperdriveEnergyUsageStandard,
			"HyperDriveSpeed":              MakeLinearLevelFunc(100e3, 100e3),
			"HyperDriveJumpRange":          HyperdriveJumpRangeBad,
			"HyperDriveJumpInitiationTime": HyperdriveInitializationTimeBad,
			"HyperDriveRechargeTime":       HyperdriveRechargeTimeBad,
		},
	)

	// Snap
	SnapComponentStats = ComposeComponentStats(
		AvgComponentStats,
		ComponentStats{
			"HyperDriveJumpInitiationTime": HyperdriveInitializationTimeGood,
			"HyperDriveRechargeTime":       HyperdriveRechargeTimeGood,
			"StaticEnergyUsed":             MakeScaledFuncLevelFunc(1.5, HyperDriveStaticEnergy),
		},
	)

	// Sojourn
	SojournComponentStats = ComposeComponentStats(
		AvgComponentStats,
		ComponentStats{
			"HyperDriveJumpRange": HyperdriveJumpRangeGood,
		},
	)

	// Hyperstream
	HyperstreamComponentStats = ComposeComponentStats(
		AvgComponentStats,
		ComponentStats{
			"HyperDriveEnergyUsage":        HyperdriveEnergyUsageHigh,
			"HyperDriveSpeed":              HyperdriveSpeedGood,
			"HyperDriveJumpInitiationTime": HyperdriveInitializationTimeStandard,
			"HyperDriveRechargeTime":       HyperdriveRechargeTimeStandard,
			"StaticEnergyUsed":             MakeScaledFuncLevelFunc(2.0, HyperDriveStaticEnergy),
		},
	)

	// Efficiency
	EfficiencyComponentStats = ComposeComponentStats(
		AvgComponentStats,
		ComponentStats{
			"HyperDriveEnergyUsage": HyperdriveEnergyUsageGood,
			"HyperDriveSpeed":       HyperdriveSpeedBad,
			"StaticEnergyUsed":      MakeScaledFuncLevelFunc(0.5, HyperDriveStaticEnergy),
		},
	)

	// Velocity
	VelocityComponentStats = ComposeComponentStats(
		AvgComponentStats,
		ComponentStats{
			"HyperDriveEnergyUsage":        HyperdriveEnergyUsageHigh,
			"HyperDriveSpeed":              HyperdriveSpeedGood,
			"HyperDriveJumpInitiationTime": HyperdriveInitializationTimeGood,
			"HyperDriveRechargeTime":       HyperdriveInitializationTimeGood,
		},
	)

	// Torrent
	TorrentComponentStats = ComposeComponentStats(
		AvgComponentStats,
		ComponentStats{
			"HyperDriveEnergyUsage":        HyperdriveEnergyUsageGood,
			"HyperDriveJumpInitiationTime": HyperdriveInitializationTimeGood,
			"HyperDriveRechargeTime":       HyperdriveInitializationTimeGood,
			"StaticEnergyUsed":             MakeScaledFuncLevelFunc(1.5, HyperDriveStaticEnergy),
		},
	)

	// Wormhole
	WormholeComponentStats = ComponentStats{
		"ComponentIonDefense":          StandardComponentIonDefense,
		"CrewRequirement":              MakeFixedLevelFunc(12),
		"HyperDriveBlockingInsulation": HyperDriveBlockingInsulation,
		"HyperDriveEnergyUsage":        HyperdriveEnergyUsageHigh,
		"HyperDriveSpeed":              MakeExpLevelFunc(0.5e6, 1.25),
		"HyperDriveJumpRange":          MakeExpLevelFunc(100e6, 1.25),
		"HyperDriveJumpInitiationTime": HyperdriveInitializationTimeStandard,
		"HyperDriveRechargeTime":       HyperdriveRechargeTimeStandard,
		"HyperDriveJumpAccuracy":       HyperDriveJumpAccuracy,
		"StaticEnergyUsed":             MakeScaledFuncLevelFunc(2.0, HyperDriveStaticEnergy),
	}
)
