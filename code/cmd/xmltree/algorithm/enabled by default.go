package algorithm

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/gocarina/gocsv"
)

func EnabledByDefault(folder string, setting bool) (err error) {

	// load all files
	j, err := LoadJobFor(folder, "ResearchProjectDefinition*.xml")
	if err != nil {
		return
	}

	// apply enabled by defaults
	err = j.applyEnabledByDefault(setting)
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

func (j *Job) applyEnabledByDefault(setting bool) (err error) {

	if setting {
		// save the current settings first
		err = j.extractTechSummaries()
		if err != nil {
			return
		}
		fmt.Println("Overriding enabled by default to true for all techs")
	} else {
		fmt.Println("Setting enabled by default back to default settings")
	}

	// todo: the things

	return
}

func (j *Job) extractTechSummaries() (err error) {

	perSourceFileSummaries := map[string]TechSummaries{}

	for _, f := range j.xfiles {

		csvName := strings.ReplaceAll(filepath.Base(f.filename), ".xml", ".csv")

		// the root will result in a single ArrayOf[RootObjectType]
		for _, researchProjects := range f.root.Elements.Elements() {

			err = assertIs(researchProjects, "ArrayOfResearchProjectDefinition")
			if err != nil {
				return
			}

			for _, researchProject := range researchProjects.Elements() {

				// each of these is a ResearchProjectDefinition
				err = assertIs(researchProject, "ResearchProjectDefinition")
				if err != nil {
					return
				}

				f.stats.objects++

				// collect the data
				summary := &TechSummary{}
				summary.ResearchProjectId = ResearchProjectId(researchProject.Child("ResearchProjectId").IntValue())
				summary.Name = researchProject.Child("Name").StringValue()
				summary.TechLevel = researchProject.Child("Column").IntValue()
				summary.EnabledByDefault = researchProject.Child("EnabledByDefault").StringValueEquals("true")
				summary.Description = researchProject.Child("Description").StringValue()

				perSourceFileSummaries[csvName] = append(perSourceFileSummaries[csvName], summary)
			}
		}
	}

	// output them individually (by source file)
	for k, v := range perSourceFileSummaries {
		err = backupAndSaveCSVTo(k, v)
		if err != nil {
			return
		}
	}

	return
}

type ResearchProjectId int

type TechSummary struct {
	ResearchProjectId ResearchProjectId // id of the research project
	Name              string            // name of the research project
	TechLevel         int               // tech level t0, t1, t2, t3
	EnabledByDefault  bool              // is this enabled by default
	Description       string            // description of the research project
}

type TechSummaries []*TechSummary
type TechSummaryByID map[ResearchProjectId]*TechSummary

func loadTechSummariesAsMap(filename string) (values TechSummaryByID, err error) {

	// load the summaries
	summaries, err := loadTechSummaries(filename)
	if err != nil {
		return
	}

	// create a map to make lookup easier & faster
	values = TechSummaryByID{}
	for i := range summaries {
		values[summaries[i].ResearchProjectId] = summaries[i]
	}

	return
}

func loadTechSummaries(filename string) (summaries TechSummaries, err error) {

	var file *os.File
	file, err = os.Open(filename)
	if err != nil {
		return
	}
	defer file.Close()

	summaries = []*TechSummary{}
	err = gocsv.UnmarshalFile(file, &summaries)
	if err != nil {
		return
	}

	return
}
