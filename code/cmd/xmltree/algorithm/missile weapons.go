package algorithm

import "math"

func MissileWeapons(folder string) (err error) {

	// load all component definition files
	j, err := LoadJobFor(folder, "ComponentDefinitions*.xml")
	if err != nil {
		return
	}

	// apply this transformation
	err = j.applyMissileWeapons()
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

func (j *Job) applyMissileWeapons() (err error) {
	return j.ApplyComponentAll(MissileWeaponData)
}

const (
	// missile weapons get a 10% bonus to hit
	MissileWeaponTargetingBonus = .1

	// missile speed
	MissileSpeed           = 350
	MissileSpeedGrowthRate = 25

	// missile range
	MissileRange           = 3000
	MissileRangeGrowthRate = 250

	// missile energy efficiency
	MissileEnergyRatio = .4

	// missile damage
	MissileDamageFactor = 1.

	// missile fire rate
	MissileFireRate = 3.2 * WeaponFireRateBasis
)

var (
	MissileWeaponData = ComponentLevelDataMap{

		"Missile, Hunter-Seeker [S]": {
			minLevel:       0,
			maxLevel:       1,
			componentStats: SeekingMissileWeaponSmall,
		},

		"Missile, Obliterator [S]": {
			minLevel:       2,
			maxLevel:       4,
			componentStats: ObliteratorMissileWeaponSmall,
		},
		"Missile, Obliterator [M]": {
			minLevel:       2,
			maxLevel:       4,
			componentStats: DeriveMediumWeaponUsing2xVolley(ObliteratorMissileWeaponSmall),
		},

		"Missile, Swarm [S]": {
			minLevel:       5,
			maxLevel:       10,
			componentStats: SwarmMissileWeaponSmall,
		},
		"Missile, Swarm [M]": {
			minLevel:       5,
			maxLevel:       10,
			componentStats: DeriveMediumWeaponUsing2xVolley(SwarmMissileWeaponSmall),
		},

		"Missile, Lightning [S]": {
			minLevel:       5,
			maxLevel:       10,
			componentStats: LightningMissileWeaponSmall,
		},
		"Missile, Lightning [M]": {
			minLevel:       5,
			maxLevel:       10,
			componentStats: DeriveMediumWeaponUsing2xVolley(LightningMissileWeaponSmall),
		},

		"Missile, Assassin [M]": {
			minLevel:       5,
			maxLevel:       10,
			componentStats: DeriveMediumWeaponUsing2xDamage(LanceMissileWeaponSmall),
		},
		"Missile, Assassin [L]": {
			minLevel:       5,
			maxLevel:       10,
			componentStats: DeriveLargeWeaponUsing4xDamage(LanceMissileWeaponSmall),
		},

		"Missile, Harbinger of Doom [M]": {
			minLevel:       5,
			maxLevel:       10,
			componentStats: DeriveMediumWeaponUsing2xDamage(HunterMissileWeaponSmall),
		},
		"Missile, Harbinger of Doom [L]": {
			minLevel:       5,
			maxLevel:       10,
			componentStats: DeriveLargeWeaponUsing4xDamage(HunterMissileWeaponSmall),
		},

		"Missile, Reinforcing Swarm [S]": {
			minLevel:       5,
			maxLevel:       10,
			componentStats: SwarmMissileWeaponSmall,
		},
		"Missile, Reinforcing Swarm [M]": {
			minLevel:       5,
			maxLevel:       10,
			componentStats: DeriveMediumWeaponUsing2xVolley(SwarmMissileWeaponSmall),
		},

		"Missile, Titan [M]": {
			minLevel:       5,
			maxLevel:       10,
			componentStats: DeriveMediumWeaponUsing2xDamage(HunterMissileWeaponSmall),
		},
		"Missile, Titan [L]": {
			minLevel:       5,
			maxLevel:       10,
			componentStats: DeriveLargeWeaponUsing4xDamage(HunterMissileWeaponSmall),
		},

		"Missile, Scorpion [S]": {
			minLevel:       5,
			maxLevel:       10,
			componentStats: ScorpionMissileWeaponSmall,
		},

		"Ground to Space Missile Launchers": {
			minLevel:       3,
			maxLevel:       9,
			componentStats: PlanetaryMissileComponentStats,
		},
	}

	// Seeking
	SeekingMissileVolleySize   = 2.
	SeekingMissileVolleyRate   = 2.
	SeekingMissileShieldBypass = 0.
	SeekingMissileArmorBypass  = -0.1

	SeekingMissileTargetBonus         = MakeFixedLevelFunc(MissileWeaponTargetingBonus)
	SeekingMissileWeaponDamage        = MakeMultiSalvoWeaponDamageFunc(MissileFireRate, SeekingMissileVolleySize, MissileDamageFactor)
	SeekingMissileWeaponEnergyPerShot = MakeEnergyPerShotFunc(MissileEnergyRatio, SeekingMissileWeaponDamage)
	SeekingMissileWeaponSpeed         = MakeLinearLevelFunc(MissileSpeed, MissileSpeedGrowthRate)

	// Missiles have a -50% countermeasure bonus compared to other seeking weapons
	MissileCountermeasureBonus = MakeScaledFuncLevelFunc(0.5, SeekingComponentCountermeasuresBonus)

	SeekingMissileWeaponSmall = ComposeComponentStats(
		SmallWeaponBaseStats,
		WeaponNoAOE,
		WeaponNoBombard,
		WeaponNoIon,
		ComponentStats{
			"ComponentCountermeasuresBonus": MissileCountermeasureBonus,
			"ComponentTargetingBonus":       SeekingMissileTargetBonus,
			"WeaponDamageFalloffRatio":      MakeFixedLevelFunc(0),
			"WeaponArmorBypass":             MakeFixedLevelFunc(SeekingMissileArmorBypass),
			"WeaponShieldBypass":            MakeFixedLevelFunc(SeekingMissileShieldBypass),
			"WeaponRange":                   MakeLinearLevelFunc(MissileRange, MissileRangeGrowthRate),
			"WeaponSpeed":                   SeekingMissileWeaponSpeed,
			"WeaponFireRate":                MakeFixedLevelFunc(MissileFireRate),
			"WeaponEnergyPerShot":           SeekingMissileWeaponEnergyPerShot,
			"WeaponRawDamage":               SeekingMissileWeaponDamage,
			"WeaponVolleyAmount":            MakeFixedLevelFunc(SeekingMissileVolleySize),
			"WeaponVolleyFireRate":          MakeFixedLevelFunc(SeekingMissileVolleyRate),
		},
	)

	// Obliterator
	ObliteratorMissileDamageFactor = 1.
	ObliteratorMissileFireRate     = MissileFireRate
	ObliteratorMissileVolleySize   = 2.
	ObliteratorMissileVolleyRate   = 1.

	ObliteratorMissileWeaponFireRate      = MakeFixedLevelFunc(ObliteratorMissileFireRate)
	ObliteratorMissileWeaponRawDamage     = MakeMultiSalvoWeaponDamageFunc(ObliteratorMissileFireRate, ObliteratorMissileVolleySize, ObliteratorMissileDamageFactor)
	ObliteratorMissileWeaponEnergyPerShot = MakeEnergyPerShotFunc(MissileEnergyRatio, ObliteratorMissileWeaponRawDamage)

	ObliteratorMissileWeaponSmall = ComposeComponentStats(
		SeekingMissileWeaponSmall,
		ComponentStats{
			"WeaponFireRate":       ObliteratorMissileWeaponFireRate,
			"WeaponRawDamage":      ObliteratorMissileWeaponRawDamage,
			"WeaponEnergyPerShot":  ObliteratorMissileWeaponEnergyPerShot,
			"WeaponVolleyAmount":   MakeFixedLevelFunc(ObliteratorMissileVolleySize),
			"WeaponVolleyFireRate": MakeFixedLevelFunc(ObliteratorMissileVolleyRate),
		},
	)

	// Lightning (20% faster than obliterator)
	LightningMissileDamageFactor = 1.
	LightningMissileSpeedFactor  = 1.2
	LightningMissileFireRate     = MissileFireRate
	LightningMissileVolleySize   = 2.
	LightningMissileVolleyRate   = .5

	LightningMissileWeaponFireRate      = MakeFixedLevelFunc(LightningMissileFireRate)
	LightningMissileWeaponRawDamage     = MakeMultiSalvoWeaponDamageFunc(LightningMissileFireRate, LightningMissileVolleySize, LightningMissileDamageFactor)
	LightningMissileWeaponEnergyPerShot = MakeEnergyPerShotFunc(MissileEnergyRatio, LightningMissileWeaponRawDamage)

	LightningMissileWeaponSmall = ComposeComponentStats(
		ObliteratorMissileWeaponSmall,
		ComponentStats{
			"WeaponFireRate":       LightningMissileWeaponFireRate,
			"WeaponRawDamage":      LightningMissileWeaponRawDamage,
			"WeaponEnergyPerShot":  LightningMissileWeaponEnergyPerShot,
			"WeaponSpeed":          MakeScaledFuncLevelFunc(LightningMissileSpeedFactor, SeekingMissileWeaponSpeed),
			"WeaponVolleyAmount":   MakeFixedLevelFunc(LightningMissileVolleySize),
			"WeaponVolleyFireRate": MakeFixedLevelFunc(LightningMissileVolleyRate),
		},
	)

	// Swarm (rapid fire)
	SwarmMissileDamageFactor = 1.
	SwarmMissileFireRate     = 1.5 * MissileFireRate
	SwarmMissileVolleySize   = 2. * SeekingMissileVolleySize
	SwarmMissileVolleyRate   = .25

	SwarmMissileWeaponFireRate      = MakeFixedLevelFunc(SwarmMissileFireRate)
	SwarmMissileWeaponRawDamage     = MakeMultiSalvoWeaponDamageFunc(SwarmMissileFireRate, SwarmMissileVolleySize, SwarmMissileDamageFactor)
	SwarmMissileWeaponEnergyPerShot = MakeEnergyPerShotFunc(MissileEnergyRatio, SwarmMissileWeaponRawDamage)

	SwarmMissileWeaponSmall = ComposeComponentStats(
		ObliteratorMissileWeaponSmall,
		ComponentStats{
			"WeaponFireRate":       SwarmMissileWeaponFireRate,
			"WeaponRawDamage":      SwarmMissileWeaponRawDamage,
			"WeaponEnergyPerShot":  SwarmMissileWeaponEnergyPerShot,
			"WeaponVolleyAmount":   MakeFixedLevelFunc(SwarmMissileVolleySize),
			"WeaponVolleyFireRate": MakeFixedLevelFunc(SwarmMissileVolleyRate),
		},
	)

	// // Reinforcing Swarm (very rapid fire)
	// ReinforcingSwarmMissileDamageFactor = 1.
	// ReinforcingSwarmMissileFireRate     = SwarmMissileFireRate
	// ReinforcingSwarmMissileVolleySize   = 1.5 * SwarmMissileVolleySize
	// ReinforcingSwarmMissileVolleyRate   = SwarmMissileVolleyRate

	// ReinforcingSwarmMissileWeaponFireRate      = MakeFixedLevelFunc(ReinforcingSwarmMissileFireRate)
	// ReinforcingSwarmMissileWeaponRawDamage     = MakeMultiSalvoWeaponDamageFunc(ReinforcingSwarmMissileFireRate, ReinforcingSwarmMissileVolleySize, ReinforcingSwarmMissileDamageFactor)
	// ReinforcingSwarmMissileWeaponEnergyPerShot = MakeEnergyRatioFunc(MissileEnergyRatio, ReinforcingSwarmMissileWeaponRawDamage)

	// ReinforcingSwarmMissileWeaponSmall = ComposeComponentStats(
	// 	SwarmMissileWeaponSmall,
	// 	ComponentStats{
	// 		"WeaponFireRate":       ReinforcingSwarmMissileWeaponFireRate,
	// 		"WeaponRawDamage":      ReinforcingSwarmMissileWeaponRawDamage,
	// 		"WeaponEnergyPerShot":  ReinforcingSwarmMissileWeaponEnergyPerShot,
	// 		"WeaponVolleyAmount":   MakeFixedLevelFunc(ReinforcingSwarmMissileVolleySize),
	// 		"WeaponVolleyFireRate": MakeFixedLevelFunc(ReinforcingSwarmMissileVolleyRate),
	// 	},
	// )

	// Lance (rapid fire heavy missiles)
	LanceMissileDamageFactor = 1.
	LanceMissileFireRate     = MissileFireRate
	LanceMissileVolleySize   = 2.
	LanceMissileVolleyRate   = 1.

	LanceMissileWeaponFireRate = MakeExpLevelFunc(LanceMissileFireRate, .95)
	MakeLanceDamageFunc        = func(fireRate LevelFunc, salvoSize, damageScale float64) LevelFunc {
		// returns a level function that takes a dynamic rate of fire and gives you the damage at that level
		return func(level int) float64 {
			dmgBasis := damageScale * WeaponDamageBasis
			rofScale := fireRate(level) / WeaponFireRateBasis / salvoSize
			return dmgBasis * rofScale * math.Pow(WeaponDamageIncreaseExp, float64(level))
		}
	}
	LanceMissileWeaponRawDamage     = MakeLanceDamageFunc(LanceMissileWeaponFireRate, LanceMissileVolleySize, LanceMissileDamageFactor)
	LanceMissileWeaponEnergyPerShot = MakeEnergyPerShotFunc(MissileEnergyRatio, LanceMissileWeaponRawDamage)

	LanceMissileWeaponSmall = ComposeComponentStats(
		ObliteratorMissileWeaponSmall,
		ComponentStats{
			"WeaponFireRate":       LanceMissileWeaponFireRate,
			"WeaponRawDamage":      LanceMissileWeaponRawDamage,
			"WeaponEnergyPerShot":  LanceMissileWeaponEnergyPerShot,
			"WeaponVolleyAmount":   MakeFixedLevelFunc(LanceMissileVolleySize),
			"WeaponVolleyFireRate": MakeFixedLevelFunc(LanceMissileVolleyRate),
		},
	)

	// Hunter (heavy missiles)
	HunterMissileDamageFactor = 1.
	HunterMissileFireRate     = MissileFireRate
	HunterMissileVolleySize   = 2.
	HunterMissileVolleyRate   = 1.

	HunterMissileWeaponFireRate      = MakeFixedLevelFunc(HunterMissileFireRate)
	HunterMissileWeaponRawDamage     = MakeMultiSalvoWeaponDamageFunc(HunterMissileFireRate, HunterMissileVolleySize, HunterMissileDamageFactor)
	HunterMissileWeaponEnergyPerShot = MakeEnergyPerShotFunc(MissileEnergyRatio, HunterMissileWeaponRawDamage)

	HunterMissileWeaponSmall = ComposeComponentStats(
		SeekingMissileWeaponSmall,
		ComponentStats{
			"WeaponFireRate":       HunterMissileWeaponFireRate,
			"WeaponRawDamage":      HunterMissileWeaponRawDamage,
			"WeaponEnergyPerShot":  HunterMissileWeaponEnergyPerShot,
			"WeaponVolleyAmount":   MakeFixedLevelFunc(HunterMissileVolleySize),
			"WeaponVolleyFireRate": MakeFixedLevelFunc(HunterMissileVolleyRate),
		},
	)

	// // Titan (very heavy missiles)
	// TitanMissileDamageFactor = 1.
	// TitanMissileFireRate     = MissileFireRate
	// TitanMissileVolleySize   = 2.
	// TitanMissileVolleyRate   = 2.

	// TitanMissileWeaponFireRate      = MakeFixedLevelFunc(TitanMissileFireRate)
	// TitanMissileWeaponRawDamage     = MakeMultiSalvoWeaponDamageFunc(TitanMissileFireRate, TitanMissileVolleySize, TitanMissileDamageFactor)
	// TitanMissileWeaponEnergyPerShot = MakeEnergyRatioFunc(MissileEnergyRatio, TitanMissileWeaponRawDamage)

	// TitanMissileWeaponSmall = ComposeComponentStats(
	// 	HunterMissileWeaponSmall,
	// 	ComponentStats{
	// 		"WeaponFireRate":       TitanMissileWeaponFireRate,
	// 		"WeaponRawDamage":      TitanMissileWeaponRawDamage,
	// 		"WeaponEnergyPerShot":  TitanMissileWeaponEnergyPerShot,
	// 		"WeaponVolleyAmount":   MakeFixedLevelFunc(TitanMissileVolleySize),
	// 		"WeaponVolleyFireRate": MakeFixedLevelFunc(TitanMissileVolleyRate),
	// 	},
	// )

	// Scorpion (swarm missiles with higher damage)
	ScorpionMissileShieldBypass = 0.
	ScorpionMissileArmorBypass  = -0.1

	ScorpionMissileDamageFactor = 1.1
	ScorpionMissileFireRate     = 1.25 * MissileFireRate
	ScorpionMissileVolleySize   = 2. * SeekingMissileVolleySize
	ScorpionMissileVolleyRate   = .25

	ScorpionMissileWeaponFireRate      = MakeFixedLevelFunc(ScorpionMissileFireRate)
	ScorpionMissileWeaponRawDamage     = MakeMultiSalvoWeaponDamageFunc(ScorpionMissileFireRate, ScorpionMissileVolleySize, ScorpionMissileDamageFactor)
	ScorpionMissileWeaponEnergyPerShot = MakeEnergyPerShotFunc(MissileEnergyRatio, ScorpionMissileWeaponRawDamage)

	ScorpionMissileWeaponSmall = ComposeComponentStats(
		ObliteratorMissileWeaponSmall,
		ComponentStats{
			"ComponentCountermeasuresBonus": MakeScaledFuncLevelFunc(1.5, MissileCountermeasureBonus),
			"ComponentTargetingBonus":       MakeFixedLevelFunc(0.2),
			"WeaponArmorBypass":             MakeFixedLevelFunc(ScorpionMissileArmorBypass),
			"WeaponShieldBypass":            MakeFixedLevelFunc(ScorpionMissileShieldBypass),
			"WeaponFireRate":                ScorpionMissileWeaponFireRate,
			"WeaponRawDamage":               ScorpionMissileWeaponRawDamage,
			"WeaponEnergyPerShot":           ScorpionMissileWeaponEnergyPerShot,
			"WeaponVolleyAmount":            MakeFixedLevelFunc(ScorpionMissileVolleySize),
			"WeaponVolleyFireRate":          MakeFixedLevelFunc(ScorpionMissileVolleyRate),
		},
	)

	// we'll make these derivative of obliterator missiles
	PlanetaryMissileComponentStats = DerivePlanetaryFromLargeWeapon(DeriveLargeWeaponUsing4xDamage(ObliteratorMissileWeaponSmall))
)
