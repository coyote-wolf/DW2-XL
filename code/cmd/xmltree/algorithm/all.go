package algorithm

import "fmt"

func All(folder string) (err error) {

	// do components first
	err = Components(folder)
	if err != nil {
		return
	}

	// now do research costs
	err = ResearchCosts(folder)
	if err != nil {
		return
	}

	// now do hulls
	err = Hulls(folder)
	if err != nil {
		return
	}

	// finally do partial ordering
	err = PartialOrdering(folder)
	if err != nil {
		return
	}

	return
}

func Components(folder string) (err error) {

	// load all component definition files
	j, err := LoadJobFor(folder, "ComponentDefinitions*.xml")
	if err != nil {
		return
	}

	// apply this transformation
	err = j.applyComponents()
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

func (j *Job) applyComponents() (err error) {

	// primary ship components first
	err = j.applyArmors()
	if err != nil {
		return
	}
	err = j.applyShields()
	if err != nil {
		return
	}
	err = j.applyHyperDrives()
	if err != nil {
		return
	}
	err = j.applyReactors()
	if err != nil {
		return
	}
	err = j.applyEngines()
	if err != nil {
		return
	}
	err = j.applyIonShields()
	if err != nil {
		return
	}
	err = j.applyBlasterWeapons()
	if err != nil {
		return
	}
	err = j.applyBeamWeapons()
	if err != nil {
		return
	}
	err = j.applyBombardWeapons()
	if err != nil {
		return
	}
	err = j.applyGraviticWeapons()
	if err != nil {
		return
	}
	err = j.applyIonWeapons()
	if err != nil {
		return
	}
	err = j.applyKineticWeapons()
	if err != nil {
		return
	}
	err = j.applyMissileWeapons()
	if err != nil {
		return
	}
	err = j.applyTorpedoWeapons()
	if err != nil {
		return
	}

	err = j.applyDamageControl()
	if err != nil {
		return
	}

	err = j.applyMiningEngines()
	if err != nil {
		return
	}

	// then derivative components
	err = j.applyFighterArmors()
	if err != nil {
		return
	}
	err = j.applyFighterEngines()
	if err != nil {
		return
	}
	err = j.applyFighterReactors()
	if err != nil {
		return
	}
	err = j.applyFighterShields()
	if err != nil {
		return
	}
	err = j.applyFighterWeaponsAndPD()
	if err != nil {
		return
	}

	return
}

func Hulls(folder string) (err error) {

	fmt.Println("All component bay indexes will be fixed to a simple incremental index")
	fmt.Println("All ship hull slots will be updated to match our desired schedule")
	fmt.Println("All fighter slots will be updated to match our desired schedule")

	// load all ship hull definition files
	j, err := LoadJobFor(folder, "ShipHulls*.xml")
	if err != nil {
		return
	}

	// apply fighter hull schedule
	err = j.applyFighterHulls()
	if err != nil {
		return
	}

	// apply ship hull schedule
	err = j.updateShipHulls()
	if err != nil {
		return
	}

	// apply renumbering
	err = j.renumberComponentBays()
	if err != nil {
		return
	}

	// extract new hull data
	err = j.extractShipComponentBays()
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}
