package algorithm

func KineticWeapons(folder string) (err error) {

	// load all component definition files
	j, err := LoadJobFor(folder, "ComponentDefinitions*.xml")
	if err != nil {
		return
	}

	// update kinetic weapons
	err = j.applyKineticWeapons()
	if err != nil {
		return
	}

	// update derivatives
	err = j.applyFighterWeaponsAndPD()
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

func (j *Job) applyKineticWeapons() (err error) {

	// apply stats for each component
	err = j.ApplyComponentAll(KineticWeaponData)

	return
}

const (
	KineticBaseDamageFactor    = 1.1                       // base damage is better
	KineticBaseFireRate        = 1.2 * WeaponFireRateBasis // rof is slower
	KineticEnergyRatio         = 0.75                      // kinetics are quite efficient
	StandardKineticArmorBypass = -0.333333333

	ForgeFireRate    = KineticBaseFireRate * 1.5 // forge rail is all about the big boom
	ForgeArmorBypass = StandardKineticArmorBypass / 2

	TerminatorArmorBypass = StandardKineticArmorBypass * 2 / 3
)

var (
	KineticWeaponData = ComponentLevelDataMap{
		"Gun, Coil [S]": {
			minLevel:       0,
			maxLevel:       1,
			componentStats: BasicKineticWeaponComponentStats,
		},
		"Gun, Rail [S]": {
			minLevel:       2,
			maxLevel:       4,
			componentStats: SmallKineticWeaponComponentStats,
		},
		"Gun, Rail [M]": {
			minLevel:       2,
			maxLevel:       4,
			componentStats: MediumKineticWeaponComponentStats,
		},
		"Gun, Heavy Rail [L]": {
			minLevel:       3,
			maxLevel:       4,
			componentStats: LargeKineticWeaponComponentStats,
		},
		"Gun, Forge Rail [M]": {
			minLevel:       5,
			maxLevel:       10,
			componentStats: MediumForgeRailWeaponComponentStats,
		},
		"Gun, Forge Rail [L]": {
			minLevel:       5,
			maxLevel:       10,
			componentStats: LargeForgeRailWeaponComponentStats,
		},
		"Gun, Terminator Assault Cannon [S]": {
			minLevel:       5,
			maxLevel:       10,
			componentStats: TerminatorWeaponComponentStats,
		},

		"Gun, Hyperion Cannon [S]": {
			minLevel:       5,
			maxLevel:       10,
			componentStats: HyperionCannonWeaponComponentStats,
		},
		"Gun, Hyperion Cannon [M]": {
			minLevel:       5,
			maxLevel:       10,
			componentStats: DeriveMediumWeaponUsing2xDamage(HyperionCannonWeaponComponentStats),
		},

		"Massive Forge Rail Battery Emplacement": {
			minLevel:       3,
			maxLevel:       9,
			componentStats: PlanetaryForgeBatteryComponentStats,
		},
	}

	StandardKineticWeaponShieldBypass  = MakeFixedLevelFunc(0)                          // rail guns have no special interaction with shields
	StandardKineticWeaponArmorBypass   = MakeFixedLevelFunc(StandardKineticArmorBypass) // rail guns are especially affected by armor
	StandardKineticWeaponSpeed         = MakeLinearLevelFunc(750, 25)
	StandardKineticWeaponRange         = MakeScaledFuncLevelFunc(1.5, StandardKineticWeaponSpeed)
	StandardKineticWeaponDamage        = MakeDamageFunc(KineticBaseFireRate, KineticBaseDamageFactor)
	StandardKineticWeaponEnergyPerShot = MakeEnergyPerShotFunc(KineticEnergyRatio, StandardKineticWeaponDamage)
	StandardKineticWeaponFireRate      = MakeFixedLevelFunc(KineticBaseFireRate)
	StandardKineticWeaponFallOff       = MakeFixedLevelFunc(0) // rail guns don't lose dmg with distance

	// Rail Guns
	BasicKineticWeaponComponentStats = ComposeComponentStats(
		SmallWeaponBaseStats,
		WeaponNoAOE,
		WeaponNoBombard,
		WeaponNoIon,
		ComponentStats{
			"ComponentCountermeasuresBonus": DirectFireComponentCountermeasuresBonus,
			"ComponentTargetingBonus":       MakeFixedLevelFunc(0),
			"WeaponDamageFalloffRatio":      StandardKineticWeaponFallOff,
			"WeaponArmorBypass":             StandardKineticWeaponArmorBypass,
			"WeaponShieldBypass":            StandardKineticWeaponShieldBypass,
			"WeaponRange":                   StandardKineticWeaponRange,
			"WeaponSpeed":                   StandardKineticWeaponSpeed,
			"WeaponFireRate":                StandardKineticWeaponFireRate,
			"WeaponEnergyPerShot":           StandardKineticWeaponEnergyPerShot,
			"WeaponRawDamage":               StandardKineticWeaponDamage,
			"WeaponVolleyAmount":            MakeFixedLevelFunc(1),
			"WeaponVolleyFireRate":          MakeFixedLevelFunc(0),
		},
	)

	SmallKineticWeaponComponentStats = BasicKineticWeaponComponentStats

	MediumKineticWeaponSpeed          = MakeLinearLevelFunc(750, 50) // t2 = 850
	MediumKineticWeaponRange          = MakeScaledFuncLevelFunc(1.75, MediumKineticWeaponSpeed)
	MediumKineticWeaponComponentStats = ComposeComponentStats(
		DeriveMediumWeaponUsing2xDamage(SmallKineticWeaponComponentStats),
		ComponentStats{
			"WeaponSpeed": MediumKineticWeaponSpeed,
			"WeaponRange": MediumKineticWeaponRange,
		},
	)

	LargeKineticWeaponSpeed          = MakeLinearLevelFunc(700, 100) // t2 = 900
	LargeKineticWeaponRange          = MakeScaledFuncLevelFunc(2, LargeKineticWeaponSpeed)
	LargeKineticWeaponComponentStats = ComposeComponentStats(
		DeriveLargeWeaponUsing4xDamage(SmallKineticWeaponComponentStats),
		ComponentStats{
			"WeaponSpeed": LargeKineticWeaponSpeed,
			"WeaponRange": LargeKineticWeaponRange,
		},
	)

	// Forge Rail Batteries
	// note: Forge Batteries fire 50% slower, and do 50% more damage per stroke
	// note they also reduce their armor penalty by 50%
	ForgeRailWeaponFireRate             = MakeFixedLevelFunc(ForgeFireRate)
	MediumForgeRailWeaponDamage         = MakeDamageFunc(ForgeFireRate, 2*KineticBaseDamageFactor)
	MediumForgeRailWeaponEnergyPerShot  = MakeEnergyPerShotFunc(KineticEnergyRatio, MediumForgeRailWeaponDamage)
	ForgeRailWeaponArmorBypass          = MakeFixedLevelFunc(ForgeArmorBypass)
	MediumForgeRailWeaponComponentStats = ComposeComponentStats(
		MediumKineticWeaponComponentStats, // inherits medium speed & range
		ComponentStats{
			"WeaponFireRate":      ForgeRailWeaponFireRate,
			"WeaponRawDamage":     MediumForgeRailWeaponDamage,
			"WeaponEnergyPerShot": MediumForgeRailWeaponEnergyPerShot,
			"WeaponArmorBypass":   ForgeRailWeaponArmorBypass,
		},
	)

	LargeForgeRailWeaponComponentStats = ComposeComponentStats(
		LargeKineticWeaponComponentStats, // inherits medium speed & range
		ComponentStats{
			"WeaponFireRate":      ForgeRailWeaponFireRate,
			"WeaponRawDamage":     MakeScaledFuncLevelFunc(2, MediumForgeRailWeaponDamage),
			"WeaponEnergyPerShot": MakeScaledFuncLevelFunc(2, MediumForgeRailWeaponEnergyPerShot),
			"WeaponArmorBypass":   ForgeRailWeaponArmorBypass,
		},
	)

	// Terminator Autocannon
	TerminatorWeaponDamage         = MakeScaledFuncLevelFunc(1.1, StandardKineticWeaponDamage)
	TerminatorWeaponEnergyPerShot  = MakeEnergyPerShotFunc(KineticEnergyRatio, TerminatorWeaponDamage)
	TerminatorWeaponComponentStats = ComposeComponentStats(
		BasicKineticWeaponComponentStats,
		ComponentStats{
			"WeaponRawDamage":     TerminatorWeaponDamage,
			"WeaponEnergyPerShot": TerminatorWeaponEnergyPerShot,
			"WeaponArmorBypass":   MakeFixedLevelFunc(TerminatorArmorBypass), // assault cannon are less bothered by armor
		},
	)

	// Hyperion Cannon
	HyperionCannonWeaponComponentStats = ComposeComponentStats(
		TerminatorWeaponComponentStats,
		ComponentStats{
			"WeaponRawDamage": MakeScaledFuncLevelFunc(1.1, TerminatorWeaponDamage),       // 10% more base damage for no extra energy cost
			"WeaponFireRate":  MakeScaledFuncLevelFunc(.9, StandardKineticWeaponFireRate), // 11.111% faster for full damage
		},
	)

	// Planetary Rail Batteries
	// note: we only care about a few of these (v1, v4, v7)
	PlanetaryForgeBatteryComponentStats = ComposeComponentStats(
		DerivePlanetaryFromLargeWeapon(LargeForgeRailWeaponComponentStats),
		ComponentStats{"WeaponSpeed": MakeScaledFuncLevelFunc(2, LargeKineticWeaponSpeed)},
	)
)

func DeriveMediumKineticFromSmall(base ComponentStats) ComponentStats {
	return ComposeComponentStats(
		base,
		LargeWeaponBaseStats,
		ComponentStats{
			"WeaponEnergyPerShot": MakeScaledFuncLevelFunc(2, base["WeaponEnergyPerShot"]),
			"WeaponRawDamage":     MakeScaledFuncLevelFunc(2, base["WeaponRawDamage"]),
		},
	)
}
