package algorithm

import (
	"fmt"
	"os"
	"path/filepath"
	"sort"
	"strings"

	"github.com/gocarina/gocsv"
)

func ExtractShipComponentBays(folder string) (err error) {

	// load all ship hull definition files
	j, err := LoadJobFor(folder, "ShipHulls*.xml")
	if err != nil {
		return
	}

	// apply this transformation
	err = j.extractShipComponentBays()
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

func (j *Job) extractShipComponentBays() (err error) {

	fmt.Println("Will extract component bay data for all designs in csv format")

	perSourceFileSummaries := map[string][]*ShipHullSummary{}

	for _, f := range j.xfiles {

		csvName := strings.ReplaceAll(filepath.Base(f.filename), ".xml", ".csv")

		// the root will result in a single ArrayOf[RootObjectType]
		for _, shiphulls := range f.root.Elements.Elements() {

			err = assertIs(shiphulls, "ArrayOfShipHull")
			if err != nil {
				return
			}

			for _, shiphull := range shiphulls.Elements() {

				// each of these is a ShipHull
				err = assertIs(shiphull, "ShipHull")
				if err != nil {
					return
				}

				f.stats.objects++

				ship := &ShipHullSummary{}

				// track the identity of this ship
				ship.ShipHullId = shiphull.Child("ShipHullId").IntValue()
				ship.Name = shiphull.Child("Name").StringValue()
				ship.RaceId = RaceID(shiphull.Child("RaceId").IntValue())
				ship.RaceName = RaceNames[ship.RaceId]
				ship.Role = shiphull.Child("Role").StringValue()
				if ship.Role != "Undefined" {
					ship.Level = shiphull.Child("Level").IntValue()

					// todo: we could list out all bonuses in a fixed order (and zero for none for that bonus type)

					ship.engineLimit = shiphull.Child("EngineLimit").IntValue()

					// scan the component bays and count up each type
					for _, c := range shiphull.Child("ComponentBays").Elements() {

						f.stats.elements++

						switch c.Child("Type").StringValue() {
						case "Weapon":
							ship.Weapon++
						case "Hangar":
							ship.Hangar++
						case "Engine":
							ship.Engine++
						case "Sensor":
							ship.Sensor++
						case "Defense":
							ship.Defense++
						case "General":
							ship.General++
						}
					}
				}

				// apply the engine limit (we no longer output that as a separate column)
				if ship.engineLimit != -1 {
					ship.Engine = ship.engineLimit
				}

				perSourceFileSummaries[csvName] = append(perSourceFileSummaries[csvName], ship)
			}
		}
	}

	// output them individually (by source file)
	for k, v := range perSourceFileSummaries {
		err = recordCSVTo("temp/"+k, v)
		if err != nil {
			return
		}
	}

	// build a master summaries
	accumulatedSummaries := []*ShipHullSummary{}
	for _, v := range perSourceFileSummaries {
		accumulatedSummaries = append(accumulatedSummaries, v...)
	}

	// sort it by role, race, level
	sortingHat := func(i, j int) bool {
		switch strings.Compare(accumulatedSummaries[i].Role, accumulatedSummaries[j].Role) {
		case -1:
			return true
		case 1:
			return false
		}
		switch strings.Compare(accumulatedSummaries[i].RaceName, accumulatedSummaries[j].RaceName) {
		case -1:
			return true
		case 1:
			return false
		}
		if accumulatedSummaries[i].Level < accumulatedSummaries[j].Level {
			return true
		}
		if accumulatedSummaries[i].Level > accumulatedSummaries[j].Level {
			return false
		}
		return accumulatedSummaries[i].ShipHullId < accumulatedSummaries[j].ShipHullId
	}
	sort.Slice(accumulatedSummaries, sortingHat)

	// output a full summarized file
	err = backupAndSaveCSVTo("ShipHulls.csv", accumulatedSummaries)

	return
}

func backupAndSaveCSVTo(filename string, data any) (err error) {

	// delete any old backup file
	err = os.Remove("temp/" + filename)
	if err == nil {
		fmt.Printf("removed temp %s\n", filename)
	} else if !os.IsNotExist(err) {
		fmt.Printf("failed to remove temp %s: %s\n", filename, err)
	}

	// backup the current file to the temp folder
	err = os.Rename(filename, "temp/"+filename)
	if err == nil {
		fmt.Printf("moved %s to temp\n", filename)
	} else if !os.IsNotExist(err) {
		fmt.Printf("failed to backup to temp %s: %s\n", filename, err)
	}

	// save the new file
	return recordCSVTo(filename, data)
}

func recordCSVTo(filename string, data any) (err error) {
	// build the target file with contents
	file, err := os.Create(filename)
	if err == nil {
		defer file.Close()
		err = gocsv.MarshalFile(data, file)
	}
	return
}
