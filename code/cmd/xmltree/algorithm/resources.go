package algorithm

import (
	"fmt"
	"math"
)

// construction resources
const (
	EmerosCrystal      = 0
	NekrosStone        = 1
	Osalia             = 2
	KaasianCrystal     = 3
	Argon              = 4
	Krypton            = 5
	Tyderios           = 6
	Silicon            = 7
	Steel              = 8
	Mebnar             = 9
	Aculon             = 10
	Cuprica            = 11
	Polymer            = 12
	Carbonite          = 13
	DyrilliumQuartz    = 77
	Hexodorium         = 78
	RiftCrystal        = 79
	StriderBrainPlasma = 80
)

func ScaleResourceCost(resourceID int, baseAmount float64) float64 {
	// technically the xml allows floats, but shows rounded values anyway
	return math.Round(baseAmount * ResourceRarityScale(resourceID))
}

func ResourceRarityScale(resourceID int) float64 {

	switch resourceID {

	// common
	case Aculon, Carbonite, Cuprica, Steel, Mebnar:
		return 1

	// less common
	case Argon, Krypton, Polymer, Silicon:
		return .75

	// rare
	case DyrilliumQuartz, EmerosCrystal, NekrosStone, Osalia, Tyderios:
		return .5

	// very rare
	case KaasianCrystal, Hexodorium:
		return .25

	// shakturi (very rare)
	case RiftCrystal, StriderBrainPlasma:
		return .25

	}
	panic(fmt.Errorf("ResourceRarity: unknown resource id: %d", resourceID))
}
