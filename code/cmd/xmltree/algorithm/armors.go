package algorithm

import (
	"github.com/lucky-wolf/xml-tree/xmltree"
)

func Armors(folder string) (err error) {

	// load all component definition files
	j, err := LoadJobFor(folder, "ComponentDefinitions*.xml")
	if err != nil {
		return
	}

	// update kinetic weapons
	err = j.applyArmors()
	if err != nil {
		return
	}

	// update derivatives
	err = j.applyFighterArmors()
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

func (j *Job) applyArmors() (err error) {

	// apply stats for each component
	err = j.ApplyComponentAll(StarshipArmorData)

	return
}

// general trend is currently that weapons increase in output faster than armors can resist it
// subtle: but note that starting values matter immensely - if weapons outpace armor to begin with - they'll only spread that delta w/o any extra exp delta
const (
	ArmorSizeCompact  = 12
	ArmorSizeStandard = 14
	ArmorSizeLarge    = 16
)

var (
	// strengths are each 50% stronger than the previous category
	WeakArmorBlastRating     = MakeExpLevelFunc(WeakArmorStrengthBasis, ArmorStrengthIncreaseExp)
	StandardArmorBlastRating = MakeScaledFuncLevelFunc(1.5, WeakArmorBlastRating)
	StrongArmorBlastRating   = MakeScaledFuncLevelFunc(2.25, WeakArmorBlastRating)

	// need to be careful not to lock ftr weapons out
	WeakArmorReactiveRating     = MakeExpLevelFunc(ArmorReactiveBasis, ArmorReactiveIncreaseExp)
	StandardArmorReactiveRating = MakeScaledFuncLevelFunc(1.5, WeakArmorReactiveRating)
	StrongArmorReactiveRating   = MakeScaledFuncLevelFunc(2.25, WeakArmorReactiveRating)

	// Ion has it's own balance, scaling, and rules
	WeakArmorIonDefense     = MakeLinearLevelFunc(0, 1)
	StandardArmorIonDefense = MakeLinearLevelFunc(0, 2)
	StrongArmorIonDefense   = MakeLinearLevelFunc(0, 4)

	// inert armor requires no crew and no energy
	InertArmorCrew         = MakeFixedLevelFunc(0)
	InertArmorStaticEnergy = MakeFixedLevelFunc(0)

	// active armor only requires some energy, but no crew
	ActiveArmorCrew         = MakeFixedLevelFunc(0)
	ActiveArmorStaticEnergy = MakeLinearLevelFunc(.5, .5)

	CompactArmorValues = SimpleValuesTable{
		"Size": xmltree.CreateInt(ArmorSizeCompact),
	}
	StandardArmorValues = SimpleValuesTable{
		"Size": xmltree.CreateInt(ArmorSizeStandard),
	}
	LargeArmorValues = SimpleValuesTable{
		"Size": xmltree.CreateInt(ArmorSizeLarge),
	}

	StarshipArmorData = ComponentLevelDataMap{

		"Armor Plating": {
			values:         StandardArmorValues,
			minLevel:       0,
			maxLevel:       1,
			componentStats: ArmorPlatingComponentStats,
		},

		"Ion Sheath Armor": {
			values:         StandardArmorValues,
			minLevel:       2,
			maxLevel:       3,
			componentStats: IonArmorComponentStats,
		},
		"Enhanced Ion Sheath Armor": {
			values:         StandardArmorValues,
			minLevel:       4,
			maxLevel:       5,
			componentStats: IonArmorComponentStats,
		},
		"Hardened Ion Sheath Armor": {
			values:         StandardArmorValues,
			minLevel:       6,
			maxLevel:       7,
			componentStats: IonArmorComponentStats,
		},
		"Ultra-Dense Ion Sheath Armor": {
			values:         StandardArmorValues,
			minLevel:       8,
			maxLevel:       9,
			componentStats: IonArmorComponentStats,
		},
		"Absorbing Ion Sheath Armor": {
			values:         StandardArmorValues,
			minLevel:       10,
			maxLevel:       10,
			componentStats: IonArmorComponentStats,
		},

		"Heavy Armor": {
			values:         StandardArmorValues,
			minLevel:       2,
			maxLevel:       3,
			componentStats: HeavyArmorComponentStats,
		},
		"Enhanced Heavy Armor": {
			values:         StandardArmorValues,
			minLevel:       4,
			maxLevel:       5,
			componentStats: HeavyArmorComponentStats,
		},
		"Hardened Heavy Armor": {
			values:         StandardArmorValues,
			minLevel:       6,
			maxLevel:       7,
			componentStats: HeavyArmorComponentStats,
		},
		"Ultra-Dense Heavy Armor": {
			values:         StandardArmorValues,
			minLevel:       8,
			maxLevel:       9,
			componentStats: HeavyArmorComponentStats,
		},
		"Absorbing Heavy Armor": {
			values:         StandardArmorValues,
			minLevel:       10,
			maxLevel:       10,
			componentStats: HeavyArmorComponentStats,
		},

		"Reactive Armor": {
			values:         StandardArmorValues,
			minLevel:       2,
			maxLevel:       3,
			componentStats: ReactiveArmorComponentStats,
		},
		"Enhanced Reactive Armor": {
			values:         StandardArmorValues,
			minLevel:       4,
			maxLevel:       5,
			componentStats: ReactiveArmorComponentStats,
		},
		"Hardened Reactive Armor": {
			values:         StandardArmorValues,
			minLevel:       6,
			maxLevel:       7,
			componentStats: ReactiveArmorComponentStats,
		},
		"Ultra-Dense Reactive Armor": {
			values:         StandardArmorValues,
			minLevel:       8,
			maxLevel:       9,
			componentStats: ReactiveArmorComponentStats,
		},
		"Absorbing Reactive Armor": {
			values:         StandardArmorValues,
			minLevel:       10,
			maxLevel:       10,
			componentStats: ReactiveArmorComponentStats,
		},

		"Flux Sheath Armor": {
			values:         StandardArmorValues,
			minLevel:       2,
			maxLevel:       3,
			componentStats: FluxArmorComponentStats1,
		},
		"Flux Enhanced Armor": {
			values:         StandardArmorValues,
			minLevel:       4,
			maxLevel:       5,
			componentStats: FluxArmorComponentStats1,
		},
		"Flux Hardened Armor": {
			values:         StandardArmorValues,
			minLevel:       6,
			maxLevel:       7,
			componentStats: FluxArmorComponentStats2,
		},
		"Flux Ultra-Dense Armor": {
			values:         StandardArmorValues,
			minLevel:       8,
			maxLevel:       9,
			componentStats: FluxArmorComponentStats2,
		},
		"Flux Absorbing Armor": {
			values:         StandardArmorValues,
			minLevel:       10,
			maxLevel:       10,
			componentStats: FluxArmorComponentStats3,
		},

		"Hex Armor": {
			values:         CompactArmorValues,
			minLevel:       2,
			maxLevel:       3,
			componentStats: HexArmorComponentStats1,
		},
		"Reactive Hex Armor": {
			values:         CompactArmorValues,
			minLevel:       4,
			maxLevel:       5,
			componentStats: HexArmorComponentStats1,
		},
		"Dense Hex Armor": {
			values:         CompactArmorValues,
			minLevel:       6,
			maxLevel:       7,
			componentStats: HexArmorComponentStats2,
		},
		"Multi-Dimensional Hex Armor": {
			values:         CompactArmorValues,
			minLevel:       8,
			maxLevel:       9,
			componentStats: HexArmorComponentStats2,
		},
		"Infinite Hex Armor": {
			values:         CompactArmorValues,
			minLevel:       10,
			maxLevel:       10,
			componentStats: HexArmorComponentStats3,
		},

		"Draconic Armor, Black": {
			values:         StandardArmorValues,
			minLevel:       2,
			maxLevel:       3,
			componentStats: DragonArmorComponentStats1,
		},
		"Draconic Armor, Red": {
			values:         StandardArmorValues,
			minLevel:       4,
			maxLevel:       5,
			componentStats: DragonArmorComponentStats1,
		},
		"Draconic Armor, Green": {
			values:         StandardArmorValues,
			minLevel:       6,
			maxLevel:       7,
			componentStats: DragonArmorComponentStats2,
		},
		"Draconic Armor, Pink": {
			values:         StandardArmorValues,
			minLevel:       8,
			maxLevel:       9,
			componentStats: DragonArmorComponentStats2,
		},
		"Draconic Armor, Blue": {
			values:         StandardArmorValues,
			minLevel:       10,
			maxLevel:       10,
			componentStats: DragonArmorComponentStats3,
		},

		"Stellar Armor": {
			values:         LargeArmorValues,
			minLevel:       10,
			maxLevel:       10,
			componentStats: StellarArmorComponentStats,
		},
	}

	ArmorPlatingComponentStats = ComponentStats{
		"ComponentIonDefense": HardenedComponentIonDefense, // armors are hardened
		"CrewRequirement":     InertArmorCrew,
		"StaticEnergyUsed":    InertArmorStaticEnergy,
		"ArmorBlastRating":    WeakArmorBlastRating,
		"ArmorReactiveRating": WeakArmorReactiveRating,
		"IonDamageDefense":    WeakArmorIonDefense,
	}

	ReactiveArmorComponentStats = ComponentStats{
		"ComponentIonDefense": HardenedComponentIonDefense, // armors are hardened
		"CrewRequirement":     ActiveArmorCrew,
		"StaticEnergyUsed":    ActiveArmorStaticEnergy,
		"ArmorBlastRating":    WeakArmorBlastRating,
		"ArmorReactiveRating": StrongArmorReactiveRating,
		"IonDamageDefense":    StandardArmorIonDefense,
	}

	HeavyArmorComponentStats = ComponentStats{
		"ComponentIonDefense": HardenedComponentIonDefense, // armors are hardened
		"CrewRequirement":     InertArmorCrew,
		"StaticEnergyUsed":    InertArmorStaticEnergy,
		"ArmorBlastRating":    StrongArmorBlastRating,
		"ArmorReactiveRating": StandardArmorReactiveRating,
		"IonDamageDefense":    WeakArmorIonDefense,
	}

	IonArmorComponentStats = ComponentStats{
		"ComponentIonDefense": HardenedComponentIonDefense, // armors are hardened
		"CrewRequirement":     ActiveArmorCrew,
		"StaticEnergyUsed":    ActiveArmorStaticEnergy,
		"ArmorBlastRating":    StandardArmorBlastRating,
		"ArmorReactiveRating": WeakArmorReactiveRating,
		"IonDamageDefense":    StrongArmorIonDefense,
	}

	FluxArmorComponentStats1 = ComponentStats{
		"ComponentIonDefense": HardenedComponentIonDefense, // armors are hardened
		"CrewRequirement":     InertArmorCrew,
		"StaticEnergyUsed":    InertArmorStaticEnergy,
		"ArmorBlastRating":    StandardArmorBlastRating,
		"ArmorReactiveRating": StandardArmorReactiveRating,
		"IonDamageDefense":    StrongArmorIonDefense,
	}

	FluxArmorComponentStats2 = ComponentStats{
		"ComponentIonDefense": HardenedComponentIonDefense, // armors are hardened
		"CrewRequirement":     InertArmorCrew,
		"StaticEnergyUsed":    InertArmorStaticEnergy,
		"ArmorBlastRating":    StrongArmorBlastRating,
		"ArmorReactiveRating": StandardArmorReactiveRating,
		"IonDamageDefense":    StrongArmorIonDefense,
	}

	FluxArmorComponentStats3 = ComponentStats{
		"ComponentIonDefense": HardenedComponentIonDefense, // armors are hardened
		"CrewRequirement":     InertArmorCrew,
		"StaticEnergyUsed":    InertArmorStaticEnergy,
		"ArmorBlastRating":    StrongArmorBlastRating,
		"ArmorReactiveRating": StrongArmorReactiveRating,
		"IonDamageDefense":    StrongArmorIonDefense,
	}

	HexArmorComponentStats1 = ComponentStats{
		"ComponentIonDefense": HardenedComponentIonDefense, // armors are hardened
		"CrewRequirement":     InertArmorCrew,
		"StaticEnergyUsed":    InertArmorStaticEnergy,
		"ArmorBlastRating":    StandardArmorBlastRating,
		"ArmorReactiveRating": StrongArmorReactiveRating,
		"IonDamageDefense":    StandardArmorIonDefense,
	}

	HexArmorComponentStats2 = ComponentStats{
		"ComponentIonDefense": HardenedComponentIonDefense, // armors are hardened
		"CrewRequirement":     InertArmorCrew,
		"StaticEnergyUsed":    InertArmorStaticEnergy,
		"ArmorBlastRating":    StandardArmorBlastRating,
		"ArmorReactiveRating": StrongArmorReactiveRating,
		"IonDamageDefense":    StrongArmorIonDefense,
	}

	HexArmorComponentStats3 = ComponentStats{
		"ComponentIonDefense": HardenedComponentIonDefense, // armors are hardened
		"CrewRequirement":     InertArmorCrew,
		"StaticEnergyUsed":    InertArmorStaticEnergy,
		"ArmorBlastRating":    StrongArmorBlastRating,
		"ArmorReactiveRating": StrongArmorReactiveRating,
		"IonDamageDefense":    StrongArmorIonDefense,
	}

	DragonArmorComponentStats1 = ComponentStats{
		"ComponentIonDefense": HardenedComponentIonDefense, // armors are hardened
		"CrewRequirement":     InertArmorCrew,
		"StaticEnergyUsed":    InertArmorStaticEnergy,
		"ArmorBlastRating":    StrongArmorBlastRating,
		"ArmorReactiveRating": StandardArmorReactiveRating,
		"IonDamageDefense":    StandardArmorIonDefense,
	}

	DragonArmorComponentStats2 = ComponentStats{
		"ComponentIonDefense": HardenedComponentIonDefense, // armors are hardened
		"CrewRequirement":     InertArmorCrew,
		"StaticEnergyUsed":    InertArmorStaticEnergy,
		"ArmorBlastRating":    StrongArmorBlastRating,
		"ArmorReactiveRating": StrongArmorReactiveRating,
		"IonDamageDefense":    StandardArmorIonDefense,
	}

	DragonArmorComponentStats3 = ComponentStats{
		"ComponentIonDefense": HardenedComponentIonDefense, // armors are hardened
		"CrewRequirement":     InertArmorCrew,
		"StaticEnergyUsed":    InertArmorStaticEnergy,
		"ArmorBlastRating":    StrongArmorBlastRating,
		"ArmorReactiveRating": StrongArmorReactiveRating,
		"IonDamageDefense":    StrongArmorIonDefense,
	}

	StellarArmorComponentStats = ComponentStats{
		"ComponentIonDefense": HardenedComponentIonDefense, // armors are hardened
		"CrewRequirement":     ActiveArmorCrew,
		"StaticEnergyUsed":    ActiveArmorStaticEnergy,
		"ArmorBlastRating":    StrongArmorBlastRating,
		"ArmorReactiveRating": StrongArmorReactiveRating,
		"IonDamageDefense":    StrongArmorIonDefense,
	}
)
