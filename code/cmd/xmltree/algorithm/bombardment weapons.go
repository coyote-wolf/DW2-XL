package algorithm

import "github.com/lucky-wolf/xml-tree/xmltree"

func BombardWeapons(folder string) (err error) {

	// load all component definition files
	j, err := LoadJobFor(folder, "ComponentDefinitions*.xml")
	if err != nil {
		return
	}

	// apply this transformation
	err = j.applyBombardWeapons()
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

func (j *Job) applyBombardWeapons() (err error) {
	return j.ApplyComponentAll(BombardWeaponData)
}

const (
	BombSize = 18

	// bombard weapons get a 10% bonus to hit
	BombardWeaponTargetingBonus = .1

	// bombard speed
	BombardSpeed         = 200
	BombardSpeedIncrease = 10

	// bombard range
	BombardRange         = 1600
	BombardRangeIncrease = 100

	// bombard energy efficiency
	BombardEnergyRatio = .5

	// bombard fire rate
	BombardFireRate = 4 * WeaponFireRateBasis

	// base bombard damage
	// generalized destruction
	BombardDamageInfrastructure = 10
	BombardDamageMilitary       = 30
	BombardDamagePopulation     = 30
	BombardDamageQuality        = 60

	// mass driver bombard factors
	// focused on military targets
	DriverBombardEnergyRatio          = KineticEnergyRatio
	DriverBombardFireRate             = 3 * WeaponFireRateBasis
	DriverBombardDamageInfrastructure = 20
	DriverBombardDamageMilitary       = 60
	DriverBombardDamagePopulation     = 15
	DriverBombardDamageQuality        = 30
)

var (
	BombStandardValues = SimpleValuesTable{
		"Size": xmltree.CreateInt(BombSize),
	}

	BombardWeaponData = ComponentLevelDataMap{

		"Bombardment, Atomic Devastator [B]": {
			values:         BombStandardValues,
			minLevel:       0,
			maxLevel:       1,
			componentStats: AtomicBombardWeapon,
		},

		"Bombardment, Judgement Day [B]": {
			values:         BombStandardValues,
			minLevel:       2,
			maxLevel:       4,
			componentStats: JudgementDayBombardWeapon,
		},

		"Bombardment, Wrath of Heaven [B]": {
			values:         BombStandardValues,
			minLevel:       5,
			maxLevel:       7,
			componentStats: WrathOfHeavenBombardWeapon,
		},

		"Bombardment, Diplomat [B]": {
			values:         BombStandardValues,
			minLevel:       8,
			maxLevel:       10,
			componentStats: DiplomatBombardWeapon,
		},

		"Bombardment, Mass Drivers [B]": {
			values:         BombStandardValues,
			minLevel:       1,
			maxLevel:       10,
			componentStats: EclipseBombardWeapon,
		},
	}

	// Atomic
	AtomicBombardWeaponEnergyPerShot = MakeEnergyPerShotFunc(BombardEnergyRatio, AtomicBombardMilitary)
	AtomicBombardWeaponSpeed         = MakeLinearLevelFunc(BombardSpeed, BombardSpeedIncrease)
	AtomicBombardRange               = MakeLinearLevelFunc(BombardRange, BombardRangeIncrease)
	AtomicBombardFireRate            = MakeFixedLevelFunc(BombardFireRate)

	AtomicBombardInfrastructure = MakeExpLevelFunc(BombardDamageInfrastructure, WeaponDamageIncreaseExp)
	AtomicBombardMilitary       = MakeExpLevelFunc(BombardDamageMilitary, WeaponDamageIncreaseExp)
	AtomicBombardPopulation     = MakeExpLevelFunc(BombardDamagePopulation, WeaponDamageIncreaseExp)
	AtomicBombardQuality        = MakeExpLevelFunc(BombardDamageQuality, WeaponDamageIncreaseExp)

	AtomicBombardWeapon = ComposeComponentStats(
		MediumWeaponBaseStats,
		WeaponNoAOE,
		WeaponNoIon,
		ComponentStats{
			"ComponentCountermeasuresBonus":     SeekingComponentCountermeasuresBonus,
			"ComponentTargetingBonus":           MakeFixedLevelFunc(0),
			"WeaponDamageFalloffRatio":          MakeFixedLevelFunc(0),
			"WeaponRawDamage":                   MakeFixedLevelFunc(0),
			"WeaponVolleyAmount":                MakeFixedLevelFunc(1),
			"WeaponVolleyFireRate":              MakeFixedLevelFunc(0),
			"WeaponRange":                       AtomicBombardRange,
			"WeaponSpeed":                       AtomicBombardWeaponSpeed,
			"WeaponFireRate":                    AtomicBombardFireRate,
			"WeaponEnergyPerShot":               AtomicBombardWeaponEnergyPerShot,
			"WeaponBombardDamageInfrastructure": AtomicBombardInfrastructure,
			"WeaponBombardDamageMilitary":       AtomicBombardMilitary,
			"WeaponBombardDamagePopulation":     AtomicBombardPopulation,
			"WeaponBombardDamageQuality":        AtomicBombardQuality,
		},
	)

	JudgementInfrastructure = func(level int) float64 {
		return 1.1 * AtomicBombardInfrastructure(level)
	}
	JudgementDayBombardWeapon = ComposeComponentStats(
		AtomicBombardWeapon,
		ComponentStats{
			// "WeaponBombardDamageInfrastructure": JudgementInfrastructure,
			"WeaponBombardDamageInfrastructure": MakeScaledFuncLevelFunc(1.1, AtomicBombardInfrastructure),
			"WeaponBombardDamageMilitary":       MakeScaledFuncLevelFunc(1.1, AtomicBombardMilitary),
			"WeaponBombardDamagePopulation":     MakeScaledFuncLevelFunc(1.1, AtomicBombardPopulation),
			"WeaponBombardDamageQuality":        MakeScaledFuncLevelFunc(1.1, AtomicBombardQuality),
		},
	)

	WrathOfHeavenBombardWeapon = ComposeComponentStats(
		AtomicBombardWeapon,
		ComponentStats{
			"WeaponBombardDamageInfrastructure": MakeScaledFuncLevelFunc(1.2, AtomicBombardInfrastructure),
			"WeaponBombardDamageMilitary":       MakeScaledFuncLevelFunc(1.2, AtomicBombardMilitary),
			"WeaponBombardDamagePopulation":     MakeScaledFuncLevelFunc(1.2, AtomicBombardPopulation),
			"WeaponBombardDamageQuality":        MakeScaledFuncLevelFunc(1.2, AtomicBombardQuality),
		},
	)

	DiplomatBombardWeapon = ComposeComponentStats(
		AtomicBombardWeapon,
		ComponentStats{
			"WeaponBombardDamageInfrastructure": MakeScaledFuncLevelFunc(1.3, AtomicBombardInfrastructure),
			"WeaponBombardDamageMilitary":       MakeScaledFuncLevelFunc(1.3, AtomicBombardMilitary),
			"WeaponBombardDamagePopulation":     MakeScaledFuncLevelFunc(1.3, AtomicBombardPopulation),
			"WeaponBombardDamageQuality":        MakeScaledFuncLevelFunc(1.3, AtomicBombardQuality),
		},
	)

	EclipseBombardWeaponEnergyPerShot = MakeEnergyPerShotFunc(DriverBombardEnergyRatio, EclipseBombardMilitary)
	EclipseBombardWeaponSpeed         = LargeKineticWeaponSpeed
	EclipseBombardRange               = LargeKineticWeaponRange
	EclipseBombardFireRate            = MakeFixedLevelFunc(DriverBombardFireRate)
	EclipseBombardInfrastructure      = MakeExpLevelFunc(DriverBombardDamageInfrastructure, WeaponDamageIncreaseExp)
	EclipseBombardMilitary            = MakeExpLevelFunc(DriverBombardDamageMilitary, WeaponDamageIncreaseExp)
	EclipseBombardPopulation          = MakeExpLevelFunc(DriverBombardDamagePopulation, WeaponDamageIncreaseExp)
	EclipseBombardQuality             = MakeExpLevelFunc(DriverBombardDamageQuality, WeaponDamageIncreaseExp)
	EclipseBombardWeapon              = ComposeComponentStats(
		AtomicBombardWeapon,
		ComponentStats{
			"WeaponRange":                       EclipseBombardRange,
			"WeaponSpeed":                       EclipseBombardWeaponSpeed,
			"WeaponFireRate":                    EclipseBombardFireRate,
			"WeaponEnergyPerShot":               EclipseBombardWeaponEnergyPerShot,
			"WeaponBombardDamageInfrastructure": MakeScaledFuncLevelFunc(1.2, EclipseBombardInfrastructure),
			"WeaponBombardDamageMilitary":       MakeScaledFuncLevelFunc(1.2, EclipseBombardMilitary),
			"WeaponBombardDamagePopulation":     MakeScaledFuncLevelFunc(1.2, EclipseBombardPopulation),
			"WeaponBombardDamageQuality":        MakeScaledFuncLevelFunc(1.2, EclipseBombardQuality),
		},
	)
)
