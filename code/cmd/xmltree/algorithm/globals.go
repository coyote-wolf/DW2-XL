package algorithm

import (
	"github.com/lucky-wolf/xml-tree/etc"
	"github.com/lucky-wolf/xml-tree/xmltree"
)

// we have some standard global functions defined here
// to allow ourselves to build specific schedules with
// references to common global schedules
//
// the aim is to allow some global basis for all of our data
// then we can make things relative to each other
// tweak some global base values, and you rearrange the entire web of dependencies

const (
	PlanetarySurfaceOffset = 4000 // simply the typical distance to near-abouts the surface of a colonizable world

	WeaponDamageBasis   = 10
	WeaponFireRateBasis = 10

	WeakArmorStrengthBasis  = 12.8 * WeaponDamageBasis // very loosely can take 16 shots from a 1.0 damage basis weapon
	WeakShieldStrengthBasis = 12.8 * WeaponDamageBasis

	// factor of shield strength that is recharged per second
	// you can see this as "fully recharged in how many seconds"
	// TODO: rather than tying this to total strength
	// todo: we need to tie to some relationship to dps
	// warn: basically, if dps > recharge_rate, attack will win eventually...
	// SlowShieldRechargeFactor     = 1. / 300.
	// StandardShieldRechargeFactor = 1. / 250.
	// FastShieldRechargeFactor     = 1. / 200.

	// this should likely be tied to dps somehow
	// for now it represents an absolute amount recharged per second
	SlowShieldRechargeBasis = .4

	ShieldStaticEnergyCoefficient                = 0.01 // energy relative to strength
	InefficientShieldRechargeEnergyCoefficient   = 15
	StandardShieldRechargeEnergyCoefficient      = 12
	EfficientShieldRechargeEnergyCoefficient     = 10
	VeryEfficientShieldRechargeEnergyCoefficient = 8
	WeaponEnergyRatioBasis                       = 2
	EngineEnergyRatioBasis                       = 2

	// overall growth of damage (compounding)
	// this gives us the growth curve for tech level over level (how does tech level gain over previous levels *N, *N^2, *N^3, etc.)
	WeaponDamageIncreaseExp = 1.05

	// slave armor and shield growth to the same growth
	ArmorStrengthIncreaseExp  = WeaponDamageIncreaseExp
	ShieldStrengthIncreaseExp = WeaponDamageIncreaseExp

	// note: ideally we'd connect this all together better than this (this is trial & error)
	ShieldResistanceBasis = 1.5 // shields have quantum amplifiers to increase this
	ArmorReactiveBasis    = 3.0 // armor doesn't have another component to amplify this, so we start higher

	// slave resistance and reactive to the same growth
	ArmorReactiveIncreaseExp    = WeaponDamageIncreaseExp
	ShieldResistanceIncreaseExp = WeaponDamageIncreaseExp

	DirectFireWeaponSize = 11
	SeekingWeaponSize    = 13
	BomberWeaponSize     = 10
	FighterWeaponSize    = 5

	DirectFirePointDefenseSize = 8
	SeekingPointDefenseSize    = 10

	PDTargetingBonus = 0.1
	PDRangeFactor    = 1.0
	PDFallOffFactor  = 1.0

	FighterWeaponTargetingBonus = 0.1
	FighterWeaponRangeFactor    = 0.5
	FighterWeaponFallOffFactor  = 2.0
	FighterDefenseScaleFactor   = 0.2
	FighterEngineThrustFactor   = 0.75 // was .5 (and .3 before that)

	FighterReactorFactor      = 0.15
	FighterWeaponEnergyFactor = 0.5
	FighterShieldEnergyFactor = 0.5
	FighterEngineEnergyFactor = 0.5
	FighterStaticEnergyFactor = 0.5
)

// name a bay category (general, weapon, etc.) and the count of bays it should contain
// this either deletes from the end of the list, or copies the last entry into more entries
// it does NOT adjust the size of bays - only copies the tail node to add more of that type
// SUBTLE: "Engine" bays are always an odd number by adding +1 bay if you ask for an even number
// subtle: this allows the game to always have a center position for a single engine / odd number of engines within the allowed count
type HullLevel = int
type ComponentType = string
type RoleName = string
type AttributeName = string
type BayCounts map[ComponentType]int
type BayCountsPerLevel map[HullLevel]BayCounts
type VisitorFunc = func(e *xmltree.XMLElement, logicalLevel int) (err error)

type StringLevelFunc = func(level int) string
type StringsTable = map[AttributeName]StringLevelFunc
type ValuesTable = map[AttributeName]LevelFunc
type RulesTable = []VisitorFunc
type RacialPostprocessTable = map[RaceID]VisitorFunc

type Tier = int
type HullTiers map[HullLevel]Tier
type HullTierFunc func(HullLevel) (Tier, bool)

type HullRoleDefinition struct {
	Tier             HullTierFunc // optional mapping of hull tier to logical level
	StringsTable     StringsTable
	ValuesTable      ValuesTable
	BonusesTable     ValuesTable
	BayCountsTable   ValuesTable
	AprioriBayCounts BayCountsPerLevel
	RulesTable       RulesTable // any additional visitors per ship hull
}
type HullBaySchedule map[RoleName]HullRoleDefinition

type BayTypeGroups map[ComponentType]BayTypeIndexes
type BayTypeIndexes struct {
	start int
	count int
}

// returns the tier (logical level) given a hull model level
func MakeTierLookupFunc(mapping HullTiers) HullTierFunc {
	return func(level HullLevel) (value Tier, ok bool) {
		value, ok = mapping[level]
		return
	}
}

// returns the tier (logical level) given a hull model level
func MakeTierIdentityFunc() HullTierFunc {
	return func(level HullLevel) (value Tier, ok bool) {
		return level, true
	}
}

// returns the tier (logical level) given a hull model level
func MakeTierLimitFunc(max int, f HullTierFunc) HullTierFunc {
	return func(level HullLevel) (value Tier, ok bool) {
		if level <= max {
			return f(level)
		}
		return -1, false
	}
}

type RaceID int

// note: we don't want to make a stringer, as sadly that interferes with csv and other encodings
// so we just have an explicit method to get the name
func (r RaceID) Name() string {
	if r < 0 || int(r) >= len(RaceNames) {
		return "Unknown"
	}
	return RaceNames[r]
}

const (
	Human     RaceID = 0
	Ackdarian RaceID = 1
	Teekan    RaceID = 2
	Haakonish RaceID = 3
	Mortalen  RaceID = 4
	Ikkuro    RaceID = 5
	Boskara   RaceID = 6
	Zenox     RaceID = 7
	Wekkarus  RaceID = 8
	Atuuk     RaceID = 9
	Dhayut    RaceID = 10
	Gizurean  RaceID = 11
	Ketarov   RaceID = 12
	Kiadian   RaceID = 13
	Naxxilian RaceID = 14
	Quameno   RaceID = 15
	Securan   RaceID = 16
	Shandar   RaceID = 17
	Sluken    RaceID = 18
	Ugnari    RaceID = 19
	Shakturi  RaceID = 20
	Guardian  RaceID = 21
)

var RaceNames = []string{
	"Human",
	"Ackdarian",
	"Teekan",
	"Haakonish",
	"Mortalen",
	"Ikkuro",
	"Boskara",
	"Zenox",
	"Wekkarus",
	"Atuuk",
	"Dhayut",
	"Gizurean",
	"Ketarov",
	"Kiadian",
	"Naxxilian",
	"Quameno",
	"Securan",
	"Shandar",
	"Sluken",
	"Ugnari",
	"Shakturi",
	"Guardian",
}

var (
	// required order of component bays types
	componentBayOrder        = []ComponentType{"Weapon", "Engine", "Hangar", "Sensor", "Defense", "General"}
	reverseComponentBayOrder = etc.Reverse(componentBayOrder)
)

type ShipHullSummary struct {
	ShipHullId  int
	Name        string
	RaceId      RaceID
	RaceName    string
	Role        string
	Level       int
	engineLimit int
	Sensor      int
	Hangar      int
	Defense     int
	Weapon      int
	General     int
	Engine      int
}

func ShipWeaponCrewRequirements(size int) int {
	switch {
	case size <= SeekingWeaponSize:
		return 5
	case size <= 2*SeekingWeaponSize:
		return 8
	default:
		return 12
	}
}

var (
	SmallCrewRequirements     = MakeFixedLevelFunc(5)
	MediumCrewRequirements    = MakeFixedLevelFunc(8)
	LargeCrewRequirements     = MakeFixedLevelFunc(12)
	PlanetaryCrewRequirements = MakeFixedLevelFunc(48) // doesn't hurt - but doesn't show up in game either
)

var (
	SmallWeaponStaticEnergy     = MakeFixedLevelFunc(1)
	MediumWeaponStaticEnergy    = MakeFixedLevelFunc(2)
	LargeWeaponStaticEnergy     = MakeFixedLevelFunc(4)
	PlanetaryWeaponStaticEnergy = MakeFixedLevelFunc(16)
)

// standard weapon countermeasure schedule (by tech level)
var SeekingComponentCountermeasuresBonus = MakeLinearLevelFunc(0.5, .02)
var DirectFireComponentCountermeasuresBonus = MakeLinearLevelFunc(0.58, 0.02)
var StandardBeamCountermeasuresBonus = MakeLinearLevelFunc(0.68, 0.02)

// 6, 12, 24, 36, 48, 60, 72, 84, 96, 108, 120
func IonWeaponComponentDamage(level int) float64 {
	// treat level 0 and 1 as the same for our purposes (currently)
	switch level {
	case 0:
		return 6
	default:
		return float64(level) * 12
	}
}

// 0,  8, 16, 24, 32
func IonShieldIonDamageDefense(level int) float64 {
	return 8 * float64(level)
}

// standard component Ion defense
func StandardComponentIonDefense(level int) float64 {
	return float64(level + 1)
}

// hardened component Ion defense
func HardenedComponentIonDefense(level int) float64 {
	return StandardComponentIonDefense(level) * 2
}
