package algorithm

func Engines(folder string) (err error) {

	// load all component definition files
	j, err := LoadJobFor(folder, "ComponentDefinitions*.xml")
	if err != nil {
		return
	}

	// update kinetic weapons
	err = j.applyEngines()
	if err != nil {
		return
	}

	// update derivatives
	err = j.applyFighterEngines()
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

func (j *Job) applyEngines() (err error) {

	// apply stats for each component
	err = j.ApplyComponentAll(StarshipEngineData)

	return
}

const (
	EngineIncreaseExp       = 1.15                          // compounding increase (level over level)
	TurboEfficiencyRatio    = 0.66666                       // 33% reduced energy cost
	MaxThrustRatio          = 1.33333                       // combat only
	CruiseThrustEnergyRatio = 0.0001                        // 1/10K
	MaxThrustEnergyRatio    = 1.5 * CruiseThrustEnergyRatio // also scaled by output
	VectorThrustEnergyRatio = 0.01                          // vector energy is 1/100
)

var (
	StarshipEngineData = ComponentLevelDataMap{
		"Engines, Ion": {
			minLevel:       0,
			maxLevel:       1,
			componentStats: IonStarshipEngineComponentStats,
		},
		"Engines, Pulsed Ion": {
			minLevel:       2,
			maxLevel:       5,
			componentStats: PulsedIonEngineComponentStats,
		},
		"Engines, Compact Ion": {
			minLevel:       2,
			maxLevel:       5,
			componentStats: CompactEngineComponentStats,
		},
		"Engines, Acceleros": {
			minLevel:       2,
			maxLevel:       5,
			componentStats: AccelerosEngineComponentStats,
		},
		"Engines, Turbo Thruster": {
			minLevel:       2,
			maxLevel:       10,
			componentStats: TurboThrusterEngineComponentStats,
		},
		"Engines, Vortex": {
			minLevel:       6,
			maxLevel:       10,
			componentStats: VortexEngineComponentStats,
		},
		"Engines, Singularity": {
			minLevel:       10,
			maxLevel:       10,
			componentStats: InfiniteFluxEngineComponentStats,
		},
		"Engines, Inertialess": {
			minLevel:       10,
			maxLevel:       10,
			componentStats: InertialessEngineComponentStats,
		},

		// Hive uses 3 levels, we make them mid-game
		"Engines, Hive": {
			minLevel:       3,
			maxLevel:       5,
			componentStats: HiveEngineComponentStats,
		},
	}

	// Cruise Thrust
	WeakStarshipCruiseThrust   = MakeScaledFuncLevelFunc(0.66666, MediumStarshipCruiseThrust)
	MediumStarshipCruiseThrust = MakeExpLevelFunc(12000, EngineIncreaseExp)
	StrongStarshipCruiseThrust = MakeScaledFuncLevelFunc(1.5, MediumStarshipCruiseThrust)

	WeakStarshipCruiseThrustEnergy   = MakeThrustEnergyUsageFunc(CruiseThrustEnergyRatio, WeakStarshipCruiseThrust)
	MediumStarshipCruiseThrustEnergy = MakeThrustEnergyUsageFunc(CruiseThrustEnergyRatio, MediumStarshipCruiseThrust)
	StrongStarshipCruiseThrustEnergy = MakeThrustEnergyUsageFunc(CruiseThrustEnergyRatio, StrongStarshipCruiseThrust)

	// Max Thrust
	WeakStarshipMaxThrust   = MakeScaledFuncLevelFunc(MaxThrustRatio, WeakStarshipCruiseThrust)
	MediumStarshipMaxThrust = MakeScaledFuncLevelFunc(MaxThrustRatio, MediumStarshipCruiseThrust)
	StrongStarshipMaxThrust = MakeScaledFuncLevelFunc(MaxThrustRatio, StrongStarshipCruiseThrust)

	WeakStarshipMaxThrustEnergy   = MakeThrustEnergyUsageFunc(MaxThrustEnergyRatio, WeakStarshipMaxThrust)
	MediumStarshipMaxThrustEnergy = MakeThrustEnergyUsageFunc(MaxThrustEnergyRatio, MediumStarshipMaxThrust)
	StrongStarshipMaxThrustEnergy = MakeThrustEnergyUsageFunc(MaxThrustEnergyRatio, StrongStarshipMaxThrust)

	// Vectoring Thrust
	WeakStarshipVectorThrust   = MakeScaledFuncLevelFunc(.666666, MediumStarshipVectorThrust)
	MediumStarshipVectorThrust = MakeExpLevelFunc(60, EngineIncreaseExp)
	StrongStarshipVectorThrust = MakeScaledFuncLevelFunc(1.5, MediumStarshipVectorThrust)

	WeakStarshipVectorEnergy   = MakeThrustEnergyUsageFunc(VectorThrustEnergyRatio, WeakStarshipVectorThrust)
	MediumStarshipVectorEnergy = MakeThrustEnergyUsageFunc(VectorThrustEnergyRatio, MediumStarshipVectorThrust)
	StrongStarshipVectorEnergy = MakeThrustEnergyUsageFunc(VectorThrustEnergyRatio, StrongStarshipVectorThrust)

	// Vectoring gives countermeasure bonus
	WeakStarshipEngineCountermeasureBonus   = MakeOffsetFuncLevelFunc(-2, StrongStarshipEngineCountermeasureBonus)
	MediumStarshipEngineCountermeasureBonus = MakeOffsetFuncLevelFunc(-1, StrongStarshipEngineCountermeasureBonus)
	StrongStarshipEngineCountermeasureBonus = MakeLinearLevelFunc(0.03, 0.03)

	StarshipEngineBaseStats = ComponentStats{
		"ComponentIonDefense": HardenedComponentIonDefense, // engines are a hardened component
		"CrewRequirement":     MakeFixedLevelFunc(5),
		"StaticEnergyUsed":    MakeFixedLevelFunc(1),
	}

	WeakStarshipEngineThrust = ComponentStats{
		"EngineMainCruiseThrust":             WeakStarshipCruiseThrust,
		"EngineMainCruiseThrustEnergyUsage":  WeakStarshipCruiseThrustEnergy,
		"EngineMainMaximumThrust":            WeakStarshipMaxThrust,
		"EngineMainMaximumThrustEnergyUsage": WeakStarshipMaxThrustEnergy,
		"StaticEnergyUsed":                   MakeFixedLevelFunc(0.5),
	}

	MediumStarshipEngineThrust = ComponentStats{
		"EngineMainCruiseThrust":             MediumStarshipCruiseThrust,
		"EngineMainCruiseThrustEnergyUsage":  MediumStarshipCruiseThrustEnergy,
		"EngineMainMaximumThrust":            MediumStarshipMaxThrust,
		"EngineMainMaximumThrustEnergyUsage": MediumStarshipMaxThrustEnergy,
		"StaticEnergyUsed":                   MakeFixedLevelFunc(1.0),
	}

	StrongStarshipEngineThrust = ComponentStats{
		"EngineMainCruiseThrust":             StrongStarshipCruiseThrust,
		"EngineMainCruiseThrustEnergyUsage":  StrongStarshipCruiseThrustEnergy,
		"EngineMainMaximumThrust":            StrongStarshipMaxThrust,
		"EngineMainMaximumThrustEnergyUsage": StrongStarshipMaxThrustEnergy,
		"StaticEnergyUsed":                   MakeFixedLevelFunc(2.0),
	}

	WeakStarshipEngineVector = ComponentStats{
		"CountermeasuresBonus":       WeakStarshipEngineCountermeasureBonus,
		"EngineVectoringThrust":      WeakStarshipVectorThrust,
		"EngineVectoringEnergyUsage": WeakStarshipVectorEnergy,
	}

	MediumStarshipEngineVector = ComponentStats{
		"CountermeasuresBonus":       MediumStarshipEngineCountermeasureBonus,
		"EngineVectoringThrust":      MediumStarshipVectorThrust,
		"EngineVectoringEnergyUsage": MediumStarshipVectorEnergy,
	}

	StrongStarshipEngineVector = ComponentStats{
		"EngineVectoringThrust":      StrongStarshipVectorThrust,
		"EngineVectoringEnergyUsage": StrongStarshipVectorEnergy,
		"CountermeasuresBonus":       StrongStarshipEngineCountermeasureBonus,
	}

	// basic
	IonStarshipEngineComponentStats = ComposeComponentStats(
		StarshipEngineBaseStats,
		WeakStarshipEngineThrust,
		WeakStarshipEngineVector,
	)

	// nimble
	PulsedIonEngineComponentStats = ComposeComponentStats(
		StarshipEngineBaseStats,
		MediumStarshipEngineThrust,
		StrongStarshipEngineVector,
	)

	// compact
	CompactEngineComponentStats = ComposeComponentStats(
		StarshipEngineBaseStats,
		WeakStarshipEngineThrust,
		WeakStarshipEngineVector,
	)

	// acceleros
	AccelerosEngineComponentStats = ComposeComponentStats(
		StarshipEngineBaseStats,
		StrongStarshipEngineThrust,
		MediumStarshipEngineVector,
	)

	// hive
	HiveEngineComponentStats = ComposeComponentStats(
		StarshipEngineBaseStats,
		StrongStarshipEngineThrust,
		WeakStarshipEngineVector,
	)

	// powerful, nimble, efficient
	TurboThrusterEngineComponentStats = ComposeComponentStats(
		StarshipEngineBaseStats,
		ComponentStats{
			"EngineMainCruiseThrust":             StrongStarshipCruiseThrust,
			"EngineMainCruiseThrustEnergyUsage":  MakeScaledFuncLevelFunc(TurboEfficiencyRatio, StrongStarshipCruiseThrustEnergy),
			"EngineMainMaximumThrust":            StrongStarshipMaxThrust,
			"EngineMainMaximumThrustEnergyUsage": MakeScaledFuncLevelFunc(TurboEfficiencyRatio, StrongStarshipMaxThrustEnergy),
			"EngineVectoringThrust":              StrongStarshipVectorThrust,
			"EngineVectoringEnergyUsage":         MakeScaledFuncLevelFunc(TurboEfficiencyRatio, StrongStarshipVectorEnergy),
			"CountermeasuresBonus":               StrongStarshipEngineCountermeasureBonus,
		},
	)

	// strong at everything (for smallest size)
	VortexEngineComponentStats = ComposeComponentStats(
		StarshipEngineBaseStats,
		StrongStarshipEngineThrust,
		StrongStarshipEngineVector,
	)

	// super engine
	InfiniteFluxEngineComponentStats = VortexEngineComponentStats

	// super vector thurster
	InertialessEngineComponentStats = VortexEngineComponentStats
)
