package algorithm

func IonWeapons(folder string) (err error) {

	// load all component definition files
	j, err := LoadJobFor(folder, "ComponentDefinitions*.xml")
	if err != nil {
		return
	}

	// apply this transformation
	err = j.applyIonWeapons()
	if err != nil {
		return
	}

	// update derivatives
	err = j.applyFighterWeaponsAndPD()
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

func (j *Job) applyIonWeapons() (err error) {

	// apply stats for each component
	err = j.ApplyComponentAll(IonComponentData)

	return
}

const (
	IonBaseFireRate          = WeaponFireRateBasis // ion is at our basis
	IonWeaponDamageFactor    = 0.8                 // ion weapons do ~80% raw damage vs. other direct fire weapons
	IonDirectFireEnergyRatio = 1.0                 // ion weapons are .8 dmg, so this is effectively .8 energy as well
	IonFtrPDScaleFactor      = 0.75
	IonArmorBypass           = 0
	IonShieldBypass          = 0
)

var (
	IonComponentData = ComponentLevelDataMap{
		"Ion Field Projector [S]": {
			minLevel:       0,
			maxLevel:       1,
			componentStats: SmallIonCannon,
		},
		"Ion Cannon [S]": {
			minLevel:       2,
			maxLevel:       5,
			componentStats: SmallIonCannon,
		},
		"Ion Cannon [M]": {
			minLevel:       3,
			maxLevel:       5,
			componentStats: MediumIonCannon,
		},

		"Ion Cannon, Rapid [S]": {
			minLevel:       6,
			maxLevel:       10,
			componentStats: RapidIonCannon,
		},

		"Ion Cannon, Heavy [M]": {
			minLevel:       6,
			maxLevel:       10,
			componentStats: MediumHeavyIonCannon,
		},
		"Ion Cannon, Heavy [L]": {
			minLevel:       6,
			maxLevel:       10,
			componentStats: LargeHeavyIonCannon,
		},

		"Ion Lance [L]": {
			minLevel:       4,
			maxLevel:       7,
			componentStats: EMLance,
		},
		"Ion Lance, Wave [L]": {
			minLevel:       8,
			maxLevel:       10,
			componentStats: EMWaveLance,
		},

		"Ion Bomb [M]": {
			minLevel:       3,
			maxLevel:       6,
			componentStats: MediumIonBomb,
		},
		"Ion Bomb [L]": {
			minLevel:       3,
			maxLevel:       6,
			componentStats: LargeIonBomb,
		},

		"Ion Pulse [M]": {
			minLevel:       7,
			maxLevel:       10,
			componentStats: MediumIonBomb,
		},
		"Ion Pulse [L]": {
			minLevel:       7,
			maxLevel:       10,
			componentStats: LargeIonBomb,
		},

		"Missile, Ion [M]": {
			minLevel:       2,
			maxLevel:       5,
			componentStats: IonMissileMedium,
		},
		"Missile, Advanced Ion [M]": {
			minLevel:       6,
			maxLevel:       9,
			componentStats: AdvIonMissile,
		},
		"Missile, Ultra Ion [M]": {
			minLevel:       10,
			maxLevel:       10,
			componentStats: UltraIonMissile,
		},

		"Massive Ion Cannon Battery": {
			minLevel:       3,
			maxLevel:       9,
			componentStats: PlanetaryHeavyIonCannon,
		},
	}

	IonWeaponRawDamage     = MakeDamageFunc(IonBaseFireRate, IonWeaponDamageFactor)
	IonWeaponEnergyPerShot = MakeEnergyPerShotFunc(IonDirectFireEnergyRatio, IonWeaponRawDamage)
	IonWeaponFireRate      = MakeFixedLevelFunc(IonBaseFireRate)

	IonWeaponArmorBypass  = MakeFixedLevelFunc(IonArmorBypass)
	IonWeaponShieldBypass = MakeFixedLevelFunc(IonShieldBypass)

	// WARN! please keep the multipliers of IonComponentDamage to a MINIMUM
	// HACK: everything is scaled off of this value - any increase means at equal tech with FULL defense, that "extra" amount will always get through
	// hack: and MOST EVERYTHING in the game will NOT have MAX ION DEFENSE!

	// note: ion weapons never have any bombard value (lighting in atmosphere is not a real issue)
	IonFieldProjector = ComposeComponentStats(
		SmallWeaponBaseStats,
		WeaponNoAOE,
		WeaponNoBombard,
		ComponentStats{
			"ComponentCountermeasuresBonus": DirectFireComponentCountermeasuresBonus,
			"ComponentTargetingBonus":       MakeFixedLevelFunc(0),
			"WeaponArmorBypass":             IonWeaponArmorBypass,
			"WeaponShieldBypass":            IonWeaponShieldBypass,
			"WeaponSpeed":                   StandardBlasterWeaponSpeed,
			"WeaponRange":                   StandardBlasterWeaponRange,
			"WeaponDamageFalloffRatio":      StandardBlasterWeaponFallOff,
			"WeaponEnergyPerShot":           IonWeaponEnergyPerShot,
			"WeaponFireRate":                IonWeaponFireRate,
			"WeaponRawDamage":               IonWeaponRawDamage,
			"WeaponVolleyAmount":            MakeFixedLevelFunc(1),
			"WeaponVolleyFireRate":          MakeFixedLevelFunc(0),
			"WeaponIonEngineDamage":         IonWeaponComponentDamage,
			"WeaponIonHyperDriveDamage":     IonWeaponComponentDamage,
			"WeaponIonSensorDamage":         IonWeaponComponentDamage,
			"WeaponIonShieldDamage":         IonWeaponComponentDamage,
			"WeaponIonWeaponDamage":         IonWeaponComponentDamage,
			"WeaponIonGeneralDamage":        IonWeaponComponentDamage,
		},
	)
	SmallIonCannon = IonFieldProjector

	// [M] is simply 2x fire [S]
	MediumIonCannon = DeriveMediumWeaponUsing2xVolleyRate(SmallIonCannon, 1)

	// rapid is simply 1.25 rof
	RapidIonCannon = ComposeComponentStats(
		IonFieldProjector,
		ComponentStats{
			"WeaponFireRate": MakeScaledFuncLevelFunc(0.8, IonWeaponFireRate), // 25% faster, but no loss of ion output
		},
	)

	// heavy medium is more powerful per shot instead of double shot
	MediumHeavyIonCannon = ComposeComponentStats(
		DeriveMediumWeaponUsing2xDamage(SmallIonCannon),
		ComponentStats{
			"WeaponIonEngineDamage":     MakeScaledFuncLevelFunc(1.1, IonWeaponComponentDamage),
			"WeaponIonHyperDriveDamage": MakeScaledFuncLevelFunc(1.1, IonWeaponComponentDamage),
			"WeaponIonSensorDamage":     MakeScaledFuncLevelFunc(1.1, IonWeaponComponentDamage),
			"WeaponIonShieldDamage":     MakeScaledFuncLevelFunc(1.1, IonWeaponComponentDamage),
			"WeaponIonWeaponDamage":     MakeScaledFuncLevelFunc(1.1, IonWeaponComponentDamage),
			"WeaponIonGeneralDamage":    MakeScaledFuncLevelFunc(1.1, IonWeaponComponentDamage),
		},
	)

	// heavy large is double heavy medium
	LargeHeavyIonCannon = ComposeComponentStats(
		DeriveLargeWeaponUsing4xDamage(SmallIonCannon),
		ComponentStats{
			"WeaponIonEngineDamage":     MakeScaledFuncLevelFunc(1.2, IonWeaponComponentDamage),
			"WeaponIonHyperDriveDamage": MakeScaledFuncLevelFunc(1.2, IonWeaponComponentDamage),
			"WeaponIonSensorDamage":     MakeScaledFuncLevelFunc(1.2, IonWeaponComponentDamage),
			"WeaponIonShieldDamage":     MakeScaledFuncLevelFunc(1.2, IonWeaponComponentDamage),
			"WeaponIonWeaponDamage":     MakeScaledFuncLevelFunc(1.2, IonWeaponComponentDamage),
			"WeaponIonGeneralDamage":    MakeScaledFuncLevelFunc(1.2, IonWeaponComponentDamage),
		},
	)

	// planetary large is based on large heavy cannon
	PlanetaryHeavyIonCannon = ComposeComponentStats(
		DerivePlanetaryFromLargeWeapon(LargeHeavyIonCannon),
		ComponentStats{
			"WeaponIonEngineDamage":     MakeScaledFuncLevelFunc(1.33333, IonWeaponComponentDamage),
			"WeaponIonHyperDriveDamage": MakeScaledFuncLevelFunc(1.33333, IonWeaponComponentDamage),
			"WeaponIonSensorDamage":     MakeScaledFuncLevelFunc(1.33333, IonWeaponComponentDamage),
			"WeaponIonShieldDamage":     MakeScaledFuncLevelFunc(1.33333, IonWeaponComponentDamage),
			"WeaponIonWeaponDamage":     MakeScaledFuncLevelFunc(1.33333, IonWeaponComponentDamage),
			"WeaponIonGeneralDamage":    MakeScaledFuncLevelFunc(1.33333, IonWeaponComponentDamage),
		},
	)

	// Lance is slower with bigger per shot values, some targeting bonus, good range
	EMLance = ComposeComponentStats(
		SmallIonCannon,
		LargeWeaponBaseStats,
		ComponentStats{
			"ComponentTargetingBonus":   StandardBeamTargetingBonus,
			"WeaponFireRate":            StandardBeamWeaponFireRate,
			"WeaponSpeed":               StandardBeamWeaponSpeed,
			"WeaponRange":               StandardBeamWeaponRange,
			"WeaponDamageFalloffRatio":  StandardBeamFalloffRatio,
			"WeaponRawDamage":           MakeScaledFuncLevelFunc(4, IonWeaponRawDamage),
			"WeaponEnergyPerShot":       MakeScaledFuncLevelFunc(4, IonWeaponEnergyPerShot),
			"WeaponIonEngineDamage":     MakeScaledFuncLevelFunc(1.2, IonWeaponComponentDamage),
			"WeaponIonHyperDriveDamage": MakeScaledFuncLevelFunc(1.2, IonWeaponComponentDamage),
			"WeaponIonSensorDamage":     MakeScaledFuncLevelFunc(1.2, IonWeaponComponentDamage),
			"WeaponIonShieldDamage":     MakeScaledFuncLevelFunc(1.2, IonWeaponComponentDamage),
			"WeaponIonWeaponDamage":     MakeScaledFuncLevelFunc(1.2, IonWeaponComponentDamage),
			"WeaponIonGeneralDamage":    MakeScaledFuncLevelFunc(1.2, IonWeaponComponentDamage),
		},
	)

	// Wave Lance has more armor bypass
	EMWaveLance = ComposeComponentStats(
		EMLance,
		ComponentStats{
			"WeaponArmorBypass": MakeFixedLevelFunc(0.33333), // std +25
		},
	)

	// Bombs are like aoe torpedoes
	IonBombFireRate            = 2. * WeaponFireRateBasis
	IonBombWeaponFireRate      = MakeFixedLevelFunc(IonBombFireRate)
	IonBombWeaponDamage        = MakeDamageFunc(IonBombFireRate, 1.0)
	IonBombWeaponEnergyPerShot = MakeEnergyPerShotFunc(IonDirectFireEnergyRatio, ImpactBlasterWeaponDamage)
	MediumIonBomb              = ComposeComponentStats(
		WeaponNoBombard,
		MediumWeaponBaseStats,
		ComponentStats{
			"ComponentCountermeasuresBonus": SeekingComponentCountermeasuresBonus,
			"ComponentTargetingBonus":       MakeFixedLevelFunc(0.2),
			"WeaponAreaEffectRange":         MakeLinearLevelFunc(150, 25),
			"WeaponAreaBlastWaveSpeed":      MakeFixedLevelFunc(300),
			"WeaponArmorBypass":             IonWeaponArmorBypass,
			"WeaponShieldBypass":            IonWeaponShieldBypass,
			"WeaponSpeed":                   EpsilonTorpedoWeaponSpeed,
			"WeaponRange":                   StandardTorpedoWeaponRange,
			"WeaponDamageFalloffRatio":      MakeFixedLevelFunc(0.15),
			"WeaponFireRate":                IonBombWeaponFireRate,
			"WeaponRawDamage":               IonBombWeaponDamage,
			"WeaponEnergyPerShot":           IonBombWeaponEnergyPerShot,
			"WeaponIonEngineDamage":         MakeScaledFuncLevelFunc(1.1, IonWeaponComponentDamage),
			"WeaponIonHyperDriveDamage":     MakeScaledFuncLevelFunc(1.1, IonWeaponComponentDamage),
			"WeaponIonSensorDamage":         MakeScaledFuncLevelFunc(1.1, IonWeaponComponentDamage),
			"WeaponIonShieldDamage":         MakeScaledFuncLevelFunc(1.1, IonWeaponComponentDamage),
			"WeaponIonWeaponDamage":         MakeScaledFuncLevelFunc(1.1, IonWeaponComponentDamage),
			"WeaponIonGeneralDamage":        MakeScaledFuncLevelFunc(1.1, IonWeaponComponentDamage),
		},
	)
	LargeIonBomb = ComposeComponentStats(
		MediumIonBomb,
		LargeWeaponBaseStats,
		ComponentStats{
			"WeaponRawDamage":           MakeScaledFuncLevelFunc(4, IonWeaponRawDamage),
			"WeaponEnergyPerShot":       MakeScaledFuncLevelFunc(4, IonWeaponEnergyPerShot),
			"WeaponIonEngineDamage":     MakeScaledFuncLevelFunc(1.2, IonWeaponComponentDamage),
			"WeaponIonHyperDriveDamage": MakeScaledFuncLevelFunc(1.2, IonWeaponComponentDamage),
			"WeaponIonSensorDamage":     MakeScaledFuncLevelFunc(1.2, IonWeaponComponentDamage),
			"WeaponIonShieldDamage":     MakeScaledFuncLevelFunc(1.2, IonWeaponComponentDamage),
			"WeaponIonWeaponDamage":     MakeScaledFuncLevelFunc(1.2, IonWeaponComponentDamage),
			"WeaponIonGeneralDamage":    MakeScaledFuncLevelFunc(1.2, IonWeaponComponentDamage),
		},
	)

	// ion missiles
	IonMissileFireRate            = 0.5 * MissileFireRate
	IonMissileVolleyAmount        = 1.0
	IonMissileVolleyFireRate      = 0.0
	IonMissileWeaponDamage        = MakeMultiSalvoWeaponDamageFunc(IonMissileFireRate, IonMissileVolleyAmount, IonWeaponDamageFactor*2) // double the damage because this is medium weapon!
	IonMissileWeaponEnergyPerShot = MakeEnergyPerShotFunc(MissileEnergyRatio, IonMissileWeaponDamage)
	IonMissileMedium              = ComposeComponentStats(
		SeekingMissileWeaponSmall,
		MediumWeaponBaseStats,
		ComponentStats{
			"WeaponFireRate":            MakeFixedLevelFunc(IonMissileFireRate),
			"WeaponRawDamage":           IonMissileWeaponDamage,
			"WeaponEnergyPerShot":       IonMissileWeaponEnergyPerShot,
			"WeaponRange":               MakeLinearLevelFunc(0.75*MissileRange, 0.5*MissileRangeGrowthRate), // reduced range due to ion energy upkeep
			"WeaponVolleyAmount":        MakeFixedLevelFunc(IonMissileVolleyAmount),
			"WeaponVolleyFireRate":      MakeFixedLevelFunc(IonMissileVolleyFireRate),
			"WeaponIonEngineDamage":     MakeScaledFuncLevelFunc(1.1, IonWeaponComponentDamage),
			"WeaponIonHyperDriveDamage": MakeScaledFuncLevelFunc(1.1, IonWeaponComponentDamage),
			"WeaponIonSensorDamage":     MakeScaledFuncLevelFunc(1.1, IonWeaponComponentDamage),
			"WeaponIonShieldDamage":     MakeScaledFuncLevelFunc(1.1, IonWeaponComponentDamage),
			"WeaponIonWeaponDamage":     MakeScaledFuncLevelFunc(1.1, IonWeaponComponentDamage),
			"WeaponIonGeneralDamage":    MakeScaledFuncLevelFunc(1.1, IonWeaponComponentDamage),
		},
	)

	AdvIonMissile = IonMissileMedium

	UltraIonMissile = AdvIonMissile
)
