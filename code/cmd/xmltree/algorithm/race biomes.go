package algorithm

// race biomes builds racial biome affinity data

type BiomeID int

const (
	// Habitable Planet Types
	Temperate_Continental BiomeID = 7
	Marshy_Swamp          BiomeID = 8
	Tropical_Ocean        BiomeID = 9
	Sandy_Desert          BiomeID = 10
	Snowball              BiomeID = 11
	Rocky_Volcanic        BiomeID = 12
	Tropical_Continental  BiomeID = 17
	Grasslands            BiomeID = 18
	Rocky_Desert          BiomeID = 19
	Savanna               BiomeID = 20
	Tundra                BiomeID = 21
	Glaciated             BiomeID = 22
	Temperate_Ocean       BiomeID = 23
	Mangrove_Swamp        BiomeID = 27
	Carbonaceous_Hothouse BiomeID = 29
	Sulphur_Volcanic      BiomeID = 30
)

type RacialSuitability map[RaceID]RacialAffinity
type RacialAffinity struct {
	Basis  SuitabilityModifiers
	Native SuitabilityModifiers
}
type SuitabilityModifiers = map[BiomeID]float64

var (
	SwampAffinity = SuitabilityModifiers{
		Marshy_Swamp:   1.0,
		Mangrove_Swamp: 1.0,
	}

	RacialSuitabilities = RacialSuitability{
		Haakonish: RacialAffinity{
			Basis: SwampAffinity,
			Native: SuitabilityModifiers{
				Marshy_Swamp:   0.1,
				Mangrove_Swamp: 0.05,
			},
		},
		Quameno: RacialAffinity{
			Basis: SwampAffinity,
			Native: SuitabilityModifiers{
				Marshy_Swamp:   0.05,
				Mangrove_Swamp: 0.1,
			},
		},
	}
)
