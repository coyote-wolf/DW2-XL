package algorithm

func BeamWeapons(folder string) (err error) {

	// load all component definition files
	j, err := LoadJobFor(folder, "ComponentDefinitions*.xml")
	if err != nil {
		return
	}

	// update beam weapons
	err = j.applyBeamWeapons()
	if err != nil {
		return
	}

	// update derivatives
	err = j.applyFighterWeaponsAndPD()
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

func (j *Job) applyBeamWeapons() (err error) {

	// apply stats for each component
	err = j.ApplyComponentAll(BeamWeaponData)

	return
}

const (
	// "light speed"
	BeamWeaponSpeed = 5000

	// all beams have a single constant fall-off
	BeamFalloff = .125

	// beams are the very definition of the standard damage
	BeamDamageFactor             = 1.0
	ThuonBeamDamageFactor        = 1.1
	ShatterforceBeamDamageFactor = 1.2
	TunnelingBeamDamageFactor    = 1.0 // Tunneling beams bypass shields, so are at -25% damage compared to omega (they approximately ignore 1/3 of ship defenses)
	PiercingBeamDamageFactor     = 1.0 // Piercing beams bypass armor, so are at -25% damage compared to omega (they approximately ignore 1/3 of ship defenses)
	OmegaBeamDamageFactor        = 1.3
	StarBeamDamageFactor         = 1.4 // high damage, but bypasses shields
	FusionBeamDamageFactor       = 1.4 // high damage, but bypasses armor

	// beams are seriously energy-hungry
	// energy input : damage output ratio
	BeamEnergyRatio             = 1.3
	ThuonBeamEnergyRatio        = 1.2
	ShatterforceBeamEnergyRatio = 1.1
	TunnelingBeamEnergyRatio    = 1.2
	PiercingBeamEnergyRatio     = 1.2
	OmegaBeamEnergyRatio        = 1.1
	StarBeamEnergyRatio         = .9
	FusionBeamEnergyRatio       = .7

	BaseBeamFireRate         = 1.1 * WeaponFireRateBasis
	ThuonBeamFireRate        = BaseBeamFireRate
	ShatterforceBeamFireRate = BaseBeamFireRate
	TunnelingBeamFireRate    = BaseBeamFireRate
	PiercingBeamFireRate     = BaseBeamFireRate
	OmegaBeamFireRate        = BaseBeamFireRate
	StarBeamFireRate         = BaseBeamFireRate
	FusionBeamFireRate       = BaseBeamFireRate

	// Tunneling beams bypass shields
	TunnelingBeamShieldBypass     = 0.4
	TunnelingBeamShieldBypassIncr = 0.2
	TunnelingBeamArmorBypass      = 0.0

	// Piercing beams bypass armor
	PiercingBeamShieldBypass    = 0.0
	PiercingBeamArmorBypass     = 0.4
	PiercingBeamArmorBypassIncr = 0.2

	// omega beams have no bypass - they're straight up damage
	OmegaBeamShieldBypass = 0
	OmegaBeamArmorBypass  = 0

	// star beams bypass shields
	StarBeamShieldBypass = 0.06944444444444444444444444444444 // gives 0.1 @ t2 for 20% growth rate
	StarBeamArmorBypass  = 0.0

	// fusion beams bypass armor
	FusionBeamShieldBypass = 0.0
	FusionBeamArmorBypass  = 0.06944444444444444444444444444444

	BeamBypassGrowthRate = 1.2 // 20% level over level

	Beam1stGenLevel = 2
	Beam2ndGenLevel = 4
	Beam3rdGenLevel = 7
)

var (
	BeamWeaponData = ComponentLevelDataMap{
		"Beam, Particle [S]": {
			minLevel:       0,
			maxLevel:       Beam1stGenLevel - 1,
			componentStats: ParticleBeamWeaponSmall,
		},
		"Beam, Particle [M]": {
			minLevel:       0,
			maxLevel:       Beam1stGenLevel - 1,
			componentStats: ParticleBeamWeaponMedium,
		},

		"Beam, Thuon [S]": {
			minLevel:       Beam1stGenLevel,
			maxLevel:       Beam2ndGenLevel - 1,
			componentStats: ThuonBeamWeaponSmall,
		},
		"Beam, Thuon [M]": {
			minLevel:       Beam1stGenLevel,
			maxLevel:       Beam2ndGenLevel - 1,
			componentStats: ThuonBeamWeaponMedium,
		},

		"Beam, Shatterforce [S]": {
			minLevel:       Beam2ndGenLevel,
			maxLevel:       Beam3rdGenLevel - 1,
			componentStats: ShatterforceBeamWeaponSmall,
		},
		"Beam, Shatterforce [M]": {
			minLevel:       Beam2ndGenLevel,
			maxLevel:       Beam3rdGenLevel - 1,
			componentStats: ShatterforceBeamWeaponMedium,
		},
		"Beam, Shatterforce [L]": {
			minLevel:       Beam2ndGenLevel,
			maxLevel:       Beam3rdGenLevel - 1,
			componentStats: ShatterforceBeamWeaponLarge,
		},

		"Beam, Piercing [S]": {
			minLevel:       Beam3rdGenLevel,
			maxLevel:       10,
			componentStats: PiercingBeamWeaponSmall,
		},
		"Beam, Piercing [M]": {
			minLevel:       Beam3rdGenLevel,
			maxLevel:       10,
			componentStats: PiercingBeamWeaponMedium,
		},
		"Beam, Piercing [L]": {
			minLevel:       Beam3rdGenLevel,
			maxLevel:       10,
			componentStats: PiercingBeamWeaponLarge,
		},

		"Beam, Tunneling [S]": {
			minLevel:       Beam3rdGenLevel,
			maxLevel:       10,
			componentStats: TunnelingBeamWeaponSmall,
		},
		"Beam, Tunneling [M]": {
			minLevel:       Beam3rdGenLevel,
			maxLevel:       10,
			componentStats: TunnelingBeamWeaponMedium,
		},
		"Beam, Tunneling [L]": {
			minLevel:       Beam3rdGenLevel,
			maxLevel:       10,
			componentStats: TunnelingBeamWeaponLarge,
		},

		"Beam, Omega [S]": {
			minLevel:       Beam3rdGenLevel,
			maxLevel:       10,
			componentStats: OmegaBeamWeaponSmall,
		},
		"Beam, Omega [M]": {
			minLevel:       Beam3rdGenLevel,
			maxLevel:       10,
			componentStats: OmegaBeamWeaponMedium,
		},
		"Beam, Omega [L]": {
			minLevel:       Beam3rdGenLevel,
			maxLevel:       10,
			componentStats: OmegaBeamWeaponLarge,
		},

		// Ikkuro Fusion Beams
		"Beam, Fusion [S]": {
			minLevel:       Beam1stGenLevel,
			maxLevel:       10,
			componentStats: FusionBeamWeaponSmall,
		},
		"Beam, Fusion [M]": {
			minLevel:       Beam1stGenLevel,
			maxLevel:       10,
			componentStats: FusionBeamWeaponMedium,
		},
		"Beam, Fusion [L]": {
			minLevel:       Beam1stGenLevel,
			maxLevel:       10,
			componentStats: FusionBeamWeaponLarge,
		},

		// Zenox Star Beams
		"Beam, Star [S]": {
			minLevel:       Beam1stGenLevel,
			maxLevel:       10,
			componentStats: StarBeamWeaponSmall,
		},
		"Beam, Star [M]": {
			minLevel:       Beam1stGenLevel,
			maxLevel:       10,
			componentStats: StarBeamWeaponMedium,
		},
		"Beam, Star [L]": {
			minLevel:       Beam1stGenLevel,
			maxLevel:       10,
			componentStats: StarBeamWeaponLarge,
		},

		"Massive Beams Emplacement": {
			minLevel:       3,
			maxLevel:       9,
			componentStats: PlanetaryBeamComponentStats,
		},
	}

	StandardBeamTargetingBonus      = MakeFixedLevelFunc(0.1)
	StandardBeamFalloffRatio        = MakeFixedLevelFunc(BeamFalloff)
	StandardBeamWeaponRange         = MakeExpLevelFunc(1000, 1.1) // 10% level over level
	StandardBeamWeaponFireRate      = MakeFixedLevelFunc(BaseBeamFireRate)
	StandardBeamWeaponSpeed         = MakeFixedLevelFunc(BeamWeaponSpeed)
	StandardBeamWeaponRawDamage     = MakeDamageFunc(BaseBeamFireRate, BeamDamageFactor)
	StandardBeamWeaponEnergyPerShot = MakeEnergyPerShotFunc(BeamEnergyRatio, StandardBeamWeaponRawDamage)

	// Particle Beam
	ParticleBeamWeaponSmall = ComposeComponentStats(
		SmallWeaponBaseStats,
		WeaponNoAOE,
		WeaponNoBombard,
		WeaponNoIon,
		ComponentStats{
			"ComponentCountermeasuresBonus": StandardBeamCountermeasuresBonus,
			"ComponentTargetingBonus":       StandardBeamTargetingBonus,
			"CrewRequirement":               SmallCrewRequirements,
			"StaticEnergyUsed":              SmallWeaponStaticEnergy,
			"WeaponDamageFalloffRatio":      StandardBeamFalloffRatio,
			"WeaponArmorBypass":             MakeFixedLevelFunc(0),
			"WeaponShieldBypass":            MakeFixedLevelFunc(0),
			"WeaponRange":                   StandardBeamWeaponRange,
			"WeaponSpeed":                   StandardBeamWeaponSpeed,
			"WeaponFireRate":                StandardBeamWeaponFireRate,
			"WeaponEnergyPerShot":           StandardBeamWeaponEnergyPerShot,
			"WeaponRawDamage":               StandardBeamWeaponRawDamage,
			"WeaponVolleyAmount":            MakeFixedLevelFunc(1),
			"WeaponVolleyFireRate":          MakeFixedLevelFunc(0),
		},
	)

	ParticleBeamWeaponMedium = DeriveMediumWeaponUsing2xDamage(ParticleBeamWeaponSmall)

	// Thuon
	ThuonBeamWeaponEnergyPerShot = MakeEnergyPerShotFunc(ThuonBeamEnergyRatio, ThuonBeamWeaponRawDamage)
	ThuonBeamWeaponFireRate      = MakeFixedLevelFunc(ThuonBeamFireRate)
	ThuonBeamWeaponRange         = StandardBeamWeaponRange
	ThuonBeamWeaponRawDamage     = MakeDamageFunc(ThuonBeamFireRate, ThuonBeamDamageFactor)

	ThuonBeamWeaponSmall = ComposeComponentStats(
		ParticleBeamWeaponSmall,
		ComponentStats{
			"WeaponEnergyPerShot": ThuonBeamWeaponEnergyPerShot,
			"WeaponFireRate":      ThuonBeamWeaponFireRate,
			"WeaponRange":         ThuonBeamWeaponRange,
			"WeaponRawDamage":     ThuonBeamWeaponRawDamage,
		},
	)

	ThuonBeamWeaponMedium = DeriveMediumWeaponUsing2xDamage(ThuonBeamWeaponSmall)

	// Shatterforce
	ShatterforceBeamWeaponEnergyPerShot = MakeEnergyPerShotFunc(ShatterforceBeamEnergyRatio, ShatterforceBeamWeaponRawDamage)
	ShatterforceBeamWeaponFireRate      = MakeFixedLevelFunc(ShatterforceBeamFireRate)
	ShatterforceBeamWeaponRange         = ThuonBeamWeaponRange
	ShatterforceBeamWeaponRawDamage     = MakeDamageFunc(ShatterforceBeamFireRate, ShatterforceBeamDamageFactor)

	ShatterforceBeamWeaponSmall = ComposeComponentStats(
		ThuonBeamWeaponSmall,
		ComponentStats{
			"WeaponEnergyPerShot": ShatterforceBeamWeaponEnergyPerShot,
			"WeaponFireRate":      ShatterforceBeamWeaponFireRate,
			"WeaponRange":         ShatterforceBeamWeaponRange,
			"WeaponRawDamage":     ShatterforceBeamWeaponRawDamage,
		},
	)

	ShatterforceBeamWeaponMedium = DeriveMediumWeaponUsing2xDamage(ShatterforceBeamWeaponSmall)
	ShatterforceBeamWeaponLarge  = DeriveLargeWeaponUsing4xDamage(ShatterforceBeamWeaponSmall)

	// (Shield) Tunneling
	TunnelingBeamWeaponEnergyPerShot = MakeEnergyPerShotFunc(TunnelingBeamEnergyRatio, TunnelingBeamWeaponRawDamage)
	TunnelingBeamWeaponFireRate      = MakeFixedLevelFunc(TunnelingBeamFireRate)
	TunnelingBeamWeaponRange         = ShatterforceBeamWeaponRange
	TunnelingBeamWeaponRawDamage     = MakeDamageFunc(TunnelingBeamFireRate, TunnelingBeamDamageFactor)

	TunnelingBeamWeaponSmall = ComposeComponentStats(
		ShatterforceBeamWeaponSmall,
		ComponentStats{
			"WeaponEnergyPerShot": TunnelingBeamWeaponEnergyPerShot,
			"WeaponFireRate":      TunnelingBeamWeaponFireRate,
			"WeaponRange":         TunnelingBeamWeaponRange,
			"WeaponRawDamage":     TunnelingBeamWeaponRawDamage,
			"WeaponShieldBypass":  MakeOffsetFuncLevelFunc(-Beam3rdGenLevel, MakeLinearLevelFunc(TunnelingBeamShieldBypass, TunnelingBeamShieldBypassIncr)),
			"WeaponArmorBypass":   MakeFixedLevelFunc(TunnelingBeamArmorBypass),
		},
	)

	TunnelingBeamWeaponMedium = DeriveMediumWeaponUsing2xDamage(TunnelingBeamWeaponSmall)
	TunnelingBeamWeaponLarge  = DeriveLargeWeaponUsing4xDamage(TunnelingBeamWeaponSmall)

	// (Armor) Piercing
	PiercingBeamWeaponEnergyPerShot = MakeEnergyPerShotFunc(PiercingBeamEnergyRatio, PiercingBeamWeaponRawDamage)
	PiercingBeamWeaponFireRate      = MakeFixedLevelFunc(PiercingBeamFireRate)
	PiercingBeamWeaponRange         = ShatterforceBeamWeaponRange
	PiercingBeamWeaponRawDamage     = MakeDamageFunc(PiercingBeamFireRate, PiercingBeamDamageFactor)

	PiercingBeamWeaponSmall = ComposeComponentStats(
		ShatterforceBeamWeaponSmall,
		ComponentStats{
			"WeaponEnergyPerShot": PiercingBeamWeaponEnergyPerShot,
			"WeaponFireRate":      PiercingBeamWeaponFireRate,
			"WeaponRange":         PiercingBeamWeaponRange,
			"WeaponRawDamage":     PiercingBeamWeaponRawDamage,
			"WeaponShieldBypass":  MakeFixedLevelFunc(PiercingBeamShieldBypass),
			"WeaponArmorBypass":   MakeOffsetFuncLevelFunc(-Beam3rdGenLevel, MakeLinearLevelFunc(PiercingBeamArmorBypass, PiercingBeamArmorBypassIncr)),
		},
	)

	PiercingBeamWeaponMedium = DeriveMediumWeaponUsing2xDamage(PiercingBeamWeaponSmall)
	PiercingBeamWeaponLarge  = DeriveLargeWeaponUsing4xDamage(PiercingBeamWeaponSmall)

	// Omega
	OmegaBeamWeaponEnergyPerShot = MakeEnergyPerShotFunc(OmegaBeamEnergyRatio, OmegaBeamWeaponRawDamage)
	OmegaBeamWeaponFireRate      = MakeFixedLevelFunc(OmegaBeamFireRate)
	OmegaBeamWeaponRange         = ShatterforceBeamWeaponRange
	OmegaBeamWeaponRawDamage     = MakeDamageFunc(OmegaBeamFireRate, OmegaBeamDamageFactor)

	OmegaBeamWeaponSmall = ComposeComponentStats(
		ShatterforceBeamWeaponSmall,
		ComponentStats{
			"WeaponEnergyPerShot": OmegaBeamWeaponEnergyPerShot,
			"WeaponFireRate":      OmegaBeamWeaponFireRate,
			"WeaponRange":         OmegaBeamWeaponRange,
			"WeaponRawDamage":     OmegaBeamWeaponRawDamage,
			"WeaponShieldBypass":  MakeFixedLevelFunc(OmegaBeamShieldBypass),
			"WeaponArmorBypass":   MakeFixedLevelFunc(OmegaBeamArmorBypass),
		},
	)

	OmegaBeamWeaponMedium = DeriveMediumWeaponUsing2xDamage(OmegaBeamWeaponSmall)
	OmegaBeamWeaponLarge  = DeriveLargeWeaponUsing4xDamage(OmegaBeamWeaponSmall)

	// Star
	StarBeamWeaponEnergyPerShot = MakeEnergyPerShotFunc(StarBeamEnergyRatio, StarBeamWeaponRawDamage)
	StarBeamWeaponFireRate      = MakeFixedLevelFunc(StarBeamFireRate)
	StarBeamWeaponRange         = ShatterforceBeamWeaponRange
	StarBeamWeaponRawDamage     = MakeDamageFunc(StarBeamFireRate, StarBeamDamageFactor)

	StarBeamWeaponSmall = ComposeComponentStats(
		ShatterforceBeamWeaponSmall,
		ComponentStats{
			"WeaponEnergyPerShot": StarBeamWeaponEnergyPerShot,
			"WeaponFireRate":      StarBeamWeaponFireRate,
			"WeaponRange":         StarBeamWeaponRange,
			"WeaponRawDamage":     StarBeamWeaponRawDamage,
			"WeaponShieldBypass":  MakeExpLevelFunc(StarBeamShieldBypass, BeamBypassGrowthRate),
			"WeaponArmorBypass":   MakeExpLevelFunc(StarBeamArmorBypass, BeamBypassGrowthRate),
		},
	)

	StarBeamWeaponMedium = DeriveMediumWeaponUsing2xDamage(StarBeamWeaponSmall)
	StarBeamWeaponLarge  = DeriveLargeWeaponUsing4xDamage(StarBeamWeaponSmall)

	// Fusion
	FusionBeamWeaponEnergyPerShot = MakeEnergyPerShotFunc(FusionBeamEnergyRatio, FusionBeamWeaponRawDamage)
	FusionBeamWeaponFireRate      = MakeFixedLevelFunc(FusionBeamFireRate)
	FusionBeamWeaponRange         = ShatterforceBeamWeaponRange
	FusionBeamWeaponRawDamage     = MakeDamageFunc(FusionBeamFireRate, FusionBeamDamageFactor)

	FusionBeamWeaponSmall = ComposeComponentStats(
		ShatterforceBeamWeaponSmall,
		ComponentStats{
			"WeaponEnergyPerShot": FusionBeamWeaponEnergyPerShot,
			"WeaponFireRate":      FusionBeamWeaponFireRate,
			"WeaponRange":         FusionBeamWeaponRange,
			"WeaponRawDamage":     FusionBeamWeaponRawDamage,
			"WeaponShieldBypass":  MakeExpLevelFunc(FusionBeamShieldBypass, BeamBypassGrowthRate),
			"WeaponArmorBypass":   MakeExpLevelFunc(FusionBeamArmorBypass, BeamBypassGrowthRate),
		},
	)

	FusionBeamWeaponMedium = DeriveMediumWeaponUsing2xDamage(FusionBeamWeaponSmall)
	FusionBeamWeaponLarge  = DeriveLargeWeaponUsing4xDamage(FusionBeamWeaponSmall)

	// we'll make these derivative of shatterforce beams
	PlanetaryBeamComponentStats = DerivePlanetaryFromLargeWeapon(ShatterforceBeamWeaponLarge)
)
