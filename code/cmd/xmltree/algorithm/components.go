package algorithm

import (
	"fmt"
	"math"
	"regexp"
	"slices"

	"github.com/lucky-wolf/xml-tree/etc"
	"github.com/lucky-wolf/xml-tree/xmltree"
)

// note: see globals.go for some standard level functions

type ComponentName = string
type WeaponFamilyName = string

// maps the name of a component attribute to a level function to derive its value
type ComponentStats = map[AttributeName]LevelFunc

// like component stats, but returns a simple level func which is more flexible (not always a number)
type FlexComponentStats = map[AttributeName]SimpleLevelFunc

// we take a "level" and return a numeric value
// typically we use this for component stats
type LevelFunc = func(level int) float64

// we return a simple value which can be applied to an XMLValue
type SimpleLevelFunc = func(level int) xmltree.SimpleValue

type ComponentIs struct {
	fighter bool
	weapon  bool
	pd      bool
	size    int
	family  string
}

type ComponentLevelData struct {
	values         SimpleValuesTable // these are applied to the base component
	minLevel       int
	maxLevel       int
	componentStats ComponentStats
	derivatives    []string // what other components are derived from this? (i.e. name of [F/B] or [PD] components)
}

type SimpleValuesTable = map[AttributeName]xmltree.SimpleValue
type ComponentLevelDataMap map[AttributeName]ComponentLevelData

func (cld ComponentLevelDataMap) Names() (names []string) {
	for k := range cld {
		names = append(names, k)
	}

	slices.Sort(names)
	return
}

func MakeFixedLevelFunc(basis float64) LevelFunc {
	return func(level int) float64 { return basis }
}

func MakeLinearLevelFunc(basis float64, add float64) LevelFunc {
	return func(level int) float64 { return basis + (add * float64(level)) }
}

func MakeExpLevelFunc(basis float64, scale float64) LevelFunc {
	return func(level int) float64 { return basis * math.Pow(scale, float64(level)) }
}

func MakeScaledFuncLevelFunc(scale float64, levelfunc LevelFunc) LevelFunc {
	return func(level int) float64 { return scale * levelfunc(level) }
}

func MakeMultipliedLevelFunc(f1, f2 LevelFunc) LevelFunc {
	return func(level int) float64 { return f1(level) * f2(level) }
}

func MakeOffsetFuncLevelFunc(offset int, levelfunc LevelFunc) LevelFunc {
	return func(level int) float64 {
		if level+offset < 0 {
			return 0
		} else {
			return levelfunc(level + offset)
		}
	}
}

func MakeIntegerLevelFunc(levelfunc LevelFunc) LevelFunc {
	return func(level int) float64 { return math.Round(levelfunc(level)) }
}

func MakeFloatsLevelFunc(values ...float64) LevelFunc {
	return func(level int) float64 {
		// note: this will panic if you try to access a level that doesn't exist
		return values[level]
	}
}

func MakeIntsLevelFunc(values ...int) LevelFunc {
	return func(level int) float64 {
		// note: this will panic if you try to access a level that doesn't exist
		return float64(values[level])
	}
}

func ComposeComponentStats(fields ComponentStats, more ...ComponentStats) (result ComponentStats) {
	// clone the base stats
	result = ComponentStats{}
	for k, v := range fields {
		result[k] = v
	}
	// merge in each additional stats
	for _, more := range more {
		for k, v := range more {
			result[k] = v
		}
	}
	return
}

func GetComponentIsms(e *xmltree.XMLElement) (is ComponentIs) {
	is.fighter = e.HasChildWithValue("IsFighterOnly", "true")
	is.weapon = e.HasPrefix("Category", "Weapon")
	is.pd = !is.fighter && e.HasChildWithValue("Category", "WeaponIntercept")
	is.size = e.Child("Size").IntValue()

	f := e.Child("Family")
	if f != nil {
		is.family = f.StringValue()
	}

	return
}

// apply stats for each component
func (j *Job) ApplyComponentAll(components ComponentLevelDataMap) (err error) {

	// fmt.Printf("Scaling: %s", strings.Join(components.Names(), ", "))

	for k, v := range components {
		err = j.ApplyComponent(k, v)
		if err != nil {
			return
		}
	}
	return
}

const ComponentResourceCostBasis = 2

var PlanetaryDefenseComponents = map[string]bool{
	"Massive Beams Emplacement":              true,
	"Ground to Space Torpedo Launchers":      true,
	"Ground to Space Missile Launchers":      true,
	"Massive Forge Rail Battery Emplacement": true,
	"Massive Ion Cannon Battery":             true,
	"Massive Blaster Cannon Battery":         true,
	"Tractor-Repulser Emplacement":           true,
	"Orbital Space Mines Projector":          true,
	"Massive Gravitic Disruptor Emplacement": true,
}

// how much resources, generically, do things require based on tech level
// NOTE: we could have a per-component basis factor to this global one (armor more, ??? less)
func ComponentResourceCost(level float64, size int) float64 {
	// level factor is one-based
	levelFactor := level + 1.0
	// size factor is 1/10th of size
	sizeFactor := float64(size) / 10.0
	return ComponentResourceCostBasis * levelFactor * sizeFactor
}

// applies stats for given component
func (j *Job) ApplyComponent(name string, data ComponentLevelData) (err error) {

	fmt.Printf("Scaling: %s", name)

	// find this component definition
	e, f := j.FindElement("Name", name)
	if e == nil {
		return fmt.Errorf("%s not found", name)
	}
	statistics := &f.stats

	// get the component isms
	is := GetComponentIsms(e)

	// apply any top-level values
	for key, sv := range data.values {
		if c := e.Child(key); c != nil {
			c.XMLValue.SetString(sv.String())
		}
	}

	// set our resources required
	rr := e.Child("ResourcesRequired")
	if rr == nil {
		// note planetary emplacements don't need resources - pointless
		if !PlanetaryDefenseComponents[name] {
			err = fmt.Errorf("Component %s is missing <ResourcesRequired> block", name)
			return
		}
	} else {
		// use average tech level for this component
		level := float64(data.maxLevel+data.minLevel) / 2.0
		// size matters (round up to decade)
		size := e.Child("Size").IntValue()
		// for each ResourceQuantity
		for _, rq := range rr.Elements() {
			// set the amount based on the base cost and rarity scaling of that resource type
			amount := ScaleResourceCost(rq.Child("ResourceId").IntValue(), ComponentResourceCost(level, size))
			rq.Child("Amount").SetValue(amount)
		}
	}

	// ensure we have correct number of component stats to update
	err = e.Child("Values").SetElementCountByCopyingFirstElementAsNeeded(1 + data.maxLevel - data.minLevel)
	if err != nil {
		return
	}

	// fill in the data from our data tables
	values := e.Child("Values").Elements()
	for i, e := range values {
		assertIs(e, "ComponentStats")
		if is.weapon && !is.pd {
			// do not allow non-intercept weapons to have intercept values
			ZeroInterceptValues(e)
		}
		for key, f := range data.componentStats {
			value := f(data.minLevel + i)
			if value == 0 {
				// note: removing zero value keys leads to very skeletal XML that I worry will be hard to work with
				e.SetChildValue(key, value)
			} else {
				e.MakeChildWithValue(key, value)
			}
		}
		statistics.elements++
		statistics.changed++
	}
	statistics.objects++

	// scale to fighter if required
	for _, name := range data.derivatives {
		err = j.DeriveFromComponentByName(e, name)
	}

	return
}

func (j *Job) DeriveFromComponentByName(source *xmltree.XMLElement, name string) (err error) {

	// figure out our target name
	e, f := j.FindElement("Name", name)
	if e == nil {
		err = fmt.Errorf("%s not found", name)
		return
	}

	// scale it
	err = j.DeriveComponentFromComponent(f, source, e)

	return
}

func IsStandardWeaponCategory(category string) bool {
	switch category {
	case "WeaponIntercept", "WeaponCloseIn", "WeaponStandoff":
		return true
	default:
		return false
	}
}

func (j *Job) DeriveComponentFromComponent(file *XFile, source *xmltree.XMLElement, e *xmltree.XMLElement) (err error) {

	statistics := &file.stats

	// distinguish what kind of target component we're dealing with
	is := GetComponentIsms(e)

	name := e.Child("Name").StringValue()
	sourceName := source.Child("Name").StringValue()

	fmt.Printf("Deriving: %s from %s", name, sourceName)

	// attempt to extract the int64 sourceSize of the size element
	sourceSizeElem := source.Child("Size")
	if sourceSizeElem == nil {
		err = fmt.Errorf("%s doesn't have a size?!", sourceName)
		return
	}
	var sourceSize int64
	sourceSize, err = sourceSizeElem.GetInt64Value()
	if err != nil {
		return
	}

	// scale component size
	if is.fighter {

		// size must be an integer
		switch {
		case is.weapon:

			category := source.Child("Category").StringValue()
			// fmt.Println(sourceName, "was", category)

			switch sourceSize {

			case DirectFireWeaponSize:

				// we're a fighter weapon
				is.size = FighterWeaponSize
				e.Child("Size").SetValue(FighterWeaponSize)

				// derive fighter weapon category
				if IsStandardWeaponCategory(category) {
					category = "WeaponCloseIn"
				}

			case SeekingWeaponSize:

				// we're a bomber weapon
				is.size = BomberWeaponSize
				e.Child("Size").SetValue(BomberWeaponSize)

				// derive bomber weapon category
				if IsStandardWeaponCategory(category) {
					category = "WeaponStandoff"
				}

			default:
				panic("wtf size source weapon is this?!")
			}

			// fmt.Println(sourceName, "will be", category)
			e.SetChildValue("Category", category)
		default:
			// non-weapons = 33% size
			// (must be an integer value, rounded up, but cannot exceed size 10 for any slot, and reactors would exceed it)
			is.size = min(10, etc.MulDivRoundUp(int(sourceSize), 1, 3))

			e.Child("Size").SetValue(is.size)
		}

	} else if is.pd {
		// note: we lie in is.size because we have downstream logic that only respects the older sizes
		switch sourceSize {
		case DirectFireWeaponSize:
			is.size = DirectFireWeaponSize
			e.Child("Size").SetValue(DirectFirePointDefenseSize)
		case SeekingWeaponSize:
			is.size = SeekingWeaponSize
			e.Child("Size").SetValue(SeekingPointDefenseSize)
		default:
			err = fmt.Errorf("PD Source \"%s\" has an invalid size: %d", sourceName, sourceSize)
			return
		}
	}

	// copy (and scale fighter) resource requirements
	if is.fighter {
		// minor weirdness: produces fractional resources, which the engine allows, but... awkward
		err = e.CopyAndVisitByTag("ResourcesRequired", source, func(e *xmltree.XMLElement) error { return e.ScaleChildBy("Amount", 0.25) })
		// err = e.RemoveByTag("ResourcesRequired")
		if err != nil {
			// warn about any missing resource requirements for these
			fmt.Printf("Warn: fighter component %s is missing ResourcesRequired block: %s", name, err)
			err = nil
		}
	} else {
		err = e.CopyByTag("ResourcesRequired", source)
	}
	if err != nil {
		fmt.Println(err)
	}

	// copy component stats
	err = e.CopyByTag("Values", source)
	if err != nil {
		fmt.Println(err)
	}

	// now that we have our own copy of the component stats (same number of levels too)
	// we can update each of those to scale for [F/B] version
	for _, e := range e.Child("Values").Elements() {

		// every element should be a component bay
		err = assertIs(e, "ComponentStats")
		if err != nil {
			return
		}

		if is.weapon {

			// "flatten" source volleys to 1 per shot but at 1/x fire rate (same dps, but distributed instead of burst firing)
			if va := e.Child("WeaponVolleyAmount").FloatValue(); va != 1 {
				e.ScaleChildBy("WeaponFireRate", 1.0/va)
				e.Child("WeaponVolleyAmount").SetValue(1)
			}
			e.SetChildValue("WeaponVolleyFireRate", "0")

			// scale derived weapon relative to our source weapon
			err = ScaleWeaponValuesFromParent(e, is)
			if err != nil {
				return
			}

			// fighters and PD never do bombard damage
			for _, e := range e.Matching(regexp.MustCompile("WeaponBombard.*")) {
				e.SetString("0")
			}
		}

		// scale down the ion defenses and offenses
		err = ScaleFtrOrPDIonValues(e, is)
		if err != nil {
			return
		}

		if is.fighter {
			// fighters never have crew requirements
			e.SetChildValue("CrewRequirement", 0)

			// scale down static energy use
			e.ScaleChildBy("StaticEnergyUsed", FighterStaticEnergyFactor)

			// scale armor values
			e.ScaleChildBy("ArmorBlastRating", FighterDefenseScaleFactor)
			e.ScaleChildBy("ArmorReactiveRating", FighterDefenseScaleFactor)

			// scale engine values
			e.ScaleChildBy("EngineMainCruiseThrust", FighterEngineThrustFactor)
			e.ScaleChildBy("EngineMainCruiseThrustEnergyUsage", FighterEngineEnergyFactor)

			e.ScaleChildBy("EngineMainMaximumThrust", FighterEngineThrustFactor)
			e.ScaleChildBy("EngineMainMaximumThrustEnergyUsage", FighterEngineEnergyFactor)

			e.ScaleChildBy("EngineVectoringThrust", FighterEngineThrustFactor)
			e.ScaleChildBy("EngineVectoringEnergyUsage", FighterEngineEnergyFactor)

			// scale reactor values
			if n := e.Child("ReactorEnergyOutputPerSecond"); n != nil {
				e.ScaleChildBy("ReactorEnergyOutputPerSecond", FighterReactorFactor)
				e.ScaleChildBy("ReactorEnergyStorageCapacity", FighterReactorFactor)
				e.ScaleChildBy("ReactorFuelUnitsForFullCharge", FighterReactorFactor)

				// strikecraft reactors store their own fuel
				// note: we have to create the storage node because we don't keep empty nodes within the parent components anymore
				err = e.InsertAt(e.ChildIndex("ReactorEnergyOutputPerSecond"), n.CloneAs("FuelStorageCapacity"))
				if err != nil {
					return
				}
				e.ScaleChildToSiblingBy("FuelStorageCapacity", "ReactorFuelUnitsForFullCharge", 50)
			}

			// scale shield values
			e.ScaleChildBy("ShieldRechargeEnergyUsage", FighterShieldEnergyFactor)
			e.ScaleChildBy("ShieldRechargeRate", FighterDefenseScaleFactor)
			e.ScaleChildBy("ShieldResistance", FighterDefenseScaleFactor)
			e.ScaleChildBy("ShieldStrength", FighterDefenseScaleFactor)
		}

		statistics.changed++
		statistics.elements++
	}

	statistics.objects++

	return
}

func ScaleFtrOrPDIonValues(e *xmltree.XMLElement, is ComponentIs) (err error) {

	// but they aren't just a ton of ion small weapons against ships (that would be OP I believe)
	if is.weapon {
		e.ScaleChildBy("WeaponIonEngineDamage", IonFtrPDScaleFactor)
		e.ScaleChildBy("WeaponIonHyperDriveDamage", IonFtrPDScaleFactor)
		e.ScaleChildBy("WeaponIonSensorDamage", IonFtrPDScaleFactor)
		e.ScaleChildBy("WeaponIonShieldDamage", IonFtrPDScaleFactor)
		e.ScaleChildBy("WeaponIonWeaponDamage", IonFtrPDScaleFactor)
		e.ScaleChildBy("WeaponIonGeneralDamage", IonFtrPDScaleFactor)
	}

	return
}

// non-intercept attack values only
func ScalePrimaryWeaponFunction(e *xmltree.XMLElement, is ComponentIs) {

	if !is.weapon {
		panic("do not call GetFighterOrPointDefenseScaling for a non-weapon")
	}

	var rof, dmg float64
	switch is.size {
	case BomberWeaponSize:
		// 2 x .85 = 1.7x total output
		rof, dmg = 2, .85
	case FighterWeaponSize, DirectFireWeaponSize, SeekingWeaponSize:
		// 4 x .375 = 1.5x total output
		rof, dmg = 4, .375
	default:
		panic("GetFighterOrPointDefenseScaling: component size is neither small seeking nor small direct fire")
	}

	// scale by our source weapon values
	e.ScaleChildBy("WeaponFireRate", 1/rof)
	e.ScaleChildBy("WeaponRawDamage", dmg)
	e.ScaleChildBy("WeaponEnergyPerShot", dmg)

	if is.fighter {
		e.ScaleChildBy("WeaponEnergyPerShot", FighterWeaponEnergyFactor)
	}
}

// intercept attack values only
// WARN: this is relative to already being scaled by ScaleFighterOrPointDefense()
func ScaleInterceptWeaponFunction(e *xmltree.XMLElement, is ComponentIs) (err error) {

	if !is.weapon {
		err = fmt.Errorf("cannot ScaleInterceptWeaponFunction for a non-weapon")
		return
	}

	// ensure all intercept values are present in child
	interceptKeys := []string{
		"WeaponInterceptComponentTargetingBonus",
		"WeaponInterceptDamageFighter",
		"WeaponInterceptDamageSeeking",
		"WeaponInterceptEnergyPerShot",
		"WeaponInterceptFireRate",
		"WeaponInterceptIonDamageRatio",
		"WeaponInterceptRange",
	}
	for _, tag := range interceptKeys {
		if e.ChildIndex(tag) == -1 {
			err = e.InsertAt(e.ChildIndex("WeaponRange"), xmltree.MakeElementWithValue(tag, "0"))
			if err != nil {
				return
			}
		}
	}

	var rof, dmg float64
	switch {
	case is.fighter && is.size == FighterWeaponSize:
		// subtle: no bomber weapons should come through here because bomber weapons don't have intercept values
		// for fighters scale intercept by...
		// previously we were at a net 4x dmg vs. fighters and 8x dmg vs. seeking
		// (8x rof, 1/2x dmg vs. fighters, and 8x rof, 1x dmg vs. seeking)
		// now we're at 5x4 = 20x rof vs. standard (was 32x)
		// and 5 x .4 = 200% total damage output compard to base, which is 1.5 normal = 300% total vs. standard weapon
		rof = 5
		dmg = .4

	case is.pd:

		switch is.size {
		case SeekingWeaponSize:
			// seeking based PD
			// we don't want to ramp up the fire rate all that much at all
			// we already flattened and doubled the fire rate
			rof = 2

			// subtle: this should get us to 1/2 dmg of base missile type vs. ftr, and 1 dmg vs. missiles or torpedoes
			dmg = 0.5 / .375

		case DirectFireWeaponSize:
			// the very high rof means we should get cool point defense rapid fire visuals
			rof = 10
			dmg = .8

		default:
			err = fmt.Errorf("ScaleInterceptWeaponFunction: PD component size is neither small seeking nor small direct fire")
			return
		}
	}

	// scale by our standard mode values
	err = e.ScaleChildToSiblingBy("WeaponInterceptFireRate", "WeaponFireRate", 1/rof)
	if err != nil {
		return
	}
	err = e.ScaleChildToSiblingBy("WeaponInterceptDamageFighter", "WeaponRawDamage", dmg)
	if err != nil {
		return
	}
	err = e.ScaleChildToSiblingBy("WeaponInterceptDamageSeeking", "WeaponRawDamage", dmg)
	if err != nil {
		return
	}
	err = e.ScaleChildToSiblingBy("WeaponInterceptEnergyPerShot", "WeaponEnergyPerShot", dmg)
	if err != nil {
		return
	}

	return
}

func ScaleWeaponValuesFromParent(e *xmltree.XMLElement, is ComponentIs) (err error) {

	if !is.weapon {
		err = fmt.Errorf("ScaleWeaponValuesFromParent() component must be a weapon")
		return
	}

	// get appropriate scaling factors
	ScalePrimaryWeaponFunction(e, is)

	if is.fighter {
		// scale range and falloff
		e.ScaleChildBy("WeaponRange", FighterWeaponRangeFactor)
		e.ScaleChildBy("WeaponDamageFalloffRatio", FighterWeaponFallOffFactor)

		// give strikecraft a to-hit bonus
		e.AdjustChildBy("ComponentTargetingBonus", FighterWeaponTargetingBonus)
	} else {
		// scale range and falloff
		e.ScaleChildBy("WeaponRange", PDRangeFactor)
		e.ScaleChildBy("WeaponDamageFalloffRatio", PDFallOffFactor)

		// PD weapons get a targeting bonus
		e.AdjustChildBy("ComponentTargetingBonus", PDTargetingBonus)
	}

	// bombers will no longer have any intercept function at all
	// use interceptors (fighters) for that!
	if is.fighter && is.size == BomberWeaponSize {
		ZeroInterceptValues(e)
	} else {

		// scale rof and damage for intercept function
		err = ScaleInterceptWeaponFunction(e, is)
		if err != nil {
			return
		}

		// currently we simply always set intercept range == base range for this weapon
		e.SetChildToSibling("WeaponInterceptRange", "WeaponRange")

		// PD must actually hit for it to be useful!
		e.AdjustChildToSiblingBy("WeaponInterceptComponentTargetingBonus", "ComponentTargetingBonus", 0.1)

		// because the dw2 team is incredibly foolish, we have no direct way to know if a weapon is Ion or not
		// so, we'll look for Ion damage attribute and base it on being non-zero there
		// note: WeaponIonGeneralDamage is often zero in vanilla, but we've made it align with all other WeaponIon*Damage values in XL
		if c := e.Child("WeaponIonGeneralDamage"); c != nil && c.StringValue() != "0" {
			e.SetChildValue("WeaponInterceptIonDamageRatio", "1")
		}
	}

	// scale intercept weapons to fire faster projectiles / seeking weapons
	// so that they can catch the fast things they're attacking
	if is.pd {
		switch is.family {
		case "Missiles":
			e.ScaleChildBy("WeaponSpeed", 1.5)
		case "Rail Guns":
			e.ScaleChildBy("WeaponSpeed", 1.25)
		}
	}

	return
}

func ZeroInterceptValues(e *xmltree.XMLElement) (err error) {

	// zero out all intercept values
	e.SetChildValue("WeaponInterceptFireRate", "0")
	e.SetChildValue("WeaponInterceptDamageFighter", "0")
	e.SetChildValue("WeaponInterceptDamageSeeking", "0")
	e.SetChildValue("WeaponInterceptEnergyPerShot", "0")
	e.SetChildValue("WeaponInterceptRange", "0")
	e.SetChildValue("WeaponInterceptComponentTargetingBonus", "0")
	e.SetChildValue("WeaponIonGeneralDamage", "0")

	return
}

func GetFighterOrPointDefenseSourceName(targetName string, is ComponentIs) (sourceName string) {

	if is.fighter {
		sourceName = targetName[:len(targetName)-len(" [F/B]")] + " [S]"
	} else {
		pdSourceMap := map[string]string{
			"Defense, Sentinel Beams [PD]": "Beam, Thuon [S]",
			"Defense, Beam Matrix [PD]":    "Beam, Shatterforce [S]",
			"Defense, Beam Array [PD]":     "Beam, Tunneling [S]",
			"Defense, Guardian Beams [PD]": "Beam, Omega [S]",

			"Defense, Repeating Blasters [PD]": "Blaster, Maxos [S]",
			"Defense, Bulwark Blasters [PD]":   "Blaster, Assault [S]",
			"Defense, Maelstrom Blasters [PD]": "Blaster, Titan [S]",

			"Defense, PDC [PD]":             "Gun, Rail [S]",
			"Defense, Terminator Grid [PD]": "Gun, Terminator Assault Cannon [S]",
			"Defense, Shrike PDC [PD]":      "Gun, Hyperion Cannon [S]",

			"Defense, Interceptor Missiles [PD]": "Missile, Obliterator [S]",
			"Defense, Aegis Batteries [PD]":      "Missile, Lightning [S]",
			"Defense, Stinger Missiles [PD]":     "Missile, Scorpion [S]",

			"Defense, Shock Cannons [PD]":       "Ion Cannon [S]",
			"Defense, Rapid Shock Cannons [PD]": "Ion Cannon, Rapid [S]",
		}

		// find the corresponding small weapon by name
		// PD in particular uses asymmetric sources
		sourceName = pdSourceMap[targetName]
		if sourceName == "" {
			// simply use the component name [S] as our source component
			sourceName = targetName[:len(targetName)-len(" [PD]")] + " [S]"

			fmt.Printf("WARN: using %s for %s (no PD specific name mapping found)", sourceName, targetName)
		}
	}

	return
}
