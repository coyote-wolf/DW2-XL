package algorithm

func TorpedoWeapons(folder string) (err error) {

	// load all component definition files
	j, err := LoadJobFor(folder, "ComponentDefinitions*.xml")
	if err != nil {
		return
	}

	// apply this transformation
	err = j.applyTorpedoWeapons()
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

func (j *Job) applyTorpedoWeapons() (err error) {
	return j.ApplyComponentAll(TorpedoWeaponData)
}

const (
	// torpedoes are slow, but grow by 10% of base speed / level
	PlasmaTorpedoSpeed    = 200
	EpsilonTorpedoSpeed   = 225
	ShockwaveTorpedoSpeed = 225
	VelocityTorpedoSpeed  = 275
	QuantumTorpedoSpeed   = 300
	PulseTorpedoSpeed     = 275
	FirestormTorpedoSpeed = 250
	InfernoTorpedoSpeed   = 250

	// base range
	PlasmaTorpedoBaseRange       = 1400
	PlasmaTorpedoRangeGrowthRate = 1.1

	// torpedoes are short ranged anyway, don't need a draconian dropoff too
	PlasmaTorpedoFallOff    = .16875 // note: this gives .15 for Epsilon, Shockwave
	EpsilonTorpedoFallOff   = PlasmaTorpedoFallOff * PlasmaTorpedoSpeed / EpsilonTorpedoSpeed
	ShockwaveTorpedoFallOff = PlasmaTorpedoFallOff * PlasmaTorpedoSpeed / ShockwaveTorpedoSpeed
	VelocityTorpedoFallOff  = PlasmaTorpedoFallOff * PlasmaTorpedoSpeed / VelocityTorpedoSpeed
	QuantumTorpedoFallOff   = PlasmaTorpedoFallOff * PlasmaTorpedoSpeed / QuantumTorpedoSpeed
	PulseTorpedoFallOff     = PlasmaTorpedoFallOff * PlasmaTorpedoSpeed / PulseTorpedoSpeed
	FirestormTorpedoFallOff = .20 // Firestorm burn out faster
	InfernoTorpedoFallOff   = .20 // Inferno burn out faster

	// torpedoes are the best base raw damage of any seeking weapon
	PlasmaTorpedoDamageFactor    = 1.3
	EpsilonTorpedoDamageFactor   = 1.3
	ShockwaveTorpedoDamageFactor = 1.4
	VelocityTorpedoDamageFactor  = 1.2
	QuantumTorpedoDamageFactor   = 1.3
	PulseTorpedoDamageFactor     = 1.4
	FirestormTorpedoDamageFactor = 1.4
	InfernoTorpedoDamageFactor   = 1.4

	// torpedoes are seriously energy-hungry
	PlasmaTorpedoEnergyRatio    = .9
	EpsilonTorpedoEnergyRatio   = .9
	ShockwaveTorpedoEnergyRatio = .9
	VelocityTorpedoEnergyRatio  = .9
	QuantumTorpedoEnergyRatio   = 1.
	PulseTorpedoEnergyRatio     = .9
	FirestormTorpedoEnergyRatio = 1.
	InfernoTorpedoEnergyRatio   = 1.

	// Torpedoes fire rate
	PlasmaTorpedoFireRate    = 2.5 * WeaponFireRateBasis
	EpsilonTorpedoFireRate   = PlasmaTorpedoFireRate
	ShockwaveTorpedoFireRate = PlasmaTorpedoFireRate
	VelocityTorpedoFireRate  = PlasmaTorpedoFireRate
	QuantumTorpedoFireRate   = PlasmaTorpedoFireRate
	PulseTorpedoFireRate     = 2 * WeaponFireRateBasis
	FirestormTorpedoFireRate = 3 * WeaponFireRateBasis
	InfernoTorpedoFireRate   = 3 * WeaponFireRateBasis

	// standard torpedoes good against armor, suck against shields
	PlasmaTorpedoShieldBypass = -0.3
	PlasmaTorpedoArmorBypass  = 0

	// boskara firestorm torps
	FirestormTorpedoShieldBypass = -0.15
	FirestormTorpedoArmorBypass  = 0

	// shakturi inferno torps
	InfernoTorpedoShieldBypass = -0.15
	InfernoTorpedoArmorBypass  = 0

	// mortalen energy torps
	PulseTorpedoShieldBypass = -0.2
	PulseTorpedoArmorBypass  = 0
)

var (
	TorpedoWeaponData = ComponentLevelDataMap{
		"Torpedo, Plasma [S]": {
			minLevel:       0,
			maxLevel:       1,
			componentStats: PlasmaTorpedoWeaponSmall,
		},

		"Torpedo, Epsilon [S]": {
			minLevel:       2,
			maxLevel:       4,
			componentStats: EpsilonTorpedoWeaponSmall,
		},
		"Torpedo, Epsilon [M]": {
			minLevel:       2,
			maxLevel:       4,
			componentStats: DeriveMediumWeaponUsing2xDamage(EpsilonTorpedoWeaponSmall),
		},

		"Torpedo, Shockwave [M]": {
			minLevel:       5,
			maxLevel:       8,
			componentStats: DeriveMediumWeaponUsing2xDamage(ShockwaveTorpedoWeaponSmall),
		},
		"Torpedo, Shockwave [L]": {
			minLevel:       5,
			maxLevel:       8,
			componentStats: DeriveLargeWeaponUsing4xDamage(ShockwaveTorpedoWeaponSmall),
		},

		"Torpedo, Velocity [S]": {
			minLevel:       5,
			maxLevel:       8,
			componentStats: VelocityTorpedoWeaponSmall,
		},
		"Torpedo, Velocity [M]": {
			minLevel:       5,
			maxLevel:       8,
			componentStats: DeriveMediumWeaponUsing2xDamage(VelocityTorpedoWeaponSmall),
		},
		// "Torpedo, Velocity [L]": {
		// 	minLevel:       5,
		// 	maxLevel:       8,
		// 	componentStats: DeriveLargeWeaponUsing4xDamage(VelocityTorpedoWeaponSmall),
		// },

		"Torpedo, Quantum [S]": {
			minLevel:       9,
			maxLevel:       10,
			componentStats: QuantumTorpedoWeaponSmall,
		},
		"Torpedo, Quantum [M]": {
			minLevel:       9,
			maxLevel:       10,
			componentStats: DeriveMediumWeaponUsing2xDamage(QuantumTorpedoWeaponSmall),
		},
		"Torpedo, Quantum [L]": {
			minLevel:       9,
			maxLevel:       10,
			componentStats: DeriveLargeWeaponUsing4xDamage(QuantumTorpedoWeaponSmall),
		},

		"Torpedo, Pulsar [S]": {
			minLevel:       1,
			maxLevel:       10,
			componentStats: PulseTorpedoWeaponSmall,
		},
		"Torpedo, Pulsar [M]": {
			minLevel:       2,
			maxLevel:       10,
			componentStats: DeriveMediumWeaponUsing2xDamage(PulseTorpedoWeaponSmall),
		},
		"Torpedo, Pulsar [L]": {
			minLevel:       3,
			maxLevel:       10,
			componentStats: DeriveLargeWeaponUsing4xDamage(PulseTorpedoWeaponSmall),
		},

		"Torpedo, Firestorm [S]": {
			minLevel:       2,
			maxLevel:       10,
			componentStats: FirestormTorpedoWeaponSmall,
		},
		"Torpedo, Firestorm [M]": {
			minLevel:       2,
			maxLevel:       10,
			componentStats: FirestormTorpedoWeaponMedium,
		},
		"Torpedo, Firestorm [L]": {
			minLevel:       4,
			maxLevel:       10,
			componentStats: DeriveLargeWeaponUsing2xDamage(FirestormTorpedoWeaponMedium),
		},

		// we create these for t8-t10 but give them a +1 level bump above everything else
		"Torpedo, Inferno [S]": {
			minLevel:       8 + 1,
			maxLevel:       10 + 1,
			componentStats: InfernoTorpedoWeaponSmall,
		},
		"Torpedo, Inferno [M]": {
			minLevel:       8 + 1,
			maxLevel:       10 + 1,
			componentStats: InfernoTorpedoWeaponMedium,
		},
		"Torpedo, Inferno [L]": {
			minLevel:       8 + 1,
			maxLevel:       10 + 1,
			componentStats: DeriveLargeWeaponUsing2xDamage(InfernoTorpedoWeaponMedium),
		},

		//todo: Torpedo, Supernova [L]
		// note: Supernova torps are similar to t10 inferno torps

		"Ground to Space Torpedo Launchers": {
			minLevel:       3,
			maxLevel:       9,
			componentStats: PlanetaryTorpedoComponentStats,
		},
	}

	MakeTorpedoSpeedFunc = func(baseSpeed float64) LevelFunc { return MakeLinearLevelFunc(baseSpeed, baseSpeed/20.) }

	StandardTorpedoWeaponDamage        = MakeDamageFunc(PlasmaTorpedoFireRate, PlasmaTorpedoDamageFactor)
	StandardTorpedoWeaponEnergyPerShot = MakeEnergyPerShotFunc(PlasmaTorpedoEnergyRatio, StandardTorpedoWeaponDamage)
	StandardTorpedoWeaponRange         = MakeExpLevelFunc(PlasmaTorpedoBaseRange, PlasmaTorpedoRangeGrowthRate)
	StandardTorpedoWeaponFireRate      = MakeFixedLevelFunc(PlasmaTorpedoFireRate)
	StandardTorpedoWeaponSpeed         = MakeTorpedoSpeedFunc(PlasmaTorpedoSpeed)

	// Plasma Torpedo
	PlasmaTorpedoWeaponSmall = ComponentStats{
		"ComponentCountermeasuresBonus":     SeekingComponentCountermeasuresBonus,
		"ComponentTargetingBonus":           MakeFixedLevelFunc(0),
		"CrewRequirement":                   SmallCrewRequirements,
		"StaticEnergyUsed":                  SmallWeaponStaticEnergy,
		"WeaponAreaEffectRange":             MakeFixedLevelFunc(0),
		"WeaponAreaBlastWaveSpeed":          MakeFixedLevelFunc(0),
		"WeaponBombardDamageInfrastructure": MakeFixedLevelFunc(0),
		"WeaponBombardDamageMilitary":       MakeFixedLevelFunc(0),
		"WeaponBombardDamagePopulation":     MakeFixedLevelFunc(0),
		"WeaponBombardDamageQuality":        MakeFixedLevelFunc(0),
		"WeaponIonEngineDamage":             MakeFixedLevelFunc(0),
		"WeaponIonHyperDriveDamage":         MakeFixedLevelFunc(0),
		"WeaponIonSensorDamage":             MakeFixedLevelFunc(0),
		"WeaponIonShieldDamage":             MakeFixedLevelFunc(0),
		"WeaponIonWeaponDamage":             MakeFixedLevelFunc(0),
		"WeaponIonGeneralDamage":            MakeFixedLevelFunc(0),
		"WeaponDamageFalloffRatio":          MakeFixedLevelFunc(PlasmaTorpedoFallOff),
		"WeaponArmorBypass":                 MakeFixedLevelFunc(PlasmaTorpedoArmorBypass),
		"WeaponShieldBypass":                MakeFixedLevelFunc(PlasmaTorpedoShieldBypass),
		"WeaponRange":                       StandardTorpedoWeaponRange,
		"WeaponSpeed":                       StandardTorpedoWeaponSpeed,
		"WeaponFireRate":                    StandardTorpedoWeaponFireRate,
		"WeaponEnergyPerShot":               StandardTorpedoWeaponEnergyPerShot,
		"WeaponRawDamage":                   StandardTorpedoWeaponDamage,
		"WeaponVolleyAmount":                MakeFixedLevelFunc(1),
		"WeaponVolleyFireRate":              MakeFixedLevelFunc(0),
	}

	// Epsilon
	EpsilonTorpedoWeaponFireRate      = MakeFixedLevelFunc(EpsilonTorpedoFireRate)
	EpsilonTorpedoWeaponRawDamage     = MakeDamageFunc(EpsilonTorpedoFireRate, EpsilonTorpedoDamageFactor)
	EpsilonTorpedoWeaponEnergyPerShot = MakeEnergyPerShotFunc(EpsilonTorpedoEnergyRatio, EpsilonTorpedoWeaponRawDamage)
	EpsilonTorpedoWeaponSpeed         = MakeTorpedoSpeedFunc(EpsilonTorpedoSpeed)

	EpsilonTorpedoWeaponSmall = ComposeComponentStats(
		PlasmaTorpedoWeaponSmall,
		ComponentStats{
			"WeaponFireRate":           EpsilonTorpedoWeaponFireRate,
			"WeaponRawDamage":          EpsilonTorpedoWeaponRawDamage,
			"WeaponEnergyPerShot":      EpsilonTorpedoWeaponEnergyPerShot,
			"WeaponSpeed":              EpsilonTorpedoWeaponSpeed,
			"WeaponDamageFalloffRatio": MakeFixedLevelFunc(EpsilonTorpedoFallOff),
		},
	)

	// Shockwave
	ShockwaveTorpedoWeaponFireRate      = MakeFixedLevelFunc(ShockwaveTorpedoFireRate)
	ShockwaveTorpedoWeaponRawDamage     = MakeDamageFunc(ShockwaveTorpedoFireRate, ShockwaveTorpedoDamageFactor)
	ShockwaveTorpedoWeaponEnergyPerShot = MakeEnergyPerShotFunc(ShockwaveTorpedoEnergyRatio, ShockwaveTorpedoWeaponRawDamage)
	ShockwaveTorpedoWeaponSpeed         = MakeTorpedoSpeedFunc(ShockwaveTorpedoSpeed)

	ShockwaveTorpedoWeaponSmall = ComposeComponentStats(
		PlasmaTorpedoWeaponSmall,
		ComponentStats{
			"WeaponFireRate":           ShockwaveTorpedoWeaponFireRate,
			"WeaponRawDamage":          ShockwaveTorpedoWeaponRawDamage,
			"WeaponEnergyPerShot":      ShockwaveTorpedoWeaponEnergyPerShot,
			"WeaponSpeed":              ShockwaveTorpedoWeaponSpeed,
			"WeaponDamageFalloffRatio": MakeFixedLevelFunc(ShockwaveTorpedoFallOff),
		},
	)

	// Velocity
	VelocityTorpedoWeaponFireRate      = MakeFixedLevelFunc(VelocityTorpedoFireRate)
	VelocityTorpedoWeaponRawDamage     = MakeDamageFunc(VelocityTorpedoFireRate, VelocityTorpedoDamageFactor)
	VelocityTorpedoWeaponEnergyPerShot = MakeEnergyPerShotFunc(VelocityTorpedoEnergyRatio, VelocityTorpedoWeaponRawDamage)
	VelocityTorpedoWeaponSpeed         = MakeTorpedoSpeedFunc(VelocityTorpedoSpeed)

	VelocityTorpedoWeaponSmall = ComposeComponentStats(
		PlasmaTorpedoWeaponSmall,
		ComponentStats{
			"WeaponFireRate":           VelocityTorpedoWeaponFireRate,
			"WeaponRawDamage":          VelocityTorpedoWeaponRawDamage,
			"WeaponEnergyPerShot":      VelocityTorpedoWeaponEnergyPerShot,
			"WeaponSpeed":              VelocityTorpedoWeaponSpeed,
			"WeaponDamageFalloffRatio": MakeFixedLevelFunc(VelocityTorpedoFallOff),
		},
	)

	// Quantum
	QuantumTorpedoWeaponFireRate      = MakeFixedLevelFunc(QuantumTorpedoFireRate)
	QuantumTorpedoWeaponRawDamage     = MakeDamageFunc(QuantumTorpedoFireRate, QuantumTorpedoDamageFactor)
	QuantumTorpedoWeaponEnergyPerShot = MakeEnergyPerShotFunc(QuantumTorpedoEnergyRatio, QuantumTorpedoWeaponRawDamage)
	QuantumTorpedoWeaponSpeed         = MakeTorpedoSpeedFunc(QuantumTorpedoSpeed)

	QuantumTorpedoWeaponSmall = ComposeComponentStats(
		PlasmaTorpedoWeaponSmall,
		ComponentStats{
			"WeaponFireRate":           QuantumTorpedoWeaponFireRate,
			"WeaponRawDamage":          QuantumTorpedoWeaponRawDamage,
			"WeaponEnergyPerShot":      QuantumTorpedoWeaponEnergyPerShot,
			"WeaponSpeed":              QuantumTorpedoWeaponSpeed,
			"WeaponDamageFalloffRatio": MakeFixedLevelFunc(QuantumTorpedoFallOff),
		},
	)

	// Pulse
	PulseTorpedoWeaponFireRate      = MakeFixedLevelFunc(PulseTorpedoFireRate)
	PulseTorpedoWeaponRawDamage     = MakeDamageFunc(PulseTorpedoFireRate, PulseTorpedoDamageFactor)
	PulseTorpedoWeaponEnergyPerShot = MakeEnergyPerShotFunc(PulseTorpedoEnergyRatio, PulseTorpedoWeaponRawDamage)
	PulseTorpedoWeaponSpeed         = MakeTorpedoSpeedFunc(PulseTorpedoSpeed)

	PulseTorpedoWeaponSmall = ComposeComponentStats(
		PlasmaTorpedoWeaponSmall,
		ComponentStats{
			"ComponentTargetingBonus":  MakeFixedLevelFunc(0.1),
			"WeaponFireRate":           PulseTorpedoWeaponFireRate,
			"WeaponRawDamage":          PulseTorpedoWeaponRawDamage,
			"WeaponEnergyPerShot":      PulseTorpedoWeaponEnergyPerShot,
			"WeaponSpeed":              PulseTorpedoWeaponSpeed,
			"WeaponDamageFalloffRatio": MakeFixedLevelFunc(PulseTorpedoFallOff),
			"WeaponArmorBypass":        MakeFixedLevelFunc(PulseTorpedoArmorBypass),
			"WeaponShieldBypass":       MakeFixedLevelFunc(PulseTorpedoShieldBypass),
		},
	)

	// Firestorm
	FirestormTorpedoWeaponFireRate      = MakeFixedLevelFunc(FirestormTorpedoFireRate)
	FirestormTorpedoWeaponRawDamage     = MakeDamageFunc(FirestormTorpedoFireRate, FirestormTorpedoDamageFactor)
	FirestormTorpedoWeaponEnergyPerShot = MakeEnergyPerShotFunc(FirestormTorpedoEnergyRatio, FirestormTorpedoWeaponRawDamage)
	FirestormTorpedoWeaponSpeed         = MakeTorpedoSpeedFunc(FirestormTorpedoSpeed)
	FirestormTorpedoFalloffRatio        = MakeFixedLevelFunc(FirestormTorpedoFallOff)

	FirestormTorpedoWeaponSmall = ComposeComponentStats(
		PlasmaTorpedoWeaponSmall,
		ComponentStats{
			"ComponentTargetingBonus":  MakeFixedLevelFunc(0.1),
			"WeaponFireRate":           FirestormTorpedoWeaponFireRate,
			"WeaponRawDamage":          FirestormTorpedoWeaponRawDamage,
			"WeaponEnergyPerShot":      FirestormTorpedoWeaponEnergyPerShot,
			"WeaponSpeed":              FirestormTorpedoWeaponSpeed,
			"WeaponDamageFalloffRatio": MakeFixedLevelFunc(FirestormTorpedoFallOff),
			"WeaponArmorBypass":        MakeFixedLevelFunc(FirestormTorpedoArmorBypass),
			"WeaponShieldBypass":       MakeFixedLevelFunc(FirestormTorpedoShieldBypass),
		},
	)

	FirestormTorpedoWeaponMedium = ComposeComponentStats(
		DeriveMediumWeaponUsing2xDamage(FirestormTorpedoWeaponSmall),
		ComponentStats{
			"WeaponBombardDamageInfrastructure": MakeScaledFuncLevelFunc(.25, AtomicBombardInfrastructure),
			"WeaponBombardDamageMilitary":       MakeScaledFuncLevelFunc(.25, AtomicBombardMilitary),
			"WeaponBombardDamagePopulation":     MakeScaledFuncLevelFunc(.25, AtomicBombardPopulation),
			"WeaponBombardDamageQuality":        MakeScaledFuncLevelFunc(.25, AtomicBombardQuality),
		},
	)

	// Inferno
	InfernoTorpedoWeaponFireRate      = MakeFixedLevelFunc(InfernoTorpedoFireRate)
	InfernoTorpedoWeaponRawDamage     = MakeDamageFunc(InfernoTorpedoFireRate, InfernoTorpedoDamageFactor)
	InfernoTorpedoWeaponEnergyPerShot = MakeEnergyPerShotFunc(InfernoTorpedoEnergyRatio, InfernoTorpedoWeaponRawDamage)
	InfernoTorpedoWeaponSpeed         = MakeTorpedoSpeedFunc(InfernoTorpedoSpeed)
	InfernoTorpedoFalloffRatio        = MakeFixedLevelFunc(InfernoTorpedoFallOff)

	InfernoTorpedoWeaponSmall = ComposeComponentStats(
		PlasmaTorpedoWeaponSmall,
		ComponentStats{
			"ComponentTargetingBonus":  MakeFixedLevelFunc(0.11),
			"WeaponFireRate":           InfernoTorpedoWeaponFireRate,
			"WeaponRawDamage":          InfernoTorpedoWeaponRawDamage,
			"WeaponEnergyPerShot":      InfernoTorpedoWeaponEnergyPerShot,
			"WeaponSpeed":              InfernoTorpedoWeaponSpeed,
			"WeaponDamageFalloffRatio": MakeFixedLevelFunc(InfernoTorpedoFallOff),
			"WeaponArmorBypass":        MakeFixedLevelFunc(InfernoTorpedoArmorBypass),
			"WeaponShieldBypass":       MakeFixedLevelFunc(InfernoTorpedoShieldBypass),
		},
	)

	InfernoTorpedoWeaponMedium = ComposeComponentStats(
		DeriveMediumWeaponUsing2xDamage(InfernoTorpedoWeaponSmall),
		ComponentStats{
			"WeaponBombardDamageInfrastructure": MakeScaledFuncLevelFunc(.3333333, AtomicBombardInfrastructure),
			"WeaponBombardDamageMilitary":       MakeScaledFuncLevelFunc(.3333333, AtomicBombardMilitary),
			"WeaponBombardDamagePopulation":     MakeScaledFuncLevelFunc(.3333333, AtomicBombardPopulation),
			"WeaponBombardDamageQuality":        MakeScaledFuncLevelFunc(.3333333, AtomicBombardQuality),
		},
	)

	PlanetaryTorpedoComponentStats = DerivePlanetaryFromLargeWeapon(DeriveLargeWeaponUsing4xDamage(EpsilonTorpedoWeaponSmall))
)
