package algorithm

import (
	"fmt"

	"github.com/lucky-wolf/xml-tree/etc"
	"github.com/lucky-wolf/xml-tree/xmltree"
)

func Troops(folder string) (err error) {

	err = fmt.Errorf("Troops is not yet implemented")

	fmt.Println("All troops will be generated from standard data")

	// load all component definition files
	j, err := LoadJobFor(folder, "TroopDefinitions*.xml")
	if err != nil {
		return
	}

	// apply this transformation
	err = j.applyTroops()
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

type Value = any
type Key = string
type UnitType string
type TroopTypeLookup map[UnitType]TroopValues
type TroopValues map[Key]Value

// type TroopTypeDefinition struct {
// 	Values     TroopValue
// 	RulesTable RulesTable // any additional visitors per troop
// }

// var (
// 	TroopTypeDefinitions = map[string]TroopValue{
// 		"Battle Droid": {
// 	}
// )

func CombineTroopValues(a, b TroopValues) TroopValues {
	c := make(TroopValues)
	for k, v := range a {
		c[k] = v
	}
	for k, v := range b {
		c[k] = v
	}
	return c
}

func ScaleTroopIntValues(a TroopValues, scale float64, keys ...Key) TroopValues {
	c := make(TroopValues)
	for k, v := range a {
		if etc.IsOneOf(k, keys...) {
			v = int(float64(v.(int)) * scale)
		}
		c[k] = v
	}
	return c
}

func ScaledTroopValues(a TroopValues, scale float64) TroopValues {
	c := make(TroopValues)
	for k, v := range a {
		c[k] = v
	}
	c["Size"] = int(float64(c["Size"].(int)) * scale)
	c["RecruitmentCost"] = map[Key]Value{"Money": int(float64(c["RecruitmentCost"].(map[Key]Value)["Money"].(int)) * scale)}
	c["MaintenanceCost"] = int(float64(c["MaintenanceCost"].(int)) * scale)
	c["AttackStrength"] = int(float64(c["AttackStrength"].(int)) * scale)
	c["DefendStrength"] = int(float64(c["DefendStrength"].(int)) * scale)
	return c
}

const (
	defaultRaceID RaceID = 255

	StandardInfSize        = 5000
	StandardInfCost        = 2500
	StandardInfMaintenance = StandardInfCost / 10
	StandardInfAttack      = 100
	StandardInfDefense     = 150

	StandardInfantryEvasion = 0.2
	StandardArmorEvasion    = 0.1
	StandardPDUEvasion      = 0.05
	StandardSpecialEvasion  = 0.4
	StandardTitanEvasion    = 0.2

	SmallTroopsFactor = 3 / 5
	LargeTroopsFactor = 5 / 3

	StandardArmorSize        = StandardInfSize * 2
	StandardArmorCost        = StandardInfCost * 2
	StandardArmorMaintenance = StandardInfMaintenance * 2
	StandardArmorAttack      = StandardInfAttack * 2
	StandardArmorDefense     = StandardInfDefense

	StandardPDUSize        = StandardInfSize
	StandardPDUCost        = StandardInfCost
	StandardPDUMaintenance = StandardInfMaintenance
	StandardPDUAttack      = StandardInfDefense
	StandardPDUDefense     = StandardInfAttack

	StandardSpecialSize        = StandardInfSize
	StandardSpecialCost        = StandardInfCost
	StandardSpecialMaintenance = StandardInfMaintenance
	StandardSpecialAttack      = StandardInfAttack
	StandardSpecialDefense     = StandardInfDefense

	StandardDroidSize        = 2000
	StandardDroidCost        = StandardInfCost
	StandardDroidMaintenance = StandardInfMaintenance
	StandardDroidAttack      = 80
	StandardDroidDefense     = 80

	StandardBattleBotSize        = 2 * StandardDroidSize
	StandardBattleBotCost        = 2 * StandardDroidCost
	StandardBattleBotMaintenance = 2 * StandardDroidMaintenance
	StandardBattleBotAttack      = 2 * StandardDroidAttack
	StandardBattleBotDefense     = 2 * StandardDroidDefense

	StandardTitanSize        = 2 * StandardBattleBotSize
	StandardTitanCost        = 2 * StandardBattleBotCost
	StandardTitanMaintenance = 2 * StandardBattleBotMaintenance
	StandardTitanAttack      = 2 * StandardBattleBotAttack
	StandardTitanDefense     = 2 * StandardBattleBotDefense
)

var (
	RacialTroopMappings = map[RaceID]TroopTypeLookup{
		defaultRaceID: {
			"Infantry":         AverageInfantry,
			"Armor":            AverageArmor,
			"Titan":            AverageTitan,
			"PlanetaryDefense": AveragePDU,
			"SpecialForces":    AverageSpecialForces,
		},
	}

	AverageInfantry = TroopValues{
		"Size":                    StandardInfSize,
		"RecruitmentCost":         map[Key]Value{"Money": StandardInfCost},
		"MaintenanceCost":         StandardInfMaintenance,
		"AttackStrength":          StandardInfAttack,
		"DefendStrength":          StandardInfDefense,
		"EvasionInfantry":         StandardInfantryEvasion,
		"EvasionArmor":            StandardInfantryEvasion,
		"EvasionTitan":            StandardInfantryEvasion,
		"EvasionPlanetaryDefense": StandardInfantryEvasion,
		"EvasionSpecialForces":    StandardInfantryEvasion,
	}

	AverageArmor = TroopValues{
		"Size":                    StandardArmorSize,
		"RecruitmentCost":         map[Key]Value{"Money": StandardArmorCost},
		"MaintenanceCost":         StandardArmorMaintenance,
		"AttackStrength":          StandardArmorAttack,
		"DefendStrength":          StandardArmorDefense,
		"EvasionInfantry":         StandardArmorEvasion,
		"EvasionArmor":            StandardArmorEvasion,
		"EvasionTitan":            StandardArmorEvasion,
		"EvasionPlanetaryDefense": StandardArmorEvasion,
		"EvasionSpecialForces":    StandardArmorEvasion,
	}

	AveragePDU = TroopValues{
		"Size":                    StandardPDUSize,
		"RecruitmentCost":         map[Key]Value{"Money": StandardPDUCost},
		"MaintenanceCost":         StandardPDUMaintenance,
		"AttackStrength":          StandardPDUAttack,
		"DefendStrength":          StandardPDUDefense,
		"EvasionInfantry":         StandardPDUEvasion,
		"EvasionArmor":            StandardPDUEvasion,
		"EvasionTitan":            StandardPDUEvasion,
		"EvasionPlanetaryDefense": StandardPDUEvasion,
		"EvasionSpecialForces":    StandardPDUEvasion,
	}

	AverageSpecialForces = TroopValues{
		"Size":                    StandardSpecialSize,
		"RecruitmentCost":         map[Key]Value{"Money": StandardSpecialCost},
		"MaintenanceCost":         StandardSpecialMaintenance,
		"AttackStrength":          StandardSpecialAttack,
		"DefendStrength":          StandardSpecialDefense,
		"EvasionInfantry":         StandardSpecialEvasion,
		"EvasionArmor":            StandardSpecialEvasion,
		"EvasionTitan":            StandardSpecialEvasion,
		"EvasionPlanetaryDefense": StandardSpecialEvasion,
		"EvasionSpecialForces":    StandardSpecialEvasion,
	}

	AverageTitan = TroopValues{
		"Size":                    StandardTitanSize,
		"RecruitmentCost":         map[Key]Value{"Money": StandardTitanCost},
		"MaintenanceCost":         StandardTitanMaintenance,
		"AttackStrength":          StandardTitanAttack,
		"DefendStrength":          StandardTitanDefense,
		"EvasionInfantry":         StandardTitanEvasion,
		"EvasionArmor":            StandardTitanEvasion,
		"EvasionTitan":            StandardTitanEvasion,
		"EvasionPlanetaryDefense": StandardTitanEvasion,
		"EvasionSpecialForces":    StandardTitanEvasion,
	}

	SmallInfantry      = ScaledTroopValues(AverageInfantry, SmallTroopsFactor)
	SmallArmor         = ScaledTroopValues(AverageArmor, SmallTroopsFactor)
	SmallDefender      = ScaledTroopValues(AveragePDU, SmallTroopsFactor)
	SmallSpecialForces = ScaledTroopValues(AverageSpecialForces, SmallTroopsFactor)
	SmallTitan         = ScaledTroopValues(AverageTitan, SmallTroopsFactor)
)

func (j *Job) applyTroops() (err error) {

	for _, f := range j.xfiles {

		// the root will result in a single ArrayOf[RootObjectType]
		for _, e := range f.root.Elements.Elements() {

			err = assertIs(e, "ArrayOfTroopDefinition")
			if err != nil {
				return
			}

			for _, e := range e.Elements() {

				// each of these is a componentdefinition
				err = assertIs(e, "TroopDefinition")
				if err != nil {
					return
				}

				// what is it
				unitType := UnitType(e.Child("Type").StringValue())
				unitRace := RaceID(e.Child("RaceId").IntValue())

				// apply the values
				err = applyUnitRaceValue(e, unitType, unitRace)
				if err != nil {
					return
				}
			}
		}
	}

	return
}

func applyUnitRaceValue(e *xmltree.XMLElement, unitType UnitType, unitRace RaceID) (err error) {

	// see if we have any data for this race
	tt, ok := RacialTroopMappings[unitRace]
	if !ok {
		fmt.Printf("Nothing for %s\n", unitRace.Name())
		return
	}

	// get the values for this type of unit
	values, ok := tt[unitType]
	if !ok {
		fmt.Printf("Using default values for %s %s\n", unitRace.Name(), unitType)
		values = RacialTroopMappings[defaultRaceID][unitType]
	}

	// apply the values to the element
	for k, v := range values {
		err = SetValueRecursive(e, k, v)
		if err != nil {
			return
		}
	}

	return
}

func SetValueRecursive(e *xmltree.XMLElement, k string, v any) (err error) {

	// find the child
	c := e.Child(k)
	if c == nil {
		err = fmt.Errorf("Warn: no child %s in %s, will create...", k, e.Name.Local)
	}

	// either directly set the value or decompose it recursively
	switch m := v.(type) {
	case map[string]any:
		if c == nil {
			// create the child if it does not exist
			c = xmltree.MakeElementWithValue(k, []any{})
			err = e.InsertAt(-1, c) //todo: we need the ability to get the "child index" of the length
			if err != nil {
				return
			}
		}
		// complex value, recurse
		for k, v := range m {
			SetValueRecursive(c, k, v)
		}
	default:
		if c == nil {
			// create the child if it does not exist
			c = xmltree.MakeElementWithValue(k, v)
			err = e.InsertAt(-1, c)
			if err != nil {
				return
			}
		} else {
			// simple value, set it
			c.SetValue(v)
		}
	}

	return
}
