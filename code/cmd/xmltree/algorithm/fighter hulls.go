package algorithm

import (
	"fmt"
	"regexp"

	"github.com/lucky-wolf/xml-tree/etc"
	"github.com/lucky-wolf/xml-tree/xmltree"
)

// we'll put hull related support algos here
type RacialQuirk struct {
	bomberCenterIsTwo bool
}
type RacialQuirks map[RaceID]RacialQuirk

type RacialNames map[RaceID]string

var (
	ComponentBayOrder = []string{"Weapon", "Hangar", "Engine", "Sensor", "Defense", "General"}

	FighterNames = RacialNames{
		Ackdarian: "Voidswimmer",
		Atuuk:     "Skyhunter",
		Boskara:   "Mandible",
		Dhayut:    "Tarantula",
		Gizurean:  "Wasp",
		Haakonish: "Toxinspitter",
		Human:     "Gyrfalcon",
		Ikkuro:    "Dawnstrike",
		Mortalen:  "Dragonclaw",
		Quameno:   "Pulseweaver",
		Teekan:    "Gnawfang",
		Wekkarus:  "Wavedancer",
		Zenox:     "Frostclaw",
	}

	BomberNames = RacialNames{
		Ackdarian: "Depthstrider",
		Atuuk:     "Skydeath",
		Boskara:   "Burrower",
		Dhayut:    "Orb Weaver",
		Gizurean:  "Hornet",
		Haakonish: "Firebelly",
		Human:     "Harpy",
		Ikkuro:    "Deathsong",
		Mortalen:  "Whiptail",
		Quameno:   "Singularity",
		Teekan:    "Diamondback",
		Wekkarus:  "Leviathan",
		Zenox:     "Blizzard",
	}

	// what we wish it were
	fighterHullSchedule = HullBaySchedule{
		"FighterInterceptor": {
			Tier: MakeTierLookupFunc(HullTiers{0: 1, 2: 2, 4: 3, 9: 4, 13: 5, 15: 6}),
			ValuesTable: ValuesTable{
				"ArmorReactiveRating":  MakeLinearLevelFunc(0, 2),
				"IonDefense":           MakeLinearLevelFunc(0, 4),
				"CountermeasuresBonus": MakeLinearLevelFunc(0.20, 0.06),
				"TargetingBonus":       MakeLinearLevelFunc(0, 0.1),
			},
			BonusesTable: ComponentStats{
				"ShipManeuvering": MakeLinearLevelFunc(0, 0.08),
				"WeaponsDamage": func(level int) float64 {
					if level > 3 {
						return float64(level-3) * 0.10
					} else {
						return 0
					}
				},
			},
			AprioriBayCounts: BayCountsPerLevel{
				// Fighter I
				1: {
					"Weapon":  1,
					"Engine":  1,
					"Defense": 1,
					"General": 2,
				},
				// Fighter II
				2: {
					"Weapon":  1,
					"Engine":  2,
					"Defense": 2,
					"General": 2,
				},
				// Fighter III
				3: {
					"Weapon":  2,
					"Engine":  2,
					"Defense": 2,
					"General": 3,
				},
				// Fighter IV
				4: {
					"Weapon":  2,
					"Engine":  3,
					"Defense": 3,
					"General": 4,
				},
				// Fighter V
				5: {
					"Weapon":  2,
					"Engine":  3,
					"Defense": 4,
					"General": 4,
				},
				// Fighter VI
				6: {
					"Weapon":  2, // fighter models don't have a 3rd weapon
					"Engine":  3,
					"Defense": 4,
					"General": 5,
				},
			},
		},
		"FighterBomber": {
			Tier: MakeTierLookupFunc(HullTiers{0: 1, 1: 2, 2: 3, 4: 4, 5: 5, 6: 6}),
			ValuesTable: ComponentStats{
				"ArmorReactiveRating":  MakeLinearLevelFunc(0, 2),
				"IonDefense":           MakeLinearLevelFunc(0, 4),
				"CountermeasuresBonus": MakeLinearLevelFunc(.20, .06),
				"TargetingBonus":       MakeLinearLevelFunc(0, .1),
			},
			BonusesTable: ComponentStats{
				"ShipManeuvering": MakeLinearLevelFunc(0, .04),
				"WeaponsDamage": func(level int) float64 {
					if level > 3 {
						return float64(level-3) * 0.10
					} else {
						return 0
					}
				},
			},
			AprioriBayCounts: BayCountsPerLevel{
				// Bomber I
				1: {
					"Weapon":  1,
					"Engine":  1,
					"Defense": 1,
					"General": 2,
				},
				// Bomber II
				2: {
					"Weapon":  1,
					"Engine":  2,
					"Defense": 2,
					"General": 2,
				},
				// Bomber III
				3: {
					"Weapon":  2,
					"Engine":  2,
					"Defense": 2,
					"General": 3,
				},
				// Bomber IV
				4: {
					"Weapon":  2,
					"Engine":  3,
					"Defense": 3,
					"General": 4,
				},
				// Bomber V
				5: {
					"Weapon":  2,
					"Engine":  3,
					"Defense": 4,
					"General": 4,
				},
				// Bomber VI
				6: {
					"Weapon":  3,
					"Engine":  3,
					"Defense": 4,
					"General": 5,
				},
			},
		},
	}
)

// racial quirks (we might just want to supply translator functions for various mesh indexes)
var racialQuirks = RacialQuirks{
	Ackdarian: {bomberCenterIsTwo: true},
	Ikkuro:    {bomberCenterIsTwo: true},
	Teekan:    {bomberCenterIsTwo: true},
}

// applies the name to the fighter hull based on race + level
func applyFighterName(shipHull *xmltree.XMLElement, logicalLevel int, table RacialNames) {
	n := shipHull.Child("Name")
	var name string
	if logicalLevel == 0 {
		name = "Unused"
	} else {
		name = table[RaceID(shipHull.Child("RaceId").IntValue())] + tierSuffixes[logicalLevel-1]
	}
	n.SetValue(name)
}

// because some races use 0,1,2 for left,right,center, but others use 1,2,0 for same, we have to do stupid shit here
func getFighterWeaponMeshIndex(shipHull *xmltree.XMLElement, index int, desiredCounts BayCounts) int {

	role := shipHull.Child("Role").StringValue()
	if role != "FighterBomber" {
		return index
	}

	race := RaceID(shipHull.Child("RaceId").IntValue())
	zeroCenter := !racialQuirks[race].bomberCenterIsTwo
	if zeroCenter {
		if desiredCounts["Weapon"] == 1 {
			// only a single weapon means use the center
			return 0
		}
		switch index {
		case 0:
			return 1
		case 1:
			return 2
		case 2:
			return 0
		}
	} else if desiredCounts["Weapon"] == 1 {
		// only a single weapon means use the center mesh
		return 2
	}
	return index
}

func FighterHulls(folder string) (err error) {

	// load all ship hull definition files
	j, err := LoadJobFor(folder, "ShipHulls*.xml")
	if err != nil {
		return
	}

	// apply this transformation
	err = j.applyFighterHulls()
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

func (j *Job) applyFighterHulls() (err error) {
	return j.applyComponentBays(fighterHullSchedule)
}

func (j *Job) applyComponentBays(schedule HullBaySchedule) (err error) {

	fmt.Println("All strikecraft component bay counts will be adjusted to match desired schedule")

	for _, f := range j.xfiles {

		// the root will result in a single ArrayOf[RootObjectType]
		for _, shiphulls := range f.root.Elements.Elements() {

			err = assertIs(shiphulls, "ArrayOfShipHull")
			if err != nil {
				return
			}

			for _, shipHull := range shiphulls.Elements() {

				// each of these is a ShipHull
				err = assertIs(shipHull, "ShipHull")
				if err != nil {
					return
				}

				// see whether we have a schedule for this role
				roleName := shipHull.Child("Role").StringValue()
				hullDefn, ok := schedule[roleName]
				if !ok {
					continue
				}

				f.stats.objects++

				logicalLevel, ok := hullDefn.Tier(shipHull.Child("Level").IntValue())

				// set name (even if we don't have a level mapping)
				switch roleName {
				case "FighterInterceptor":
					applyFighterName(shipHull, logicalLevel, FighterNames)
				case "FighterBomber":
					applyFighterName(shipHull, logicalLevel, BomberNames)
				}

				// do nothing more for this level if we don't have a logical level
				if !ok {
					continue
				}

				desiredCounts, ok := hullDefn.AprioriBayCounts[logicalLevel]
				if !ok {
					continue
				}

				f.stats.elements++

				// set the slot counts
				err = applyFighterComponentBaySchedule(shipHull, desiredCounts)
				if err != nil {
					return
				}

				// determine how much capacity we'd need to fill every available component bay
				idealCapacity := j.totalComponentBaySize(shipHull.Child("ComponentBays"))

				// set engine limit
				if count, ok := desiredCounts["Engine"]; ok {
					shipHull.Child("EngineLimit").SetValue(count)
					// remove extra engine size if it was auto-added
					// note: we add an extra engine bay if the max count is even to ensure we can do odd numbers of engines
					if count%2 == 0 {
						ebay := shipHull.Child("ComponentBays").FindRecurse("Type", "Engine")
						idealCapacity -= ebay.Child("MaximumComponentSize").IntValue()
					}
				}

				// give room for 5/8ths of ideal capacity needed
				// note: this is an arbitrary ratio to keep things reasonably tight
				capacity := etc.MulDivRoundUp(idealCapacity, 5, 8)
				// size is hp (we make bombers 20% stronger & bigger)
				var size int
				switch roleName {
				case "FighterInterceptor":
					size = int(float64(capacity) * .5)
				case "FighterBomber":
					size = int(float64(capacity) * .6)
				default:
					err = fmt.Errorf("what the hell is this doing here: %s", roleName)
					return
				}
				maxSize := capacity + size
				shipHull.Child("Size").SetValue(size)
				shipHull.Child("MaximumSize").SetValue(maxSize)

				// fighters & bombers display size is 1/2 that of ships (see ShipDisplaySizeRule)
				dispSize := int(float64(maxSize) / 4.0)
				shipHull.Child("DisplaySize").SetValue(dispSize)

				// set some purely tier based numbers
				for key, value := range hullDefn.ValuesTable {
					shipHull.SetChildValue(key, value(logicalLevel))
				}

				// apply bonuses table
				// todo: use racial bonuses
				err = applyHullBonusesTable(shipHull, hullDefn.BonusesTable, logicalLevel)
				if err != nil {
					return
				}

				f.stats.changed++
			}
		}
	}

	return
}

func applyFighterComponentBaySchedule(shipHull *xmltree.XMLElement, desiredCounts BayCounts) (err error) {

	// scan the component bays by type to figure out where they each begin (and their counts)
	indexes, err := getComponentBayIndexesFromHull(shipHull)
	if err != nil {
		return
	}

	// insert or remove elements from collection at the now known indexes (in reverse order)
	for _, componentBayType := range reverseComponentBayOrder {

		desired, ok := desiredCounts[componentBayType]
		if !ok {
			continue
		}
		actual := indexes[componentBayType].count

		switch componentBayType {

		case "Weapon":
			// as of 1218 DW2 wants 90deg args for fighters and bombers
			// hack throwing this in an odd place because it just doesn't really fit anywhere
			componentBays := shipHull.Child("ComponentBays").Elements()
			for i := indexes[componentBayType].start; i < indexes[componentBayType].start+actual; i++ {
				componentBays[i].SetChildValue("RotationHalfArcRange", 0.785)
			}

		case "Engine":
			if desiredCounts["Engine"]%2 == 0 {
				// note: always make engine bays odd to allow for the game to balance them
				// note: but we don't allocate space for it, and we set the EngineLimit according to the schedule
				desired += 1
			}
			// case "General":
			// 	// as of 1205 we should be able to add only 2 general slots for strike craft
			// 	desired = max(3, desired)
		}

		switch {
		case desired > actual:
			// append copies
			i := indexes[componentBayType].start + actual
			e := shipHull.Child("ComponentBays").Elements()[i-1]
			for c, d := 0, desired-actual; c < d; c++ {
				err = shipHull.Child("ComponentBays").InsertAt(i+c, e.Clone())
				if err != nil {
					return
				}
			}
		case actual > desired:
			// delete unneeded elements
			err = shipHull.Child("ComponentBays").RemoveSpan(indexes[componentBayType].start+desired, actual-desired)
			if err != nil {
				return
			}
		}
	}

	// renumber everything to ensure it's coherent
	err = renumberFighterComponentBays(shipHull, desiredCounts)

	return
}

// builds a BayTypeGroups for the caller
func getComponentBayIndexesFromHull(shipHull *xmltree.XMLElement) (counts BayTypeGroups, err error) {

	componentbays := shipHull.Child("ComponentBays")
	if componentbays == nil {
		err = fmt.Errorf("element has no component bays")
		return
	}

	// scan the component bays in order, inserting or deleting as needed

	// initialize our map (golang will panic otherwise)
	counts = BayTypeGroups{}

	for i, c := range componentbays.Elements() {

		// figure out what type this is
		bayType := c.Child("Type").StringValue()

		// update our start index & count
		g := counts[bayType]
		if g.count == 0 {
			g.start = i
		}
		g.count++
		counts[bayType] = g
	}

	// fix the missing bay types by giving them the index of where they would have been
	index := 0
	for _, bayType := range ComponentBayOrder {
		count := counts[bayType].count
		if count == 0 {
			counts[bayType] = BayTypeIndexes{index, 0}
		} else {
			index += counts[bayType].count
		}
	}

	return
}

// matches #weapon0 and the like...
var hullComponentMeshRegex = regexp.MustCompile(`(#\w+)\d+`)

// renumbers all elements and returns the group counts (weapon, defense, etc.)
func renumberFighterComponentBays(shipHull *xmltree.XMLElement, desiredCounts BayCounts) (err error) {

	// track our counts as we renumber
	counts := BayCounts{}

	// scan the component bays in order
	for i, c := range shipHull.Child("ComponentBays").Elements() {

		// id is trivial - just linear numbering within this list
		c.Child("ComponentBayId").SetValue(i)

		// mesh name is harder...
		// determine what type of component bay this is (we count mesh names independently)
		t := c.Child("Type").StringValue()

		// get the current count of this type (starts at zero)
		if m := c.Child("MeshName"); m != nil && hullComponentMeshRegex.MatchString(m.StringValue()) {

			ci := counts[t]

			switch t {
			case "Weapon":
				// get the translated mesh index for this weapon for this race
				ci = getFighterWeaponMeshIndex(shipHull, ci, desiredCounts)
			}

			oldname := m.StringValue()
			newname := hullComponentMeshRegex.ReplaceAllString(oldname, fmt.Sprintf("${1}%d", ci))

			// update this one to match our count
			m.SetString(newname)

			// update our count for the next one
			counts[t] += 1
		}
	}

	return
}

// adds up all the bay sizes
func (j *Job) totalComponentBaySize(componentbays *xmltree.XMLElement) (size int) {

	// scan the component bays in order
	for _, c := range componentbays.Elements() {
		size += c.Child("MaximumComponentSize").IntValue()
	}

	return
}
