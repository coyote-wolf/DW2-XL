package algorithm

func DamageControl(folder string) (err error) {

	// load all component definition files
	j, err := LoadJobFor(folder, "ComponentDefinitions*.xml")
	if err != nil {
		return
	}

	// apply this transformation
	err = j.applyDamageControl()
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

func (j *Job) applyDamageControl() (err error) {

	// apply stats for each component
	err = j.ApplyComponentAll(DamageControlData)

	return
}

const (
	// HullRepairBasis            = 0.05                 // <HullRepair> is dead code -- don't use!
	RepairRateExp              = WeaponDamageIncreaseExp // tie repair improvements to the same growth rate of weapon damage
	DamageReductionBasis       = 0.10                    // vanilla starts at 25% at t1, ends at 45% at t6
	DamageRepairBasis          = 0.05                    // vanilla starts at .05 at t1, ends at .50 at t6
	DestroyedRepairChanceBasis = 0.005                   // vanilla starts at .5% at t4, ends at 1.% at t6
	DamageRepairEnergyRatio    = 50

	OrganicCoefficient = 1.333333333
)

var (
	StandardRepairMaintenanceSavings = MakeLinearLevelFunc(0.05, 0.01)
	StandardDamageReduction          = MakeExpLevelFunc(DamageReductionBasis, RepairRateExp)
	StandardDamageRepair             = MakeExpLevelFunc(DamageRepairBasis, RepairRateExp)
	StandardDestroyedRepairChance    = MakeExpLevelFunc(DestroyedRepairChanceBasis, RepairRateExp)
	StandardRepairEnergyUsed         = MakeScaledFuncLevelFunc(DamageRepairEnergyRatio, StandardDamageRepair)
	StandardRepairCrewRequirement    = MakeFixedLevelFunc(10)

	DamageControlData = ComponentLevelDataMap{
		"Damage Control, A1C Bot": {
			minLevel:       0,
			maxLevel:       1,
			componentStats: A1CComponentStats,
		},
		"Damage Control, R27 Bot": {
			minLevel:       2,
			maxLevel:       4,
			componentStats: R27ComponentStats,
		},
		"Damage Control, Nano Bots": {
			minLevel:       5,
			maxLevel:       7,
			componentStats: NanoBotsComponentStats,
		},
		"Damage Control, Nano Swarms": {
			minLevel:       8,
			maxLevel:       10,
			componentStats: NanoSwarmsComponentStats,
		},

		"Damage Control, Organic": {
			minLevel:       0,
			maxLevel:       10,
			componentStats: OrganicComponentStats,
		},
	}

	// basic
	A1CComponentStats = ComponentStats{
		"CommandMaintenanceSavings": StandardRepairMaintenanceSavings,
		"ComponentIonDefense":       HardenedComponentIonDefense,
		"CrewRequirement":           StandardRepairCrewRequirement,
		"DamageReduction":           StandardDamageReduction,
		"DamageRepair":              StandardDamageRepair,
		"DestroyedRepairChance":     StandardDestroyedRepairChance,
		"StaticEnergyUsed":          StandardRepairEnergyUsed,
	}

	// R27
	R27ComponentStats = ComposeComponentStats(
		A1CComponentStats,
		ComponentStats{
			"CrewRequirement": MakeFixedLevelFunc(8),
		},
	)

	// NanoBots
	NanoBotsComponentStats = ComposeComponentStats(
		A1CComponentStats,
		ComponentStats{
			"CrewRequirement": MakeFixedLevelFunc(6),
		},
	)

	// NanoSwarms
	NanoSwarmsComponentStats = ComposeComponentStats(
		A1CComponentStats,
		ComponentStats{
			"CrewRequirement": MakeFixedLevelFunc(4),
		},
	)

	// Organic
	OrganicComponentStats = ComponentStats{
		"CommandMaintenanceSavings": MakeScaledFuncLevelFunc(OrganicCoefficient, StandardRepairMaintenanceSavings),
		"ComponentIonDefense":       MakeScaledFuncLevelFunc(2, HardenedComponentIonDefense), // organic is doubly hardened
		"CrewRequirement":           MakeIntegerLevelFunc(MakeScaledFuncLevelFunc(2.0-OrganicCoefficient, StandardRepairCrewRequirement)),
		"DamageReduction":           MakeScaledFuncLevelFunc(OrganicCoefficient, StandardDamageReduction),
		"DamageRepair":              MakeScaledFuncLevelFunc(OrganicCoefficient, StandardDamageRepair),
		"DestroyedRepairChance":     MakeScaledFuncLevelFunc(OrganicCoefficient, StandardDestroyedRepairChance),
		"StaticEnergyUsed":          MakeScaledFuncLevelFunc(2.0-OrganicCoefficient, StandardRepairEnergyUsed),
	}
)
