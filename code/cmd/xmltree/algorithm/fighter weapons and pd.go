package algorithm

import (
	"fmt"
	"strings"
)

func FighterWeaponsAndPD(folder string) (err error) {

	// load all component definition files
	j, err := LoadJobFor(folder, "ComponentDefinitions*.xml")
	if err != nil {
		return
	}

	// apply this transformation
	err = j.applyFighterWeaponsAndPD()
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

func (j *Job) applyFighterWeaponsAndPD() (err error) {

	// fmt.Println("All strikecraft weapons and PD weapons will be scaled to ship components")

	for _, f := range j.xfiles {

		// the root will result in a single ArrayOf[RootObjectType]
		for _, e := range f.root.Elements.Elements() {

			err = assertIs(e, "ArrayOfComponentDefinition")
			if err != nil {
				return
			}

			for _, e := range e.Elements() {

				// each of these is a componentdefinition
				err = assertIs(e, "ComponentDefinition")
				if err != nil {
					return
				}

				// only weapons...
				if !e.HasPrefix("Category", "Weapon") {
					continue
				}

				// for fighters and PD...
				targetName := e.Child("Name").StringValue()
				if !strings.HasSuffix(targetName, "[F/B]") && !strings.HasSuffix(targetName, "[PD]") {
					continue
				}

				// find the corresponding small weapon by name
				sourceName := GetFighterOrPointDefenseSourceName(targetName, GetComponentIsms(e))
				source, _ := j.FindElement("Name", sourceName)
				if source == nil {
					fmt.Printf("Missing: %s (for %s)", sourceName, targetName)
					continue
				}

				// debug
				if !Quiet {
					fmt.Printf("%s from %s\n", targetName, sourceName)
				}

				// do it
				err = j.DeriveComponentFromComponent(f, source, e)
				if err != nil {
					return
				}
			}
		}
	}

	return
}
