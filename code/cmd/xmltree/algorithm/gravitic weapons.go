package algorithm

func GraviticWeapons(folder string) (err error) {

	// load all component definition files
	j, err := LoadJobFor(folder, "ComponentDefinitions*.xml")
	if err != nil {
		return
	}

	// update gravitic weapons
	err = j.applyGraviticWeapons()
	if err != nil {
		return
	}

	// update derivatives
	err = j.applyFighterWeaponsAndPD()
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

func (j *Job) applyGraviticWeapons() (err error) {

	// apply stats for each component
	err = j.ApplyComponentAll(GraviticWeaponData)

	return
}

const (
	// Gravitic Disruptors have a hefty fall-off rate
	GraviticFalloff = .30

	// damage coefficients
	GraviticCoefficient = 0.75 // reduce all gravitic disruptors by this to account for the fact that they have >= +100% penetration (combined)

	// gravitic weapons are very slow to fire
	//  (similar rof to that of missiles, which means that they have a big punch when they do fire)
	//  so for now, we'll try not giving them any further boost on raw damage ratio (similar to beams)
	GraviticDamageFactor            = 1.0 * GraviticCoefficient
	GraviticFluxDamageFactor        = 1.1 * GraviticCoefficient
	GraviticBeamDamageFactor        = 1.1 * GraviticCoefficient
	ResonantGraviticDamageFactor    = 1.2 * GraviticCoefficient
	DealbreakerGraviticDamageFactor = 1.25 * GraviticCoefficient

	// gravitics are seriously energy-hungry
	// but the fire infrequently, which gives them a heavy per-shot cost
	GraviticEnergyRatio            = 1.2
	GraviticFluxEnergyRatio        = 1.3
	GraviticBeamEnergyRatio        = 1.4
	ResonantGraviticEnergyRatio    = 1.3
	DealbreakerGraviticEnergyRatio = 1.3

	BaseGraviticFireRate        = 2.9 * WeaponFireRateBasis
	GraviticFluxFireRate        = 2.9 * WeaponFireRateBasis
	GraviticBeamFireRate        = 2.8 * WeaponFireRateBasis
	ResonantGraviticFireRate    = 2.7 * WeaponFireRateBasis
	DealbreakerGraviticFireRate = 2.6 * WeaponFireRateBasis

	// subtle: by unbalancing it towards a smaller amount getting through shields
	// subtle: resistance and reactive ratings are very powerful here

	// 21% of all damage is unstoppable
	StandardGraviticShieldBypass = 0.3
	StandardGraviticArmorBypass  = 0.7

	// 24% of all damage is unstoppable
	DealbreakerGraviticShieldBypass = 0.3
	DealbreakerGraviticArmorBypass  = 0.8
)

var (
	GraviticWeaponData = ComponentLevelDataMap{

		"Gravitic Disruptor, Experimental [S]": {
			minLevel:       0,
			maxLevel:       1,
			componentStats: GraviticWeaponSmall,
		},

		"Gravitic Disruptor, Flux Projector [L]": {
			minLevel:       2,
			maxLevel:       3,
			componentStats: DeriveLargeWeaponUsing4xDamage(GraviticFluxWeaponSmall),
		},

		"Gravitic Disruptor [S]": {
			minLevel:       5,
			maxLevel:       6,
			componentStats: GraviticBeamWeaponSmall,
		},
		"Gravitic Disruptor [M]": {
			minLevel:       4,
			maxLevel:       6,
			componentStats: DeriveMediumWeaponUsing2xDamage(GraviticBeamWeaponSmall),
		},
		"Gravitic Disruptor [L]": {
			minLevel:       4,
			maxLevel:       6,
			componentStats: DeriveLargeWeaponUsing4xDamage(GraviticBeamWeaponSmall),
		},

		"Gravitic Disruptor, Resonant [S]": {
			minLevel:       7,
			maxLevel:       10,
			componentStats: ResonantGraviticWeaponSmall,
		},
		"Gravitic Disruptor, Resonant [M]": {
			minLevel:       7,
			maxLevel:       10,
			componentStats: DeriveMediumWeaponUsing2xDamage(ResonantGraviticWeaponSmall),
		},
		"Gravitic Disruptor, Resonant [L]": {
			minLevel:       7,
			maxLevel:       10,
			componentStats: DeriveLargeWeaponUsing4xDamage(ResonantGraviticWeaponSmall),
		},

		"Gravitic Disruptor, Dealbreaker [S]": {
			minLevel:       4,
			maxLevel:       10,
			componentStats: DealbreakerGraviticWeaponSmall,
		},
		"Gravitic Disruptor, Dealbreaker [M]": {
			minLevel:       3,
			maxLevel:       10,
			componentStats: DeriveMediumWeaponUsing2xDamage(DealbreakerGraviticWeaponSmall),
		},
		"Gravitic Disruptor, Dealbreaker [L]": {
			minLevel:       2,
			maxLevel:       10,
			componentStats: DeriveLargeWeaponUsing4xDamage(DealbreakerGraviticWeaponSmall),
		},

		"Massive Gravitic Disruptor Emplacement": {
			minLevel:       3,
			maxLevel:       9,
			componentStats: PlanetaryGraviticComponentStats,
		},
	}

	StandardGraviticTargetingBonus      = MakeFixedLevelFunc(-0.1)
	StandardGraviticFalloffRatio        = MakeFixedLevelFunc(GraviticFalloff)
	StandardGraviticWeaponRange         = MakeExpLevelFunc(800, 1.1) // 10% level over level
	StandardGraviticWeaponFireRate      = MakeFixedLevelFunc(BaseGraviticFireRate)
	StandardGraviticWeaponRawDamage     = MakeDamageFunc(BaseGraviticFireRate, GraviticDamageFactor)
	StandardGraviticWeaponEnergyPerShot = MakeEnergyPerShotFunc(GraviticEnergyRatio, StandardGraviticWeaponRawDamage)

	// Particle Gravitic
	GraviticWeaponSmall = ComposeComponentStats(
		SmallWeaponBaseStats,
		WeaponNoAOE,
		WeaponNoBombard,
		WeaponNoIon,
		ComponentStats{
			"ComponentCountermeasuresBonus": StandardBeamCountermeasuresBonus,
			"ComponentTargetingBonus":       StandardGraviticTargetingBonus,
			"CrewRequirement":               SmallCrewRequirements,
			"StaticEnergyUsed":              SmallWeaponStaticEnergy,
			"WeaponDamageFalloffRatio":      StandardGraviticFalloffRatio,
			"WeaponRange":                   StandardGraviticWeaponRange,
			"WeaponSpeed":                   StandardBeamWeaponSpeed,
			"WeaponFireRate":                StandardGraviticWeaponFireRate,
			"WeaponEnergyPerShot":           StandardGraviticWeaponEnergyPerShot,
			"WeaponRawDamage":               StandardGraviticWeaponRawDamage,
			"WeaponVolleyAmount":            MakeFixedLevelFunc(1),
			"WeaponVolleyFireRate":          MakeFixedLevelFunc(0),
			"WeaponShieldBypass":            MakeFixedLevelFunc(StandardGraviticShieldBypass),
			"WeaponArmorBypass":             MakeFixedLevelFunc(StandardGraviticArmorBypass),
		},
	)

	// Gravitic Disruptor, Flux Projector
	GraviticFluxWeaponEnergyPerShot = MakeEnergyPerShotFunc(GraviticFluxEnergyRatio, GraviticFluxWeaponRawDamage)
	GraviticFluxWeaponFireRate      = MakeFixedLevelFunc(GraviticFluxFireRate)
	GraviticFluxWeaponRange         = StandardGraviticWeaponRange
	GraviticFluxWeaponRawDamage     = MakeDamageFunc(GraviticFluxFireRate, GraviticFluxDamageFactor)

	GraviticFluxWeaponSmall = ComposeComponentStats(
		GraviticWeaponSmall,
		ComponentStats{
			"WeaponEnergyPerShot": GraviticFluxWeaponEnergyPerShot,
			"WeaponFireRate":      GraviticFluxWeaponFireRate,
			"WeaponRange":         GraviticFluxWeaponRange,
			"WeaponRawDamage":     GraviticFluxWeaponRawDamage,
		},
	)

	// Gravitic Beam Projector
	GraviticBeamWeaponEnergyPerShot = MakeEnergyPerShotFunc(GraviticBeamEnergyRatio, GraviticBeamWeaponRawDamage)
	GraviticBeamWeaponFireRate      = MakeFixedLevelFunc(GraviticBeamFireRate)
	GraviticBeamWeaponRange         = StandardGraviticWeaponRange
	GraviticBeamWeaponRawDamage     = MakeDamageFunc(GraviticBeamFireRate, GraviticBeamDamageFactor)

	GraviticBeamWeaponSmall = ComposeComponentStats(
		GraviticWeaponSmall,
		ComponentStats{
			"WeaponEnergyPerShot": GraviticBeamWeaponEnergyPerShot,
			"WeaponFireRate":      GraviticBeamWeaponFireRate,
			"WeaponRange":         GraviticBeamWeaponRange,
			"WeaponRawDamage":     GraviticBeamWeaponRawDamage,
		},
	)

	// Resonant Gravitic
	ResonantGraviticWeaponEnergyPerShot = MakeEnergyPerShotFunc(ResonantGraviticEnergyRatio, ResonantGraviticWeaponRawDamage)
	ResonantGraviticWeaponFireRate      = MakeFixedLevelFunc(ResonantGraviticFireRate)
	ResonantGraviticWeaponRange         = GraviticFluxWeaponRange
	ResonantGraviticWeaponRawDamage     = MakeDamageFunc(ResonantGraviticFireRate, ResonantGraviticDamageFactor)

	ResonantGraviticWeaponSmall = ComposeComponentStats(
		GraviticFluxWeaponSmall,
		ComponentStats{
			"WeaponEnergyPerShot": ResonantGraviticWeaponEnergyPerShot,
			"WeaponFireRate":      ResonantGraviticWeaponFireRate,
			"WeaponRange":         ResonantGraviticWeaponRange,
			"WeaponRawDamage":     ResonantGraviticWeaponRawDamage,
		},
	)

	// Dealbreaker
	DealbreakerGraviticWeaponEnergyPerShot = MakeEnergyPerShotFunc(DealbreakerGraviticEnergyRatio, DealbreakerGraviticWeaponRawDamage)
	DealbreakerGraviticWeaponFireRate      = MakeFixedLevelFunc(DealbreakerGraviticFireRate)
	DealbreakerGraviticWeaponRange         = ResonantGraviticWeaponRange
	DealbreakerGraviticWeaponRawDamage     = MakeDamageFunc(DealbreakerGraviticFireRate, DealbreakerGraviticDamageFactor)

	DealbreakerGraviticWeaponSmall = ComposeComponentStats(
		ResonantGraviticWeaponSmall,
		ComponentStats{
			"WeaponEnergyPerShot": DealbreakerGraviticWeaponEnergyPerShot,
			"WeaponFireRate":      DealbreakerGraviticWeaponFireRate,
			"WeaponRange":         DealbreakerGraviticWeaponRange,
			"WeaponRawDamage":     DealbreakerGraviticWeaponRawDamage,
			"WeaponShieldBypass":  MakeFixedLevelFunc(DealbreakerGraviticShieldBypass),
			"WeaponArmorBypass":   MakeFixedLevelFunc(DealbreakerGraviticArmorBypass),
		},
	)

	// we'll make these derivative of shatterforce gravitics
	PlanetaryGraviticComponentStats = DerivePlanetaryFromLargeWeapon(DeriveLargeWeaponUsing4xDamage(GraviticBeamWeaponSmall))
)
