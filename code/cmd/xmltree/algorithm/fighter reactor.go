package algorithm

import (
	"fmt"
	"strings"
)

func FighterReactors(folder string) (err error) {

	// load all component definition files
	j, err := LoadJobFor(folder, "ComponentDefinitions*.xml")
	if err != nil {
		return
	}

	// apply this transformation
	err = j.applyFighterReactors()
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

func (j *Job) applyFighterReactors() (err error) {

	// fmt.Println("All strikecraft reactors will be scaled to ship components")

	for _, f := range j.xfiles {

		// the root will result in a single ArrayOf[RootObjectType]
		for _, e := range f.root.Elements.Elements() {

			err = assertIs(e, "ArrayOfComponentDefinition")
			if err != nil {
				return
			}

			for _, e := range e.Elements() {

				// each of these is a componentdefinition
				err = assertIs(e, "ComponentDefinition")
				if err != nil {
					return
				}

				// only reactors...
				if !e.HasChildWithValue("Category", "Reactor") {
					continue
				}

				// for fighters...
				targetName := e.Child("Name").StringValue()
				if !strings.HasSuffix(targetName, "[F/B]") {
					continue
				}

				// find the corresponding ship reactors by same name
				sourceName := strings.TrimSpace(targetName[:len(targetName)-len("[F/B]")])
				source, _ := j.FindElement("Name", sourceName)
				if source == nil {
					fmt.Printf("element not found: %s for %s", sourceName, targetName)
					continue
				}

				// do it
				err = j.DeriveComponentFromComponent(f, source, e)
				if err != nil {
					return
				}
			}
		}
	}

	return
}
