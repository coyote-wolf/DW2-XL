package algorithm

import (
	"github.com/lucky-wolf/xml-tree/xmltree"
)

func Shields(folder string) (err error) {

	// load all component definition files
	j, err := LoadJobFor(folder, "ComponentDefinitions*.xml")
	if err != nil {
		return
	}

	// update kinetic weapons
	err = j.applyShields()
	if err != nil {
		return
	}

	// update derivatives
	err = j.applyFighterShields()
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

func (j *Job) applyShields() (err error) {

	// apply stats for each component
	return j.ApplyComponentAll(StarshipShieldData)
}

func ShieldStaticEnergy(shieldStrength LevelFunc) LevelFunc {
	return MakeScaledFuncLevelFunc(ShieldStaticEnergyCoefficient, shieldStrength)
}

func ShieldRechargeEnergy(shieldRecharge LevelFunc, rechargeCoefficient float64) LevelFunc {
	return MakeScaledFuncLevelFunc(rechargeCoefficient, shieldRecharge)
}

// 30%, 25%, 20%, 15%, 10%, 5%, 0...
func ShieldPenetrationChance(level int) float64 {
	return max(0, .3-float64(level)*.05)
}

// 60%, 50%, 40%, 30%, 20%, 10%, 0...
func ShieldPenetrationRatio(level int) float64 {
	return max(0, .6-float64(level)*.1)
}

// warn: counter-intuitively, if we tie recharge to strength, then the cost of recharging fast small ~= large slow
// func GenerateShieldStatsFor(shieldStrength LevelFunc, rechargeRate float64) ComponentStats {
// 	shieldRecharge := MakeMultipliedLevelFunc(shieldStrength, MakeExpLevelFunc(rechargeRate, ShieldStrengthIncreaseExp))
// 	return ComponentStats{
// 		"ShieldStrength":            shieldStrength,
// 		"ShieldRechargeRate":        shieldRecharge,
// 		"ShieldRechargeEnergyUsage": ShieldRechargeEnergy(shieldRecharge),
// 		"StaticEnergyUsed":          ShieldStaticEnergy(shieldStrength),
// 	}
// }

func GenerateShieldStatsFor(shieldStrength LevelFunc, rechargeRate LevelFunc, rechargeCoefficient float64) ComponentStats {
	return ComponentStats{
		"ShieldStrength":            shieldStrength,
		"ShieldRechargeRate":        rechargeRate,
		"ShieldRechargeEnergyUsage": ShieldRechargeEnergy(rechargeRate, rechargeCoefficient),
		"StaticEnergyUsed":          ShieldStaticEnergy(shieldStrength),
	}
}

const (
	ShieldSizeSmall    = 12
	ShieldSizeStandard = 14
	ShieldSizeLarge    = 16
)

var (
	// weak -> standard -> strong are each 50% stronger than the previous category
	WeakShieldStrength       = MakeExpLevelFunc(WeakShieldStrengthBasis, ShieldStrengthIncreaseExp)
	StandardShieldStrength   = MakeExpLevelFunc(WeakShieldStrengthBasis*1.5, ShieldStrengthIncreaseExp)
	ImprovedShieldStrength   = MakeExpLevelFunc(WeakShieldStrengthBasis*1.875, ShieldStrengthIncreaseExp) // 25% stronger than standard
	StrongShieldStrength     = MakeExpLevelFunc(WeakShieldStrengthBasis*2.25, ShieldStrengthIncreaseExp)
	VeryStrongShieldStrength = MakeExpLevelFunc(WeakShieldStrengthBasis*2.8125, ShieldStrengthIncreaseExp) // 25% stronger than strong

	// counter-intuitively, recharge rate is independent of strength
	SlowShieldRecharge      = MakeExpLevelFunc(SlowShieldRechargeBasis, ShieldStrengthIncreaseExp)
	StandardShieldRecharge  = MakeExpLevelFunc(SlowShieldRechargeBasis*2.0, ShieldStrengthIncreaseExp)
	QuickShieldRecharge     = MakeExpLevelFunc(SlowShieldRechargeBasis*4.0, ShieldStrengthIncreaseExp)
	VeryQuickShieldRecharge = MakeExpLevelFunc(SlowShieldRechargeBasis*8.0, ShieldStrengthIncreaseExp)

	// resistance is independent of strength
	// like reactives, we don't want to accelerate too quickly and leave ftr weapons in the dust
	// so we simply scale resistance to the same growth rate as weapon damage
	WeakShieldResistance     = MakeExpLevelFunc(ShieldResistanceBasis, ShieldResistanceIncreaseExp)
	StandardShieldResistance = MakeScaledFuncLevelFunc(1.5, WeakShieldResistance)
	StrongShieldResistance   = MakeScaledFuncLevelFunc(2.25, WeakArmorReactiveRating)

	// Ion has it's own scaling & rules
	WeakShieldIonDefense     = MakeLinearLevelFunc(0, 1)
	StandardShieldIonDefense = MakeLinearLevelFunc(0, 2)
	StrongShieldIonDefense   = MakeLinearLevelFunc(0, 3)
	// WeakShieldIonDefense     = MakeLinearLevelFunc(0, 5)
	// StandardShieldIonDefense = MakeLinearLevelFunc(10, 5)
	// StrongShieldIonDefense   = MakeLinearLevelFunc(20, 5)

	SmallShieldValues = SimpleValuesTable{
		"Size": xmltree.CreateInt(ShieldSizeSmall),
	}
	StandardShieldValues = SimpleValuesTable{
		"Size": xmltree.CreateInt(ShieldSizeStandard),
	}
	LargeShieldValues = SimpleValuesTable{
		"Size": xmltree.CreateInt(ShieldSizeLarge),
	}
	ShieldAmplifierValues = SimpleValuesTable{
		"Size": xmltree.CreateInt(ShieldSizeStandard / 2),
	}

	StarshipShieldData = ComponentLevelDataMap{

		// quantum amps
		"Shield Capacitor, Quantum": {
			values:         ShieldAmplifierValues,
			minLevel:       6,
			maxLevel:       10,
			componentStats: QuantumCapcitorStats,
			derivatives:    []string{"Shield Capacitor, Quantum [F/B]"},
		},

		// hydrocore amps
		"Shield Capacitor, HydroCore": {
			values: SimpleValuesTable{
				"Size": xmltree.CreateInt(ShieldSizeStandard/2 - 1),
			},
			minLevel:       7, // simply +1 level compared to quantum amps
			maxLevel:       11,
			componentStats: HydroCapcitorStats,
			derivatives:    []string{"Shield Capacitor, HydroCore [F/B]"},
		},

		// basic
		"Deflectors": {
			values:         StandardShieldValues,
			minLevel:       0,
			maxLevel:       1,
			componentStats: BasicShieldComponentStats,
		},

		// mid-game
		"Corvidian Shields": {
			values:         StandardShieldValues,
			minLevel:       2,
			maxLevel:       5,
			componentStats: CorvidianShieldComponentStats,
		},
		"Talassos Shields": {
			values:         StandardShieldValues,
			minLevel:       2,
			maxLevel:       5,
			componentStats: TalassosShieldComponentStats,
		},
		"Deucalios Shields": {
			values:         StandardShieldValues,
			minLevel:       2,
			maxLevel:       5,
			componentStats: DeucaliosShieldComponentStats,
		},

		// advanced
		"Meridian Shields": {
			values:         StandardShieldValues,
			minLevel:       6,
			maxLevel:       10,
			componentStats: MeridianShieldComponentStats,
		},

		// super
		"Citadel Shields": {
			values:         LargeShieldValues,
			minLevel:       11,
			maxLevel:       11,
			componentStats: CitadelShieldComponentStats,
		},

		// zenox
		"Megatron Z4 Shields": {
			values:         SmallShieldValues,
			minLevel:       2,
			maxLevel:       5,
			componentStats: ZenoxZ4ShieldComponentStats,
		},
		"Megatron Z9 Shields": {
			values:         SmallShieldValues,
			minLevel:       6,
			maxLevel:       10,
			componentStats: ZenoxZ9ShieldComponentStats,
		},

		// quameno
		"Tortoise Shields": {
			values:         StandardShieldValues,
			minLevel:       2,
			maxLevel:       5,
			componentStats: QuamenoShieldComponentStats,
		},
		"Testudo Shields": {
			values:         StandardShieldValues,
			minLevel:       6,
			maxLevel:       10,
			componentStats: TestudoShieldComponentStats,
		},
	}

	QuantumCapcitorStats = ComposeComponentStats(
		GenerateShieldStatsFor(WeakShieldStrength, SlowShieldRecharge, StandardShieldRechargeEnergyCoefficient),
		ComponentStats{
			"ComponentIonDefense": HardenedComponentIonDefense,
			"CrewRequirement":     SmallCrewRequirements,
			"ShieldResistance":    WeakShieldResistance,
			"IonDamageDefense":    WeakShieldIonDefense,
		},
	)

	HydroCapcitorStats = ComposeComponentStats(
		GenerateShieldStatsFor(WeakShieldStrength, StandardShieldRecharge, EfficientShieldRechargeEnergyCoefficient),
		ComponentStats{
			"ComponentIonDefense": HardenedComponentIonDefense,
			"CrewRequirement":     SmallCrewRequirements,
			"ShieldResistance":    WeakShieldResistance,
			"IonDamageDefense":    WeakShieldIonDefense,
		},
	)

	CoreShieldStats = ComponentStats{
		"ComponentIonDefense":     HardenedComponentIonDefense, // shields are hardened
		"CrewRequirement":         SmallCrewRequirements,
		"ShieldPenetrationChance": ShieldPenetrationChance,
		"ShieldPenetrationRatio":  ShieldPenetrationRatio,
	}

	BasicShieldComponentStats = ComposeComponentStats(
		CoreShieldStats,
		GenerateShieldStatsFor(WeakShieldStrength, SlowShieldRecharge, InefficientShieldRechargeEnergyCoefficient),
		ComponentStats{
			"ShieldResistance": WeakShieldResistance,
			"IonDamageDefense": WeakShieldIonDefense,
		},
	)

	CorvidianShieldComponentStats = ComposeComponentStats(
		CoreShieldStats,
		GenerateShieldStatsFor(StandardShieldStrength, StandardShieldRecharge, StandardShieldRechargeEnergyCoefficient),
		ComponentStats{
			"ShieldResistance": StandardShieldResistance,
			"IonDamageDefense": StandardShieldIonDefense,
		},
	)

	TalassosShieldComponentStats = ComposeComponentStats(
		CoreShieldStats,
		GenerateShieldStatsFor(WeakShieldStrength, QuickShieldRecharge, EfficientShieldRechargeEnergyCoefficient),
		ComponentStats{
			"ShieldResistance": StandardShieldResistance,
			"IonDamageDefense": StandardShieldIonDefense,
		},
	)

	DeucaliosShieldComponentStats = ComposeComponentStats(
		CoreShieldStats,
		GenerateShieldStatsFor(StrongShieldStrength, SlowShieldRecharge, InefficientShieldRechargeEnergyCoefficient),
		ComponentStats{
			"ShieldResistance": StandardShieldResistance,
			"IonDamageDefense": StandardShieldIonDefense,
		},
	)

	// Meridian
	MeridianShieldComponentStats = ComposeComponentStats(
		CoreShieldStats,
		GenerateShieldStatsFor(StrongShieldStrength, QuickShieldRecharge, EfficientShieldRechargeEnergyCoefficient),
		ComponentStats{
			"ShieldResistance": StandardShieldResistance,
			"IonDamageDefense": StrongShieldIonDefense,
		},
	)

	// GenerateShieldStatsFor(StrongShieldStrength, FastShieldRechargeFactor),
	CitadelShieldComponentStats = ComposeComponentStats(
		CoreShieldStats,
		GenerateShieldStatsFor(StrongShieldStrength, QuickShieldRecharge, EfficientShieldRechargeEnergyCoefficient),
		ComponentStats{
			"ShieldResistance": StrongShieldResistance,
			"IonDamageDefense": StrongShieldIonDefense,
		},
	)

	// GenerateShieldStatsFor(StandardShieldStrength, FastShieldRechargeFactor),
	ZenoxZ4ShieldComponentStats = ComposeComponentStats(
		CoreShieldStats,
		GenerateShieldStatsFor(StandardShieldStrength, QuickShieldRecharge, EfficientShieldRechargeEnergyCoefficient),
		ComponentStats{
			"ShieldResistance": StandardShieldResistance,
			"IonDamageDefense": StandardShieldIonDefense,
		},
	)
	ZenoxZ9ShieldComponentStats = ComposeComponentStats(
		ZenoxZ4ShieldComponentStats,
		GenerateShieldStatsFor(ImprovedShieldStrength, VeryQuickShieldRecharge, VeryEfficientShieldRechargeEnergyCoefficient),
		ComponentStats{
			"ShieldResistance": StandardShieldResistance,
			"IonDamageDefense": StandardShieldIonDefense,
		},
	)

	// GenerateShieldStatsFor(StrongShieldStrength, StandardShieldRechargeFactor),
	QuamenoShieldComponentStats = ComposeComponentStats(
		CoreShieldStats,
		GenerateShieldStatsFor(StrongShieldStrength, StandardShieldRecharge, EfficientShieldRechargeEnergyCoefficient),
		ComponentStats{
			"ShieldResistance": StandardShieldResistance,
			"IonDamageDefense": WeakShieldIonDefense,
		},
	)
	TestudoShieldComponentStats = ComposeComponentStats(
		CoreShieldStats,
		GenerateShieldStatsFor(VeryStrongShieldStrength, StandardShieldRecharge, VeryEfficientShieldRechargeEnergyCoefficient),
		ComponentStats{
			"ShieldResistance": StandardShieldResistance,
			"IonDamageDefense": WeakShieldIonDefense,
		},
	)
)
