package algorithm

import (
	"fmt"
	"slices"
)

func ScalePlanetFrequencies(folder string, factor float64, scope string, ids []int) (err error) {

	// load all orbtype files
	j, err := LoadJobFor(folder, "OrbTypes*.xml")
	if err != nil {
		return
	}

	// apply this transformation
	err = j.scalePlanetFrequencies(factor, scope, ids)
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

func (j *Job) scalePlanetFrequencies(factor float64, scope string, ids []int) (err error) {

	if factor == 1 {
		err = fmt.Errorf("because the scaling factor is 1.0, no changes will be made")
		return
	}

	if len(ids) == 0 {
		err = fmt.Errorf("because no orb type IDs were specified, no changes will be made")
		return
	}

	// all or dash means no scope limitation (i.e. all planets and stars)
	if scope == "all" || scope == "-" {
		scope = ""
	} else if scope != "Star" && scope != "Planet" {
		err = fmt.Errorf("scope must be 'Star', 'Planet', or 'all'")
		return
	}

	explainScope := func() string {
		if scope == "" {
			return "planets and stars"
		} else {
			return scope + "s"
		}
	}
	fmt.Printf("Orbs %v around %s will have their frequencies scaled by %v\n", ids, explainScope(), factor)

	for _, f := range j.xfiles {

		statistics := &f.stats

		// the root will result in a single ArrayOf[RootObjectType]
		for _, e := range f.root.Elements.Elements() {

			err = assertIs(e, "ArrayOfOrbType")
			if err != nil {
				return
			}

			for _, e := range e.Elements() {

				// each of these is a OrbType
				err = assertIs(e, "OrbType")
				if err != nil {
					return
				}

				// adjust only parents of the given category
				// "Star" means only children of stars (planets)
				// "Planet" means only children of planets (moons)
				if scope != "" && scope != e.Child("Category").StringValue() {
					continue
				}

				// go through all children and apply our adjustment there
				for _, f := range e.Child("ChildTypes").Elements() {

					err = assertIs(f, "OrbTypeFactor")
					if err != nil {
						return
					}

					// apply the desired factor to matching orb types
					if slices.Contains(ids, f.Child("OrbTypeId").IntValue()) {
						err = f.ScaleChildBy("Factor", factor)
						if err != nil {
							return
						}
						statistics.changed++
					}

					statistics.elements++
				}

				statistics.objects++
			}
		}
	}
	err = nil
	return
}
