package algorithm

import (
	"fmt"

	"github.com/lucky-wolf/xml-tree/xmltree"
)

func PartialOrdering(folder string) (err error) {

	// do all file types we can handle
	j, err := LoadJobFor(folder, "ColonyEventDefinitions*.xml", "ComponentDefinitions*.xml", "GameEvents*.xml", "OrbTypes*.xml", "PlanetaryFacilityDefinitions*.xml", "Races*.xml", "ResearchProjectDefinitions*.xml", "ShipHulls*.xml", "TroopDefinitions*.xml")
	if err != nil {
		return
	}

	// apply this transformation
	err = j.applyPartialOrdering()
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

func (j *Job) applyPartialOrdering() (err error) {

	fmt.Println("All xml objects will have their first elements in a fixed order")

	for _, f := range j.xfiles {

		// the root will result in a single ArrayOf[RootObjectType]
		for _, e := range f.root.Elements.Elements() {

			switch e.Name.Local {
			case "ArrayOfComponentDefinition":
				err = j.applyPartialOrderingTo(f, e,
					"ComponentId",
					"Name",
					"Description",
					"ImageFilename",
					"Size",
					"IsFighterOnly",
					"Category",
					"Family",
					"SoundEffectFilename",
					"SoundEffectFilenameAlternate",
					"SoundEffectVolume",
					"SoundEffectAlternateVolume",
					"ParticleEffectNames",
					"DisplayTextureNames",
					"DisplayColors",
					"DisplayRotationType",
					"DisplayRotationRate",
					"UseLargeMeshIfAvailable",
					"WeaponMeshType",
					"WeaponEffect",
					"Bonuses",
					"ResourcesRequired",
					"Values",
				)
			case "ArrayOfOrbType":
				err = j.applyPartialOrderingTo(f, e, "OrbTypeId", "Category", "Name", "Description", "ImageFilename")
			case "ArrayOfPlanetaryFacilityDefinition":
				err = j.applyPartialOrderingTo(f, e, "PlanetaryFacilityDefinitionId", "Name", "Description", "ImageFilename", "Size", "IsRuins", "BuildCost", "MaintenanceCost", "FacilityFamilyId", "FacilityFamilyLevel", "ExclusiveWithinFamily")
			case "ArrayOfRace":
				err = j.applyPartialOrderingTo(f, e, "RaceId", "Name", "Description", "ImageFilename", "BundleName")
			case "ArrayOfResearchProjectDefinition":
				err = j.applyPartialOrderingTo(f, e, "ResearchProjectId", "Name", "Description", "ImageFilename")
			case "ArrayOfShipHull":
				err = j.applyPartialOrderingTo(f, e,
					"ShipHullId",
					"Name",
					"Description",
					"ImageFilename",
					"RaceId",
					"Role",
					"Level",
					"DesignTemplateId",
					"Size",
					"MaximumSize",
					"DisplaySize",
					"ModelName",
					"CockpitImageFilename",
					"BaseCost",
					"BuildSpeedFactor",
					"ArmorReactiveRating",
					"IonDefense",
					"EngineLimit",
					"Stealth",
					"CountermeasuresBonus",
					"TargetingBonus",
					"TurnRollRate",
					"MaximumTurnRollAngle",
					"Bonuses",
					"ModelSize",
					"ComponentBays",
					"ResourcesRequired",
				)
			case "ArrayOfTroopDefinition":
				err = j.applyPartialOrderingTo(f, e,
					"TroopDefinitionId", "Name", "Description", "ImageFilename",
					"RaceId", "Type", "Size", "RecruitmentCost", "MaintenanceCost",
					"AttackStrength", "DefendStrength", "SabotageStrength",
					"EvasionInfantry", "EvasionArmor", "EvasionTitan", "EvasionPlanetaryDefense", "EvasionSpecialForces",
				)
			case "ArrayOfColonyEventDefinition":
				err = j.applyPartialOrderingTo(f, e, "ColonyEventDefinitionId", "Name", "Type", "Description", "ImageFilename", "IconFilename")
			case "ArrayOfGameEvent":
				err = j.applyPartialOrderingTo(f, e, "Name", "GroupName", "Title", "Description", "ImageFilepath")
			default:
				err = fmt.Errorf("no ordering defined for: %s", e.Name.Local)
			}
			if err != nil {
				return
			}
		}
	}

	err = nil
	return
}

func (j *Job) applyPartialOrderingTo(f *XFile, arrayOf *xmltree.XMLElement, firsts ...string) (err error) {

	elements := arrayOf.Elements()
	f.stats.objects = len(elements)
	for _, object := range elements {

		// for each first, find it, and insert at next top position
		to := object.ZeroElementIndex()
		for _, tag := range firsts {

			// see if this element exists
			from := object.ChildIndex(tag)
			if from == -1 {
				continue
			}

			// yes, then see if we need to reorder it (not already in correct spot)
			f.stats.elements++
			if from != to {
				object.Reorder(from, to)
				f.stats.changed++
			}

			// we'll place the next element one further down
			to++
		}
	}

	return
}
