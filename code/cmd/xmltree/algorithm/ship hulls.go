package algorithm

import (
	"fmt"
	"math"
	"os"

	"github.com/lucky-wolf/xml-tree/etc"
	"github.com/lucky-wolf/xml-tree/xmltree"

	"github.com/gocarina/gocsv"
)

const (
	ShipSizeRelativeToMaximumSize = .333333333333
	GlobalHullCostScalingFactor   = 1.25
	EscortShipCostPerSpace        = 4
	FrigateShipCostPerSpace       = 6
	DestroyerShipCostPerSpace     = 8
	CruiserShipCostPerSpace       = 10
	CapitalShipCostPerSpace       = 12
	StationCostPerSpace           = 15

	WekkarusHullWeakness = -0.2
	ZenoxHullStealth     = 10
)

// Size is hull hp
// MaximumSize - Size = Space Available for Components

func MakeCostPerUsableSpaceFunc(costPerUsableSize float64, size LevelFunc, maxsize LevelFunc) LevelFunc {
	return func(level int) float64 {
		return math.Round(costPerUsableSize * float64(maxsize(level)-size(level)))
	}
}

func MakeScaledScaledFunc(f1, f2 LevelFunc) LevelFunc {
	return func(level int) float64 {
		return f1(level) * f2(level)
	}
}

var (
	// XL ship hull schedules
	EscortMaximumSize    = MakeLinearLevelFunc(375, 75)
	FrigateMaximumSize   = MakeLinearLevelFunc(450, 90)
	DestroyerMaximumSize = MakeLinearLevelFunc(600, 120)
	CruiserMaximumSize   = MakeLinearLevelFunc(800, 160)
	CapitalMaximumSize   = MakeLinearLevelFunc(1200, 240)

	// we're using 1/3 of maximum size for these
	EscortSize    = MakeIntegerLevelFunc(MakeScaledFuncLevelFunc(ShipSizeRelativeToMaximumSize, EscortMaximumSize))
	FrigateSize   = MakeIntegerLevelFunc(MakeScaledFuncLevelFunc(ShipSizeRelativeToMaximumSize, FrigateMaximumSize))
	DestroyerSize = MakeIntegerLevelFunc(MakeScaledFuncLevelFunc(ShipSizeRelativeToMaximumSize, DestroyerMaximumSize))
	CruiserSize   = MakeIntegerLevelFunc(MakeScaledFuncLevelFunc(ShipSizeRelativeToMaximumSize, CruiserMaximumSize))
	CapitalSize   = MakeIntegerLevelFunc(MakeScaledFuncLevelFunc(ShipSizeRelativeToMaximumSize, CapitalMaximumSize))

	// cost is a simple doubling function with horizontal = 25% increase level over level
	EscortBaseCost    = MakeIntegerLevelFunc(MakeScaledScaledFunc(MakeExpLevelFunc(EscortShipCostPerSpace, GlobalHullCostScalingFactor), EscortMaximumSize))
	FrigateBaseCost   = MakeIntegerLevelFunc(MakeScaledScaledFunc(MakeExpLevelFunc(FrigateShipCostPerSpace, GlobalHullCostScalingFactor), FrigateMaximumSize))
	DestroyerBaseCost = MakeIntegerLevelFunc(MakeScaledScaledFunc(MakeExpLevelFunc(DestroyerShipCostPerSpace, GlobalHullCostScalingFactor), DestroyerMaximumSize))
	CruiserBaseCost   = MakeIntegerLevelFunc(MakeScaledScaledFunc(MakeExpLevelFunc(CruiserShipCostPerSpace, GlobalHullCostScalingFactor), CruiserMaximumSize))
	CapitalBaseCost   = MakeIntegerLevelFunc(MakeScaledScaledFunc(MakeExpLevelFunc(CapitalShipCostPerSpace, GlobalHullCostScalingFactor), CapitalMaximumSize))

	// build speed
	// question: do we really want to amplify the slowdown of larger ships?
	EscortBuildSpeed    = MakeLinearLevelFunc(1.00, -.05)
	FrigateBuildSpeed   = MakeLinearLevelFunc(0.95, -.05)
	DestroyerBuildSpeed = MakeLinearLevelFunc(0.90, -.05)
	CruiserBuildSpeed   = MakeLinearLevelFunc(0.85, -.05)
	CapitalBuildSpeed   = MakeLinearLevelFunc(0.80, -.05)

	// DFB
	DFBUsable  = MakeLinearLevelFunc(1000, 400)
	DFBSize    = MakeScaledFuncLevelFunc(2, DFBUsable)
	DFBMaxSize = func(level int) float64 { return DFBUsable(level) + DFBSize(level) }
	DFBCost    = MakeIntegerLevelFunc(MakeScaledScaledFunc(MakeExpLevelFunc(StationCostPerSpace, GlobalHullCostScalingFactor), DFBUsable))

	// Mon
	MonUsable  = MakeLinearLevelFunc(800, 200)
	MonSize    = MonUsable
	MonMaxSize = func(level int) float64 { return MonUsable(level) + MonSize(level) }
	MonCost    = MakeIntegerLevelFunc(MakeScaledScaledFunc(MakeExpLevelFunc(StationCostPerSpace, GlobalHullCostScalingFactor), MonUsable))

	// Space Stations
	StarPortUsable  = MakeLinearLevelFunc(1000, 300)
	StarPortSize    = MakeScaledFuncLevelFunc(4, StarPortUsable)
	StarPortMaxSize = func(level int) float64 { return StarPortUsable(level) + StarPortSize(level) }
	StarPortCost    = MakeIntegerLevelFunc(MakeScaledScaledFunc(MakeExpLevelFunc(StationCostPerSpace, GlobalHullCostScalingFactor), StarPortUsable))

	StarBaseUsable  = MakeLinearLevelFunc(2000, 400)
	StarBaseSize    = MakeScaledFuncLevelFunc(4, StarBaseUsable)
	StarBaseMaxSize = func(level int) float64 { return StarBaseUsable(level) + StarBaseSize(level) }
	StarBaseCost    = MakeOffsetFuncLevelFunc(3, StarPortCost)

	StarFortUsable  = MakeLinearLevelFunc(3000, 600)
	StarFortSize    = MakeScaledFuncLevelFunc(4, StarFortUsable)
	StarFortMaxSize = func(level int) float64 { return StarFortUsable(level) + StarFortSize(level) }
	StarFortCost    = MakeOffsetFuncLevelFunc(5, StarPortCost)

	shipHullSchedule = HullBaySchedule{
		"Escort": {
			Tier: MakeTierIdentityFunc(),
			StringsTable: StringsTable{
				"Name": MakeClassNamesFunc("Escort"),
			},
			ValuesTable: ValuesTable{
				"MaximumSize":          EscortMaximumSize,
				"Size":                 EscortSize,
				"BaseCost":             EscortBaseCost,
				"BuildSpeedFactor":     EscortBuildSpeed,
				"ArmorReactiveRating":  MakeLinearLevelFunc(3, 2),
				"IonDefense":           MakeLinearLevelFunc(2, 2),
				"CountermeasuresBonus": MakeLinearLevelFunc(.20, .05),
				"TargetingBonus":       MakeLinearLevelFunc(0, 0),
			},
			BonusesTable: ComponentStats{
				"ShipManeuvering": MakeLinearLevelFunc(.20, .05),
				"ShipSpeed":       MakeLinearLevelFunc(.20, .05),
			},
			RulesTable: RulesTable{
				ShipDisplaySizeRule,
				StandardizeWeaponSizes,
				StandardizeSensorSizes,
				StandardizeWarshipHangarSizes,
			},
		},
		"Frigate": {
			Tier: MakeTierIdentityFunc(),
			StringsTable: StringsTable{
				"Name": MakeClassNamesFunc("Frigate"),
			},
			ValuesTable: ValuesTable{
				"MaximumSize":          FrigateMaximumSize,
				"Size":                 FrigateSize,
				"BaseCost":             FrigateBaseCost,
				"BuildSpeedFactor":     FrigateBuildSpeed,
				"ArmorReactiveRating":  MakeLinearLevelFunc(4, 2),
				"IonDefense":           MakeLinearLevelFunc(3, 2),
				"CountermeasuresBonus": MakeLinearLevelFunc(.10, .05),
				"TargetingBonus":       MakeLinearLevelFunc(0, 0),
			},
			BonusesTable: ComponentStats{
				"ShipManeuvering": MakeLinearLevelFunc(.15, .05),
				"ShipSpeed":       MakeLinearLevelFunc(.15, .05),
			},
			RulesTable: RulesTable{
				ShipDisplaySizeRule,
				StandardizeWeaponSizes,
				StandardizeSensorSizes,
				StandardizeWarshipHangarSizes,
			},
		},
		"Destroyer": {
			Tier: MakeTierIdentityFunc(),
			StringsTable: StringsTable{
				"Name": MakeClassNamesFunc("Destroyer"),
			},
			ValuesTable: ValuesTable{
				"MaximumSize":          DestroyerMaximumSize,
				"Size":                 DestroyerSize,
				"BaseCost":             DestroyerBaseCost,
				"BuildSpeedFactor":     DestroyerBuildSpeed,
				"ArmorReactiveRating":  MakeLinearLevelFunc(5, 2),
				"IonDefense":           MakeLinearLevelFunc(4, 2),
				"CountermeasuresBonus": MakeLinearLevelFunc(0, .05),
				"TargetingBonus":       MakeLinearLevelFunc(0, .05),
			},
			BonusesTable: ComponentStats{
				"ShipManeuvering": MakeLinearLevelFunc(0, .05),
				"ShipSpeed":       MakeLinearLevelFunc(.10, .05),
			},
			RulesTable: RulesTable{
				ShipDisplaySizeRule,
				StandardizeWeaponSizes,
				StandardizeSensorSizes,
				StandardizeWarshipHangarSizes,
			},
		},
		"Cruiser": {
			Tier: MakeTierLookupFunc(HullTiers{0: 0, 1: 1, 2: 2, 3: 3}),
			StringsTable: StringsTable{
				// "Name": MakeLookupNamesIdentityFunc("Cruiser", "Cruiser, Fast", "Cruiser, Shield", "Cruiser, Fleet", "Cruiser, Heavy", "Cruiser, Galaxy", "Cruiser, Patrol", "Cruiser, Command", "Cruiser, Advanced"),
				"Name": MakeClassNamesFunc("Cruiser"),
			},
			ValuesTable: ValuesTable{
				"MaximumSize":          CruiserMaximumSize,
				"Size":                 CruiserSize,
				"BaseCost":             CruiserBaseCost,
				"BuildSpeedFactor":     CruiserBuildSpeed,
				"ArmorReactiveRating":  MakeLinearLevelFunc(6, 2),
				"IonDefense":           MakeLinearLevelFunc(5, 2),
				"CountermeasuresBonus": MakeLinearLevelFunc(-0.10, .05),
				"TargetingBonus":       MakeLinearLevelFunc(+0.10, .05),
			},
			BonusesTable: ComponentStats{
				"ShieldRechargeRate": MakeLinearLevelFunc(.05, .05),
				"WeaponsRange":       MakeLinearLevelFunc(.1, .1),
				"WeaponsDamage":      MakeLinearLevelFunc(.05, .05),
			},
			RulesTable: RulesTable{
				ShipDisplaySizeRule,
				StandardizeWeaponSizes,
				StandardizeSensorSizes,
				StandardizeWarshipHangarSizes,
			},
		},
		"CapitalShip": {
			Tier: MakeTierIdentityFunc(),
			StringsTable: StringsTable{
				"Name": MakeClassNamesFunc("Battleship"),
			},
			ValuesTable: ValuesTable{
				"MaximumSize":          CapitalMaximumSize,
				"Size":                 CapitalSize,
				"BaseCost":             CapitalBaseCost,
				"BuildSpeedFactor":     CapitalBuildSpeed,
				"ArmorReactiveRating":  MakeLinearLevelFunc(7, 2),
				"IonDefense":           MakeLinearLevelFunc(6, 2),
				"CountermeasuresBonus": MakeLinearLevelFunc(-0.20, .05),
				"TargetingBonus":       MakeLinearLevelFunc(+0.20, .05),
			},
			BonusesTable: ComponentStats{
				"ShieldRechargeRate": MakeLinearLevelFunc(.3, .1),
				"WeaponsRange":       MakeLinearLevelFunc(.3, .1),
				"WeaponsDamage":      MakeLinearLevelFunc(.1, .1),
			},
			RulesTable: RulesTable{
				ShipDisplaySizeRule,
				StandardizeWeaponSizes,
				StandardizeSensorSizes,
				StandardizeWarshipHangarSizes,
			},
		},
		"Carrier": {
			Tier: MakeTierIdentityFunc(),
			StringsTable: StringsTable{
				"Name": MakeClassNamesFunc("Carrier"),
			},
			ValuesTable: ValuesTable{
				"MaximumSize":          CapitalMaximumSize,
				"Size":                 CapitalSize,
				"BaseCost":             CapitalBaseCost,
				"BuildSpeedFactor":     CapitalBuildSpeed,
				"ArmorReactiveRating":  MakeLinearLevelFunc(7, 2),
				"IonDefense":           MakeLinearLevelFunc(6, 2),
				"CountermeasuresBonus": MakeLinearLevelFunc(-0.20, .05),
				"TargetingBonus":       MakeLinearLevelFunc(+0.20, .05),
			},
			BonusesTable: ComponentStats{
				"ShieldRechargeRate": MakeLinearLevelFunc(.3, .1),
				"WeaponsRange":       MakeLinearLevelFunc(.3, .1),
				"WeaponsDamage":      MakeLinearLevelFunc(.1, .1),
			},
			RulesTable: RulesTable{
				ShipDisplaySizeRule,
				StandardizeWeaponSizes,
				StandardizeSensorSizes,
				StandardizeCarrierHangarSizes,
			},
		},
		"TroopTransport": {
			Tier: MakeTierIdentityFunc(),
			StringsTable: StringsTable{
				// note: to use a class identity name we really need to fix level 0 so we start at the right I
				// "Name": MakeClassNamesFunc("Troop Transport"),
				"Name": MakeLookupNamesIdentityFunc("Troop Transport, Small", "Troop Transport, Standard", "Troop Transport, Heavy", "Troop Transport, Huge", "Troop Transport, Massive", "Troop Transport, Colossal"),
			},
			ValuesTable: ValuesTable{
				"Size":                 MakeIntsLevelFunc(150, 200, 240, 350, 425, 500),
				"MaximumSize":          MakeIntsLevelFunc(500, 650, 815, 1100, 1325, 1550), // 350, 450, 575, 750, 900, 1050
				"ArmorReactiveRating":  MakeLinearLevelFunc(5, 1),
				"IonDefense":           MakeLinearLevelFunc(4, 1),
				"CountermeasuresBonus": MakeLinearLevelFunc(0, 0),
				"TargetingBonus":       MakeLinearLevelFunc(0, 0),
			},
			BonusesTable: ComponentStats{
				"BoardingDefense": MakeFixedLevelFunc(.2),
			},
			BayCountsTable: ValuesTable{
				"Defense": MakeLinearLevelFunc(4, 1),
				"General": MakeLinearLevelFunc(16, 2),
			},
			RulesTable: RulesTable{
				ShipDisplaySizeRule,
				StandardizeWeaponSizes,
				StandardizeSensorSizes,
			},
		},
		"ColonyShip": {
			Tier: MakeTierIdentityFunc(),
			StringsTable: StringsTable{
				"Name": MakeLookupNamesIdentityFunc("Colonizer, Early", "Colonizer, Standard", "Colonizer, Large", "Colonizer, Huge", "Colonizer, Massive"),
			},
			ValuesTable: ValuesTable{
				"Size":        MakeIntsLevelFunc(150, 225, 235, 425, 500),
				"MaximumSize": MakeIntsLevelFunc(450, 580, 710, 1275, 1500),
			},
			BayCountsTable: ValuesTable{
				"General": MakeLinearLevelFunc(12, 1),
			},
			RulesTable: RulesTable{
				ShipDisplaySizeRule,
				StandardizeWeaponSizesPDOnly,
				StandardizeSensorSizes,
				StandardizeColonizationSizes,
			},
		},
		"ConstructionShip": {
			Tier: MakeTierIdentityFunc(),
			StringsTable: StringsTable{
				"Name": MakeLookupNamesIdentityFunc("Constructor, Small", "Constructor, Medium", "Constructor, Large"),
			},
			ValuesTable: ValuesTable{
				"Size":        MakeIntsLevelFunc(150, 225, 260),
				"MaximumSize": MakeIntsLevelFunc(475, 650, 760),
			},
			BayCountsTable: ValuesTable{
				"General": MakeLinearLevelFunc(12, 2),
			},
			RulesTable: RulesTable{
				ShipDisplaySizeRule,
				StandardizeWeaponSizesPDOnly,
				StandardizeSensorSizes,
			},
		},
		"ExplorationShip": {
			Tier: MakeTierIdentityFunc(),
			StringsTable: StringsTable{
				"Name": MakeLookupNamesIdentityFunc("Explorer, Small", "Explorer, Medium", "Explorer, Large"),
			},
			ValuesTable: ValuesTable{
				"Size":        MakeIntsLevelFunc(125, 200, 235),
				"MaximumSize": MakeIntsLevelFunc(400, 600, 710),
			},
			RulesTable: RulesTable{
				ShipDisplaySizeRule,
				StandardizeWeaponSizes,
				StandardizeSensorSizes,
			},
		},
		"FreighterSmall": {
			Tier: MakeTierIdentityFunc(),
			StringsTable: StringsTable{
				"Name": MakeLookupNamesIdentityFunc("Short Haul Freighter"),
			},
			ValuesTable: ValuesTable{
				"Size":        MakeIntsLevelFunc(125),
				"MaximumSize": MakeIntsLevelFunc(400),
			},
			BayCountsTable: ValuesTable{
				"General": MakeFixedLevelFunc(12),
			},
			RulesTable: RulesTable{
				ShipDisplaySizeRule,
				StandardizeWeaponSizesPDOnly,
				StandardizeSensorSizes,
			},
		},
		"FreighterMedium": {
			Tier: MakeTierIdentityFunc(),
			StringsTable: StringsTable{
				"Name": MakeLookupNamesIdentityFunc("Medium Haul Freighter"),
			},
			ValuesTable: ValuesTable{
				"Size":        MakeIntsLevelFunc(200),
				"MaximumSize": MakeIntsLevelFunc(600),
			},
			BayCountsTable: ValuesTable{
				"General": MakeFixedLevelFunc(14),
			},
			RulesTable: RulesTable{
				ShipDisplaySizeRule,
				StandardizeWeaponSizesPDOnly,
				StandardizeSensorSizes,
			},
		},
		"FreighterLarge": {
			Tier: MakeTierIdentityFunc(),
			StringsTable: StringsTable{
				"Name": MakeLookupNamesIdentityFunc("Long Haul Freighter"),
			},
			ValuesTable: ValuesTable{
				"Size":        MakeIntsLevelFunc(225),
				"MaximumSize": MakeIntsLevelFunc(675),
			},
			BayCountsTable: ValuesTable{
				"General": MakeFixedLevelFunc(16),
			},
			RulesTable: RulesTable{
				ShipDisplaySizeRule,
				StandardizeWeaponSizesPDOnly,
				StandardizeSensorSizes,
			},
		},
		"FuelTanker": {
			Tier: MakeTierIdentityFunc(),
			StringsTable: StringsTable{
				"Name": MakeLookupNamesIdentityFunc("Refueler, Small", "Refueler, Medium", "Refueler, Large"),
			},
			ValuesTable: ValuesTable{
				"Size":        MakeIntsLevelFunc(150, 200, 225),
				"MaximumSize": MakeIntsLevelFunc(450, 600, 675),
			},
			BayCountsTable: ValuesTable{
				"General": MakeLinearLevelFunc(14, 1),
			},
			RulesTable: RulesTable{
				ShipDisplaySizeRule,
				StandardizeWeaponSizesPDOnly,
				StandardizeSensorSizes,
			},
		},
		"MiningShip": {
			Tier: MakeTierIdentityFunc(),
			StringsTable: StringsTable{
				"Name": MakeLookupNamesIdentityFunc("Miner, Small", "Miner, Medium", "Miner, Large"),
			},
			ValuesTable: ValuesTable{
				"Size":        MakeIntsLevelFunc(125, 150, 200),
				"MaximumSize": MakeIntsLevelFunc(400, 450, 600),
			},
			BayCountsTable: ValuesTable{
				"General": MakeLinearLevelFunc(11, 1),
			},
			RulesTable: RulesTable{
				ShipDisplaySizeRule,
				StandardizeWeaponSizesPDOnly,
				StandardizeSensorSizes,
			},
		},
		"PassengerShip": {
			Tier: MakeTierIdentityFunc(),
			StringsTable: StringsTable{
				"Name": MakeLookupNamesIdentityFunc("Passenger Liner, Small", "Passenger Liner, Medium", "Passenger Liner, Large"),
			},
			ValuesTable: ValuesTable{
				"Size":        MakeIntsLevelFunc(150, 200, 225),
				"MaximumSize": MakeIntsLevelFunc(450, 600, 675),
			},
			BayCountsTable: ValuesTable{
				"General": MakeLinearLevelFunc(12, 1),
			},
			RulesTable: RulesTable{
				ShipDisplaySizeRule,
				StandardizeWeaponSizesPDOnly,
				StandardizeSensorSizes,
			},
		},
		"MiningStation": {
			Tier: MakeTierIdentityFunc(),
			StringsTable: StringsTable{
				"Name": MakeLookupNamesIdentityFunc("Mining Station", "Mining Station, Advanced"),
			},
			ValuesTable: ValuesTable{
				"Size":        MakeIntsLevelFunc(225, 350),
				"MaximumSize": MakeIntsLevelFunc(700, 1050),
			},
			BayCountsTable: ValuesTable{
				"General": MakeLinearLevelFunc(14, 2),
			},
			RulesTable: RulesTable{
				StationDisplaySizeRule,
				MakeLowHighLevelRule(StandardizeWeaponSizesPDOnly, 0, StandardizeWeaponSizes),
				StandardizeSensorSizes,
			},
		},
		"MonitoringStation": {
			Tier: MakeTierIdentityFunc(),
			StringsTable: StringsTable{
				"Name": MakeClassNamesFunc("Monitoring Station"),
			},
			ValuesTable: ValuesTable{
				"MaximumSize": MonMaxSize,
				"Size":        MonSize,
				"BaseCost":    MonCost,
			},
			BonusesTable: ComponentStats{
				"ScannerRange":       MakeLinearLevelFunc(1.0, .5),
				"ArmorStrength":      MakeLinearLevelFunc(.5, .5),
				"ShieldRechargeRate": MakeLinearLevelFunc(.5, .5),
				"WeaponsRange":       MakeLinearLevelFunc(.5, .5),
				"WeaponsDamage":      MakeLinearLevelFunc(.5, .5),
			},
			RulesTable: RulesTable{
				StationDisplaySizeRule,
				StandardizeWeaponSizes,
				StandardizeSensorSizes,
				// StandardizeStationHangarSizes,
			},
		},
		"ResearchStation": {
			Tier: MakeTierIdentityFunc(),
			StringsTable: StringsTable{
				"Name": MakeLookupNamesIdentityFunc("Research Station"),
			},
			ValuesTable: ValuesTable{
				"Size":        MakeIntsLevelFunc(225),
				"MaximumSize": MakeIntsLevelFunc(925),
			},
			BonusesTable: ComponentStats{
				// research stations are a state base, and get additional military bonuses
				"ScannerRange":       MakeLinearLevelFunc(.25, .25),
				"ArmorStrength":      MakeLinearLevelFunc(.25, .25),
				"ShieldRechargeRate": MakeLinearLevelFunc(.25, .25),
				"WeaponsRange":       MakeLinearLevelFunc(.25, .25),
				"WeaponsDamage":      MakeLinearLevelFunc(.25, .25),
			},
			RulesTable: RulesTable{
				StationDisplaySizeRule,
				StandardizeWeaponSizes,
				StandardizeSensorSizes,
				// StandardizeStationHangarSizes,
			},
		},
		"ResortBase": {
			Tier: MakeTierIdentityFunc(),
			StringsTable: StringsTable{
				"Name": MakeLookupNamesIdentityFunc("Resort Station"),
			},
			ValuesTable: ComponentStats{
				"Size":        MakeIntsLevelFunc(325),
				"MaximumSize": MakeIntsLevelFunc(975),
			},
			RulesTable: RulesTable{
				StationDisplaySizeRule,
				StandardizeWeaponSizes,
				StandardizeSensorSizes,
				// StandardizeStationHangarSizes,
			},
		},
		"DefensiveBase": {
			Tier: MakeTierIdentityFunc(),
			StringsTable: StringsTable{
				"Name": MakeClassNamesFunc("Space Bastion"),
			},
			ValuesTable: ValuesTable{
				"DisplaySize":          MakeLinearLevelFunc(1500, 500),
				"MaximumSize":          DFBMaxSize,
				"Size":                 DFBSize,
				"BaseCost":             DFBCost,
				"BuildSpeedFactor":     MakeFixedLevelFunc(1.0), // help speed things up now that we have a lot of hull
				"ArmorReactiveRating":  MakeLinearLevelFunc(6, 2),
				"IonDefense":           MakeLinearLevelFunc(8, 4),
				"CountermeasuresBonus": MakeLinearLevelFunc(-0.20, -.1),
				"TargetingBonus":       MakeLinearLevelFunc(+0.20, .1),
			},
			BayCountsTable: ValuesTable{
				"General": MakeLinearLevelFunc(16, 3),
			},
			BonusesTable: ComponentStats{
				"ArmorStrength":      MakeLinearLevelFunc(.5, .5),
				"ShieldRechargeRate": MakeLinearLevelFunc(.5, .5),
				"WeaponsRange":       MakeLinearLevelFunc(.5, .5),
				"WeaponsDamage":      MakeLinearLevelFunc(.5, .5),
			},
			RulesTable: RulesTable{
				StandardizeWeaponSizes,
				StandardizeSensorSizes,
				StandardizeDefenseBaseHangarSizes,
			},
		},
		"SpaceportSmall": {
			Tier: MakeTierIdentityFunc(),
			StringsTable: StringsTable{
				"Name": MakeClassNamesFunc("Starport"),
			},
			ValuesTable: ValuesTable{
				// "DisplaySize":          MakeFixedLevelFunc(4000),
				"DisplaySize":          MakeLinearLevelFunc(3000, 500),
				"MaximumSize":          StarPortMaxSize,
				"Size":                 StarPortSize,
				"BaseCost":             StarPortCost,
				"BuildSpeedFactor":     MakeFixedLevelFunc(2.0), // help speed things up now that we have a lot of hull
				"ArmorReactiveRating":  MakeLinearLevelFunc(3, 1),
				"IonDefense":           MakeLinearLevelFunc(2, 2),
				"CountermeasuresBonus": MakeLinearLevelFunc(-0.20, -.05),
				"TargetingBonus":       MakeLinearLevelFunc(+0.20, .1),
			},
			BayCountsTable: ValuesTable{
				"General": MakeLinearLevelFunc(16, 2),
			},
			BonusesTable: ComponentStats{
				"ScannerRange":       MakeLinearLevelFunc(.1, .1),
				"ArmorStrength":      MakeLinearLevelFunc(.5, .5),
				"ShieldRechargeRate": MakeLinearLevelFunc(.5, .5),
				"WeaponsRange":       MakeLinearLevelFunc(.5, .5),
				"WeaponsDamage":      MakeLinearLevelFunc(2., .25),
			},
			RulesTable: RulesTable{
				StandardizeWeaponSizes,
				StandardizeSensorSizes,
				StandardizeMilitaryBaseHangarSizes,
			},
		},
		"SpaceportMedium": {
			Tier: MakeTierIdentityFunc(),
			StringsTable: StringsTable{
				"Name": MakeClassNamesFunc("Starbase"),
			},
			ValuesTable: ValuesTable{
				"DisplaySize":          MakeLinearLevelFunc(5000, 500),
				"MaximumSize":          StarBaseMaxSize,
				"Size":                 StarBaseSize,
				"BaseCost":             StarBaseCost,
				"BuildSpeedFactor":     MakeFixedLevelFunc(2.0), // help speed things up now that we have a lot of hull
				"ArmorReactiveRating":  MakeLinearLevelFunc(8, 2),
				"IonDefense":           MakeLinearLevelFunc(10, 4),
				"CountermeasuresBonus": MakeLinearLevelFunc(-0.25, -.05),
				"TargetingBonus":       MakeLinearLevelFunc(+0.25, .05),
			},
			BayCountsTable: ValuesTable{
				"General": MakeLinearLevelFunc(22, 2),
			},
			BonusesTable: ComponentStats{
				"ScannerRange":       MakeLinearLevelFunc(.4, .2),
				"ArmorStrength":      MakeLinearLevelFunc(1.5, .5),
				"ShieldRechargeRate": MakeLinearLevelFunc(1.5, .5),
				"WeaponsRange":       MakeLinearLevelFunc(1.5, .5),
				"WeaponsDamage":      MakeLinearLevelFunc(2.75, .25),
			},
			RulesTable: RulesTable{
				StandardizeWeaponSizes,
				StandardizeSensorSizes,
				StandardizeMilitaryBaseHangarSizes,
			},
		},
		"SpaceportLarge": {
			Tier: MakeTierIdentityFunc(),
			StringsTable: StringsTable{
				"Name": MakeClassNamesFunc("Starfort"),
			},
			ValuesTable: ValuesTable{
				"DisplaySize":          MakeLinearLevelFunc(6000, 1000),
				"MaximumSize":          StarFortMaxSize,
				"Size":                 StarFortSize,
				"BaseCost":             StarFortCost,
				"BuildSpeedFactor":     MakeFixedLevelFunc(2.0), // help speed things up now that we have a lot of hull
				"ArmorReactiveRating":  MakeLinearLevelFunc(12, 2),
				"IonDefense":           MakeLinearLevelFunc(18, 4),
				"CountermeasuresBonus": MakeLinearLevelFunc(-0.35, -.05),
				"TargetingBonus":       MakeLinearLevelFunc(+0.35, .05),
			},
			BayCountsTable: ValuesTable{
				"General": MakeLinearLevelFunc(26, 2),
			},
			BonusesTable: ComponentStats{
				"ScannerRange":       MakeLinearLevelFunc(.8, .2),
				"ArmorStrength":      MakeLinearLevelFunc(2.0, 0),
				"ShieldRechargeRate": MakeLinearLevelFunc(2.0, 0),
				"WeaponsRange":       MakeLinearLevelFunc(2.0, 0),
				"WeaponsDamage":      MakeLinearLevelFunc(3.0, 0),
			},
			RulesTable: RulesTable{
				StandardizeWeaponSizes,
				StandardizeSensorSizes,
				StandardizeMilitaryBaseHangarSizes,
			},
		},
	}

	racialRules = RacialPostprocessTable{
		Wekkarus: WekkarusWeakHulls,
		Zenox:    ZenoxStealthHulls,
	}

	tierSuffixes = []string{" I", " II", " III", " IV", " V", " VI", " VI", " VIII", " IX", " X"}

	StandardizeWeaponSizes = MakeComponentTypeVisitor("Weapon",
		func(e *xmltree.XMLElement, logicalLevel int) (err error) {
			e = e.Child("MaximumComponentSize")
			switch e.IntValue() {
			case 19:
				e.SetValue(20)
			case 39:
				e.SetValue(40)
			case 120:
				e.SetValue(80)
			}
			return
		},
	)
	StandardizeWeaponSizesPDOnly = MakeComponentTypeVisitor("Weapon", MakeSetAttributeRule("MaximumComponentSize", 10))

	StandardizeSensorSizes = MakeComponentTypeVisitor("Sensor", MakeSetAttributeRule("MaximumComponentSize", 50))

	StandardizeDefenseBaseHangarSizes  = MakeComponentTypeVisitor("Hangar", MakeSetAttributeRule("MaximumComponentSize", 50))
	StandardizeMilitaryBaseHangarSizes = MakeComponentTypeVisitor("Hangar", MakeSetAttributeRule("MaximumComponentSize", 100))
	StandardizeCarrierHangarSizes      = MakeComponentTypeVisitor("Hangar", MakeLowHighLevelRule(MakeSetAttributeRule("MaximumComponentSize", 50), 2, MakeSetAttributeRule("MaximumComponentSize", 100)))

	// we cannot extract this one because it needs to know the raceId
	// StandardizeWarshipHangarSizes = MakeComponentTypeVisitor("Hangar", MakeSetAttributeRule("MaximumComponentSize", 100)
)

func WekkarusWeakHulls(e *xmltree.XMLElement, logicalLevel int) (err error) {
	// reduce the health of all hulls by 20% while preserving the same component size
	adj := int(math.Round(e.Child("Size").FloatValue() * WekkarusHullWeakness))
	e.SetChildValue("Size", e.Child("Size").IntValue()+adj)
	e.SetChildValue("MaximumSize", e.Child("MaximumSize").IntValue()+adj)
	return
}

func ZenoxStealthHulls(e *xmltree.XMLElement, logicalLevel int) (err error) {
	// give all hulls a stealth bonus
	c := e.Child("Stealth")
	if c != nil {
		c.SetValue(ZenoxHullStealth)
	} else {
		// note: we sort it anyway, so this is just approximate
		// i := e.ChildIndex("TargetingBonus")
		e.Append(xmltree.MakeElementWithValue("Stealth", ZenoxHullStealth))
	}
	return
}

func StandardizeColonizationSizes(e *xmltree.XMLElement, logicalLevel int) (err error) {
	for _, bay := range e.Child("ComponentBays").Elements() {
		if bay.HasChildWithValue("Type", "General") {
			bay.SetChildValue("MaximumComponentSize", 75)
			logicalLevel--
			if logicalLevel <= 0 {
				return
			}
		}
	}
	return
}

func ShipDisplaySizeRule(e *xmltree.XMLElement, logicalLevel int) (err error) {
	e.Child("DisplaySize").SetValue(e.Child("MaximumSize").IntValue() / 2)
	return
}

func StationDisplaySizeRule(e *xmltree.XMLElement, logicalLevel int) (err error) {
	e.Child("DisplaySize").SetValue(e.Child("MaximumSize").IntValue())
	return
}

func MakeClassNamesFunc(class string) StringLevelFunc {
	return func(level int) string {
		return class + tierSuffixes[level]
	}
}

type StringLookupTable map[int]string

func MakeLookupNamesFunc(substitutions StringLookupTable) StringLevelFunc {
	return func(level int) string {
		return substitutions[level]
	}
}

func MakeLookupNamesIdentityFunc(substitutions ...string) StringLevelFunc {
	return MakeLookupNamesFunc(MakeIdentityLookupTable(substitutions...))
}

func MakeIdentityLookupTable(substitutions ...string) StringLookupTable {
	table := StringLookupTable{}
	for i, s := range substitutions {
		table[i] = s
	}
	return table
}

func MakeSetAttributeRule(attr AttributeName, value any) VisitorFunc {
	return func(e *xmltree.XMLElement, logicalLevel int) (err error) {
		e.SetChildValue(attr, value)
		return
	}
}

func MakeLowLevelRule(cutoffLevel int, vf VisitorFunc) VisitorFunc {
	return func(e *xmltree.XMLElement, logicalLevel int) (err error) {
		if logicalLevel <= cutoffLevel {
			err = vf(e, logicalLevel)
		}
		return
	}
}

func MakeLowHighLevelRule(lowFn VisitorFunc, lowLevel int, highFn VisitorFunc) VisitorFunc {
	return func(e *xmltree.XMLElement, logicalLevel int) (err error) {
		if logicalLevel <= lowLevel {
			err = lowFn(e, logicalLevel)
		} else {
			err = highFn(e, logicalLevel)
		}
		return
	}
}

func UndefineShipHull(shipHull *xmltree.XMLElement) (err error) {

	// debug
	fmt.Printf("Undefining ship hull %s level %s\n", shipHull.Child("Name").StringValue(), shipHull.Child("Level").StringValue())

	keep := []string{"ShipHullId", "Name", "RaceId", "Role", "ModelName"}

	for _, child := range shipHull.Elements() {
		if !etc.IsOneOf(child.Name.Local, keep...) {
			err = shipHull.RemoveByTag(child.Name.Local)
			if err != nil {
				return
			}
		}
	}

	shipHull.Child("Name").SetValue("Undefined")
	shipHull.Child("Role").SetValue("Undefined")

	return
}

func MakeComponentTypeVisitor(component ComponentType, visitor VisitorFunc) VisitorFunc {
	return func(e *xmltree.XMLElement, logicalLevel int) (err error) {
		for _, bay := range e.Child("ComponentBays").Elements() {
			if bay.HasChildWithValue("Type", component) {
				err = visitor(bay, logicalLevel)
				if err != nil {
					return
				}
			}
		}
		return
	}
}

func ShipHulls(folder string) (err error) {

	// load all ship hull definition files
	j, err := LoadJobFor(folder, "ShipHulls*.xml")
	if err != nil {
		return
	}

	// apply this transformation
	err = j.updateShipHulls()
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

func UpdateAndExtractShipHulls(folder string) (err error) {

	// load all ship hull definition files
	j, err := LoadJobFor(folder, "ShipHulls*.xml")
	if err != nil {
		return
	}

	// update the hulls
	err = j.updateShipHulls()
	if err != nil {
		return
	}

	// extract new hull data
	err = j.extractShipComponentBays()
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

func (j *Job) updateShipHulls() (err error) {
	return j.applyShipComponentBays(shipHullSchedule)
}

// warn: this is a linear list of data in unpredictable order
func loadHullSummaries(filename string) (accumulatedSummaries []*ShipHullSummary, err error) {

	var file *os.File
	file, err = os.Open(filename)
	if err != nil {
		return
	}
	defer file.Close()
	accumulatedSummaries = []*ShipHullSummary{}
	err = gocsv.UnmarshalFile(file, &accumulatedSummaries)
	if err != nil {
		return
	}
	return
}

// warn: this is a linear search (an O(N) algorithm)
func hullSummaryFor(accumulatedSummaries []*ShipHullSummary, raceId RaceID, roleName string, hullLevel int) *ShipHullSummary {
	for _, s := range accumulatedSummaries {
		if s.RaceId == raceId && s.Role == roleName && s.Level == hullLevel {
			return s
		}
	}
	return nil
}

type ShipHullsByRole map[string]ShipHullsByLevel
type ShipHullsByLevel map[int]*xmltree.XMLElement

func (j *Job) applyShipComponentBays(schedule HullBaySchedule) (err error) {

	fmt.Println("Some ship hulls will be adjusted to match desired schedule")

	// read our desired component bay schedules
	summaries, err := loadHullSummaries("ShipHulls.csv")
	if err != nil {
		return
	}

	for _, f := range j.xfiles {

		// store the shiphulls we care about by role & level
		refs := ShipHullsByRole{}

		for _, shiphulls := range f.root.Elements.Elements() {

			// the root will result in a single ArrayOf[RootObjectType]
			err = assertIs(shiphulls, "ArrayOfShipHull")
			if err != nil {
				return
			}

			// first pass: build data
			for _, shipHull := range shiphulls.Elements() {

				// each of these is a ShipHull
				err = assertIs(shipHull, "ShipHull")
				if err != nil {
					return
				}

				// see whether we have a schedule for this role
				roleName := shipHull.Child("Role").StringValue()
				_, ok := schedule[roleName]
				if !ok {
					continue
				}

				hullLevel := shipHull.Child("Level").IntValue()

				// at this point, just make a record of it
				if refs[roleName] == nil {
					refs[roleName] = ShipHullsByLevel{}
				}

				// subtle: we clone the entire ship hull entity (deep copy) so that we have something pristine to refer to later
				// clone := shipHull.Clone()

				// verify that the damn clone really works

				refs[roleName][hullLevel] = shipHull

			}
		}

		deferred := []func() error{}

		// second pass: normalize data & apply schedule
		for roleName, defs := range refs {
			for hullLevel, shipHull := range defs {

				// only mess with the hulls we have a schedule for
				hullDefn, ok := schedule[roleName]
				if !ok {
					continue
				}

				f.stats.objects++

				// get our logical level (may be the same)
				logicalLevel, ok := hullDefn.Tier(hullLevel)
				if !ok {
					// if this isn't a valid tier, we undefine it
					shipHull := shipHull
					deferred = append(deferred, func() error { return UndefineShipHull(shipHull) })
					continue
				}

				// apply any string values
				for key, value := range hullDefn.StringsTable {
					_, err = shipHull.MakeChildWithValue(key, value(logicalLevel))
					if err != nil {
						return
					}
					f.stats.elements++
					f.stats.changed++
				}

				// apply any numeric values
				for key, value := range hullDefn.ValuesTable {
					_, err = shipHull.MakeChildWithValue(key, value(logicalLevel))
					if err != nil {
						return
					}
					f.stats.elements++
					f.stats.changed++
				}

				// apply bonuses table
				err = applyHullBonusesTable(shipHull, hullDefn.BonusesTable, logicalLevel)
				if err != nil {
					return
				}

				// check if data exists for bay counts
				_, ok = hullDefn.AprioriBayCounts[hullLevel]
				if ok {
					err = fmt.Errorf("we do not currently handle apriori bay counts for ship hulls")
					return
				}

				// check if the hull summaries contains the bay counts
				hs := hullSummaryFor(summaries, RaceID(shipHull.Child("RaceId").IntValue()), roleName, hullLevel)
				if hs != nil {
					// note: BayCountsTable is for overriding this data, cannot be used without it
					err = adjustComponentBays(shipHull, hs, hullDefn, logicalLevel, refs[roleName], &deferred)
					if err != nil {
						return
					}
				}

				// do any final rules processing
				for i := range hullDefn.RulesTable {
					err = hullDefn.RulesTable[i](shipHull, logicalLevel)
					if err != nil {
						return
					}
				}

				// do any race-specific rules processing
				race := RaceID(shipHull.Child("RaceId").IntValue())
				v := racialRules[race]
				if v != nil {
					err = v(shipHull, logicalLevel)
					if err != nil {
						return
					}
				}
			}
		}

		// do deferred deletions
		for _, d := range deferred {
			err = d()
			if err != nil {
				return
			}
		}

		// ensure our ids are coherent AFTER deleting anything!
		for _, shiphulls := range f.root.Elements.Elements() {
			for _, shipHull := range shiphulls.Elements() {
				err = renumberHullComponentBays(shipHull)
				if err != nil {
					return
				}
			}
		}
	}

	return
}

func applyHullBonusesTable(shipHull *xmltree.XMLElement, bonusesTable ValuesTable, logicalLevel int) (err error) {

	if len(bonusesTable) == 0 {
		return
	}

	// ensure we have a blank bonuses array
	bonuses := shipHull.Child("Bonuses")
	if bonuses == nil {
		// create the bonuses array
		bonuses = xmltree.MakeElementWithValue("Bonuses", []any{})
		err = shipHull.InsertAt(shipHull.ChildIndex("ComponentBays"), bonuses)
		if err != nil {
			return
		}
	} else {
		// kill existing bonus data
		bonuses.SetContents([]any{})
	}

	// build only the bonuses defined in our schedule
	// sorting the keys gives us reliable output ordering (otherwise it sort of randomizes it each time)
	for _, key := range etc.SortedKeys(bonusesTable) {

		// determine the bonus amount
		amount := bonusesTable[key](logicalLevel)
		if amount == 0 {
			// skip zero bonuses (we've already removed the entries from the hull definition, so this works)
			continue
		}

		// non-zero: create the bonus entry
		bonus := xmltree.MakeElementWithValue("Bonus", []any{})
		err = bonus.Append(
			xmltree.MakeElementWithValue("Type", key),
			xmltree.MakeElementWithValue("Amount", amount),
			xmltree.MakeElementWithValue("AppliesTo", "Item"),
		)
		if err != nil {
			return
		}

		// append it to the bonuses array
		err = bonuses.Append(bonus)
		if err != nil {
			return
		}
	}

	return
}

func getComponentBayDonorsFor(hulls ShipHullsByLevel, count int, componentBayType ComponentType) (donors []*xmltree.XMLElement, err error) {
	// see if we have a donor we can crib from for the desired amount
	for _, shipHull := range hulls {
		var indexes BayTypeGroups
		indexes, err = getComponentBayIndexesFromHull(shipHull)
		if err != nil {
			return
		}
		if indexes[componentBayType].count >= count {
			// return the sub-slice that covers this full bay range
			start := indexes[componentBayType].start
			count := indexes[componentBayType].count
			donors = shipHull.Child("ComponentBays").Elements()[start : start+count]
			return
		}
	}
	return
}

func renumberHullComponentBays(shipHull *xmltree.XMLElement) (err error) {

	// renumber the component bays - skip if there are none (this is a valid state)
	bays := shipHull.Child("ComponentBays")
	if bays == nil {
		return
	}

	// id is trivial - just linear numbering within this list
	for i, c := range bays.Elements() {
		e := c.Child("ComponentBayId")
		if e.IntValue() != i {
			e.SetValue(i)
		}
	}

	return
}

var (
	StrikecraftRoles = map[RoleName]any{"FighterBomber": nil, "FighterInterceptor": nil}
	MilitaryRoles    = map[RoleName]any{"Escort": nil, "Frigate": nil, "Destroyer": nil, "Cruiser": nil, "Carrier": nil, "CapitalShip": nil, "TroopTransport": nil, "DefensiveBase": nil, "MonitoringStation": nil}
	StationRoles     = map[RoleName]any{"DefensiveBase": nil, "MiningStation": nil, "MonitoringStation": nil, "ResearchStation": nil, "ResortBase": nil, "SpaceportSmall": nil, "SpaceportMedium": nil, "SpaceportLarge": nil}
)

func RoleIsMilitary(role RoleName) bool {
	_, ok := MilitaryRoles[role]
	return ok
}

func RoleIsStation(role RoleName) bool {
	_, ok := StationRoles[role]
	return ok
}

// depends on race - so cannot have the visitor function extracted (needs to be called for the hull element)
func StandardizeWarshipHangarSizes(shipHull *xmltree.XMLElement, logicalLevel int) (err error) {

	role := shipHull.Child("Role").StringValue()
	if RoleIsStation(role) || !RoleIsMilitary(role) {
		err = fmt.Errorf("we only handle warship hangar sizes")
		return
	}

	raceId := RaceID(shipHull.Child("RaceId").IntValue())

	shipVisitor := func(bay *xmltree.XMLElement) (err error) {

		node := bay.Child("MaximumComponentSize")

		switch {
		case raceId == Teekan:
			// all Teekan warships get large hangars
			node.SetValue(50)
		default:
			// all other warships have aux hangars only
			if logicalLevel < 3 {
				node.SetValue(25)
			} else {
				node.SetValue(50)
			}
		}

		return
	}

	for _, bay := range shipHull.Child("ComponentBays").Elements() {
		if bay.HasChildWithValue("Type", "Hangar") {
			err = shipVisitor(bay)
			if err != nil {
				return
			}
		}
	}

	return
}

func adjustComponentBays(shipHull *xmltree.XMLElement, hs *ShipHullSummary, hullDefn HullRoleDefinition, logicalLevel int, ref ShipHullsByLevel, deferred *[]func() error) (err error) {

	// convert this to bay counts
	desiredCounts := BayCounts{
		"Weapon":  hs.Weapon,
		"Hangar":  hs.Hangar,
		"Engine":  hs.Engine,
		"Sensor":  hs.Sensor,
		"Defense": hs.Defense,
		"General": hs.General,
	}

	// override with BayCountsTable (if present)
	for k, v := range hullDefn.BayCountsTable {
		desiredCounts[k] = int(v(logicalLevel))
	}

	// always make engine bays odd to allow for the game to balance them
	if desiredCounts["Engine"] != 0 && desiredCounts["Engine"]%2 == 0 {
		desiredCounts["Engine"]++
	}

	// scan the component bays by type to figure out where they each begin (and their counts)
	var indexes BayTypeGroups
	indexes, err = getComponentBayIndexesFromHull(shipHull)
	if err != nil {
		return
	}

	// note: defer deletions until after all additions so we don't delete the very data we need until we don't need it
	// subtle: if we had a deep clone of the data in refs, we wouldn't need to defer this
	type Deletion struct {
		bays  *xmltree.XMLElement
		start int
		count int
	}

	// do our inserts and deletions in reverse order so we don't invalidate the indexes of things before us
	for _, componentBayType := range reverseComponentBayOrder {

		desired, ok := desiredCounts[componentBayType]
		if !ok {
			continue
		}

		start := indexes[componentBayType].start
		count := indexes[componentBayType].count

		adjustment := desired - count
		bays := shipHull.Child("ComponentBays")

		switch {
		case adjustment < 0:
			// defer delete unneeded elements
			// subtle: we defer this so we don't remove data from ourself until we don't need it anymore
			*deferred = append(*deferred, func() error { return bays.RemoveSpan(start+desired, -adjustment) })

		case adjustment > 0:
			// append elements from another level of this hull that has enough elements to cover the desired amount
			// if possible - if not, make them up (we can't make them up for Engines nor Weapons which require model tags)

			// see if we have a donor we can crib from for the desired amount
			var donors []*xmltree.XMLElement
			donors, err = getComponentBayDonorsFor(ref, desired, componentBayType)
			if err != nil {
				return
			}

			// use a donor if we have one
			if donors != nil {

				// subtle: because they don't just extend the list to include later items, but do so out of order...
				// subtle: we'll simply copy the whole section from our donor so that this subset is coherent (if not perfect)

				// delete old elements
				err = shipHull.Child("ComponentBays").RemoveSpan(start, count)
				if err != nil {
					return
				}

				// make clones from the source at the same offsets
				for index := 0; index < desired; index++ {
					e := donors[index]
					err = bays.InsertAt(start+index, e.Clone())
					if err != nil {
						return
					}
				}

			} else if componentBayType == "Engine" || componentBayType == "Weapon" {
				return fmt.Errorf("we must have a source component to copy from for a %s", componentBayType)
			} else {

				// create empty bay
				var e *xmltree.XMLElement
				e, err = createEmptyBay(componentBayType)
				if err != nil {
					return
				}

				// make clones from the source at the same offsets
				for index := 0; index < adjustment; index++ {
					err = bays.InsertAt(start+count+index, e.Clone())
					if err != nil {
						return
					}
				}

			}
		}
	}

	// update engine limit

	if desiredCounts["Engine"] == hs.Engine {
		hs.engineLimit = -1
	} else {
		hs.engineLimit = hs.Engine
	}

	// our limit is the incoming desired number of engines
	shipHull.MakeChildWithValue("EngineLimit", hs.engineLimit)

	return
}

func createEmptyBay(componentBayType ComponentType) (bay *xmltree.XMLElement, err error) {

	skeleton := map[string]any{
		"ComponentBayId":       0,
		"Type":                 componentBayType,
		"MaximumComponentSize": 50,
	}

	switch componentBayType {
	case "Defense", "General", "Hangar", "Sensor":
		bay = xmltree.MakeElementWithValue("ComponentBay", []any{})
	default:
		err = fmt.Errorf("we must have at least one component to clone for a %s", componentBayType)
		return
	}

	// build only the bonuses defined in our schedule
	// sorting the keys gives us reliable output ordering (otherwise it sort of randomizes it each time)
	for key, value := range skeleton {
		err = bay.Append(xmltree.MakeElementWithValue(key, value))
		if err != nil {
			return
		}
	}

	return
}
