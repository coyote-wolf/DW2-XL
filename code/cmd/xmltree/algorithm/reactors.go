package algorithm

import "github.com/lucky-wolf/xml-tree/xmltree"

func Reactors(folder string) (err error) {

	// load all component definition files
	j, err := LoadJobFor(folder, "ComponentDefinitions*.xml")
	if err != nil {
		return
	}

	// update kinetic weapons
	err = j.applyReactors()
	if err != nil {
		return
	}

	// update derivatives
	err = j.applyFighterReactors()
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

func (j *Job) applyReactors() (err error) {

	// apply stats for each component
	err = j.ApplyComponentAll(StarshipReactorData)

	return
}

func MakeFuelUnitsLevelFunc(efficiency, capacity LevelFunc) LevelFunc {
	return func(level int) float64 { return efficiency(level) / 1000.0 * capacity(level) }
}

func MakeReactorOutputFunc(basis, growth float64) LevelFunc {
	return MakeLinearLevelFunc(basis*growth, growth)
}

const (
	// capacitors are what allow spike-withdrawls of energy (e.g. firing an energy torpedo)
	LowReactorCapacityRatio  = 2.0
	MedReactorCapacityRatio  = 2.5
	HighReactorCapacityRatio = 3.0

	// reactor sizes
	ReactorSizeUltraTiny    = 10 // ultrafusion
	ReactorSizeTiny         = 11 // hyperfusion
	ReactorSizeSuperCompact = 12 // fusion
	ReactorSizeCompact      = 15 // novacore
	ReactorSizeStandard     = 18 // harmonic, dark star
	ReactorSizeLarge        = 20 // caslon, cycling plasmatic
	ReactorSizeXLarge       = 24 // plasmatic
)

var (
	StarshipReactorData = ComponentLevelDataMap{
		"Energy Core, Fission": {
			values: SimpleValuesTable{
				"Size": xmltree.CreateInt(ReactorSizeLarge),
			},
			minLevel:       0,
			maxLevel:       1,
			componentStats: CaslonReactorComponentStats,
		},
		// "Energy Core, Fusion Prototype": {
		// 	values: SimpleValuesTable{
		// 		"Size": xmltree.CreateInt(ReactorSizeCompact),
		// 	},
		// 	minLevel:       1,
		// 	maxLevel:       1,
		// 	componentStats: FusionReactorComponentStats,
		// },
		"Energy Core, Novacore": {
			values: SimpleValuesTable{
				"Size": xmltree.CreateInt(ReactorSizeCompact),
			},
			minLevel:       2,
			maxLevel:       10,
			componentStats: NovaReactorComponentStats,
		},
		"Energy Core, Fusion": {
			values: SimpleValuesTable{
				"Size": xmltree.CreateInt(ReactorSizeSuperCompact),
			},
			minLevel:       2,
			maxLevel:       4,
			componentStats: FusionReactorComponentStats,
		},
		"Energy Core, Hyperfusion": {
			values: SimpleValuesTable{
				"Size": xmltree.CreateInt(ReactorSizeTiny),
			},
			minLevel:       5,
			maxLevel:       7,
			componentStats: HyperFusionReactorComponentStats,
		},
		"Energy Core, Ultrafusion": {
			values: SimpleValuesTable{
				"Size": xmltree.CreateInt(ReactorSizeUltraTiny),
			},
			minLevel:       8,
			maxLevel:       10,
			componentStats: UltraFusionReactorComponentStats,
		},
		"Energy Core, Harmonic": {
			values: SimpleValuesTable{
				"Size": xmltree.CreateInt(ReactorSizeStandard),
			},
			minLevel:       2,
			maxLevel:       4,
			componentStats: HarmonicReactorComponentStats,
		},
		"Energy Core, Resonant": {
			values: SimpleValuesTable{
				"Size": xmltree.CreateInt(ReactorSizeCompact),
			},
			minLevel:       5,
			maxLevel:       7,
			componentStats: HyperHarmonicReactorComponentStats,
		},
		"Energy Core, Zero Point": {
			values: SimpleValuesTable{
				"Size": xmltree.CreateInt(ReactorSizeCompact),
			},
			minLevel:       8,
			maxLevel:       10,
			componentStats: ZeroPointReactorComponentStats,
		},
		"Energy Core, Plasmatic": {
			values: SimpleValuesTable{
				"Size": xmltree.CreateInt(ReactorSizeXLarge),
			},
			minLevel:       2,
			maxLevel:       4,
			componentStats: PlasmaticReactorComponentStats,
		},
		"Energy Core, Plasmatic Cycling": {
			values: SimpleValuesTable{
				"Size": xmltree.CreateInt(ReactorSizeLarge),
			},
			minLevel:       5,
			maxLevel:       7,
			componentStats: HyperPlasmaticReactorComponentStats,
		},
		"Energy Core, Dark Star": {
			values: SimpleValuesTable{
				"Size": xmltree.CreateInt(ReactorSizeStandard),
			},
			minLevel:       11,
			maxLevel:       11,
			componentStats: ZeroPointReactorComponentStats,
		},
	}

	// reactor efficiency
	ReactorEfficiencyGood = MakeScaledFuncLevelFunc(0.8, ReactorEfficiencyFair)
	ReactorEfficiencyFair = MakeExpLevelFunc(1.25, 0.9)
	ReactorEfficiencyPoor = MakeScaledFuncLevelFunc(1.25, ReactorEfficiencyFair)

	CaslonReactorOutput    = MakeExpLevelFunc(100, HyperDrivePowerExp)
	FusionReactorOutput    = MakeScaledFuncLevelFunc(0.8, CaslonReactorOutput)
	HarmonicReactorOutput  = MakeScaledFuncLevelFunc(1.5, FusionReactorOutput)
	NovaReactorOutput      = HarmonicReactorOutput // same as harmonic, but better efficiency
	PlasmaticReactorOutput = MakeScaledFuncLevelFunc(2.0, FusionReactorOutput)
	ZeroPointReactorOutput = MakeScaledFuncLevelFunc(1.1, PlasmaticReactorOutput)

	HyperReactorDiscontinuityFactor = 1.1
	HyperFusionReactorOutput        = MakeScaledFuncLevelFunc(HyperReactorDiscontinuityFactor, FusionReactorOutput)
	HyperHarmonicReactorOutput      = MakeScaledFuncLevelFunc(HyperReactorDiscontinuityFactor, HarmonicReactorOutput)
	HyperPlasmaticReactorOutput     = MakeScaledFuncLevelFunc(HyperReactorDiscontinuityFactor, PlasmaticReactorOutput)

	UltraReactorDiscontinuityFactor = 1.2
	UltraFusionReactorOutput        = MakeScaledFuncLevelFunc(UltraReactorDiscontinuityFactor, FusionReactorOutput)

	// basic
	CaslonReactorCapacity       = MakeScaledFuncLevelFunc(MedReactorCapacityRatio, CaslonReactorOutput)
	CaslonEfficiency            = ReactorEfficiencyPoor
	CaslonFuelUnits             = MakeFuelUnitsLevelFunc(CaslonEfficiency, CaslonReactorCapacity)
	CaslonReactorComponentStats = ComponentStats{
		"ComponentIonDefense":           HardenedComponentIonDefense, // reactors are a hardened component
		"CrewRequirement":               MakeFixedLevelFunc(5),
		"ReactorEnergyOutputPerSecond":  CaslonReactorOutput,
		"ReactorEnergyStorageCapacity":  CaslonReactorCapacity,
		"ReactorFuelUnitsForFullCharge": CaslonFuelUnits,
		"StaticEnergyUsed":              MakeFixedLevelFunc(0),
	}

	// novacore (Quameno)
	NovaReactorCapacity       = MakeScaledFuncLevelFunc(MedReactorCapacityRatio, NovaReactorOutput)
	NovaEfficiency            = ReactorEfficiencyGood
	NovaFuelUnits             = MakeFuelUnitsLevelFunc(NovaEfficiency, NovaReactorCapacity)
	NovaReactorComponentStats = ComposeComponentStats(
		CaslonReactorComponentStats,
		ComponentStats{
			"ReactorEnergyOutputPerSecond":  NovaReactorOutput,
			"ReactorEnergyStorageCapacity":  NovaReactorCapacity,
			"ReactorFuelUnitsForFullCharge": NovaFuelUnits,
		},
	)

	// fusion
	FusionReactorCapacity       = MakeScaledFuncLevelFunc(HighReactorCapacityRatio, FusionReactorOutput)
	FusionEfficiency            = ReactorEfficiencyGood
	FusionFuelUnits             = MakeFuelUnitsLevelFunc(FusionEfficiency, FusionReactorCapacity)
	FusionReactorComponentStats = ComposeComponentStats(
		CaslonReactorComponentStats,
		ComponentStats{
			"ReactorEnergyOutputPerSecond":  FusionReactorOutput,
			"ReactorEnergyStorageCapacity":  FusionReactorCapacity,
			"ReactorFuelUnitsForFullCharge": FusionFuelUnits,
		},
	)

	// harmonic
	HarmonicReactorCapacity       = MakeScaledFuncLevelFunc(MedReactorCapacityRatio, HarmonicReactorOutput)
	HarmonicEfficiency            = ReactorEfficiencyFair
	HarmonicFuelUnits             = MakeFuelUnitsLevelFunc(HarmonicEfficiency, HarmonicReactorCapacity)
	HarmonicReactorComponentStats = ComposeComponentStats(
		CaslonReactorComponentStats,
		ComponentStats{
			"ReactorEnergyOutputPerSecond":  HarmonicReactorOutput,
			"ReactorEnergyStorageCapacity":  HarmonicReactorCapacity,
			"ReactorFuelUnitsForFullCharge": HarmonicFuelUnits,
		},
	)

	// plasmatic
	PlasmaticReactorCapacity       = MakeScaledFuncLevelFunc(LowReactorCapacityRatio, PlasmaticReactorOutput)
	PlasmaticEfficiency            = ReactorEfficiencyPoor
	PlasmaticFuelUnits             = MakeFuelUnitsLevelFunc(PlasmaticEfficiency, PlasmaticReactorCapacity)
	PlasmaticReactorComponentStats = ComposeComponentStats(
		CaslonReactorComponentStats,
		ComponentStats{
			"ReactorEnergyOutputPerSecond":  PlasmaticReactorOutput,
			"ReactorEnergyStorageCapacity":  PlasmaticReactorCapacity,
			"ReactorFuelUnitsForFullCharge": PlasmaticFuelUnits,
		},
	)

	// hyper fusion
	HyperFusionReactorCapacity       = MakeScaledFuncLevelFunc(HighReactorCapacityRatio, HyperFusionReactorOutput)
	HyperFusionEfficiency            = ReactorEfficiencyGood
	HyperFusionFuelUnits             = MakeFuelUnitsLevelFunc(HyperFusionEfficiency, HyperFusionReactorCapacity)
	HyperFusionReactorComponentStats = ComposeComponentStats(
		CaslonReactorComponentStats,
		ComponentStats{
			"ReactorEnergyOutputPerSecond":  HyperFusionReactorOutput,
			"ReactorEnergyStorageCapacity":  HyperFusionReactorCapacity,
			"ReactorFuelUnitsForFullCharge": HyperFusionFuelUnits,
		},
	)

	// hyper harmonic
	HyperHarmonicReactorCapacity       = MakeScaledFuncLevelFunc(MedReactorCapacityRatio, HyperHarmonicReactorOutput)
	HyperHarmonicEfficiency            = ReactorEfficiencyFair
	HyperHarmonicFuelUnits             = MakeFuelUnitsLevelFunc(HyperHarmonicEfficiency, HyperHarmonicReactorCapacity)
	HyperHarmonicReactorComponentStats = ComposeComponentStats(
		CaslonReactorComponentStats,
		ComponentStats{
			"ReactorEnergyOutputPerSecond":  HyperHarmonicReactorOutput,
			"ReactorEnergyStorageCapacity":  HyperHarmonicReactorCapacity,
			"ReactorFuelUnitsForFullCharge": HyperHarmonicFuelUnits,
		},
	)

	// hyper plasmatic
	HyperPlasmaticReactorCapacity       = MakeScaledFuncLevelFunc(LowReactorCapacityRatio, HyperPlasmaticReactorOutput)
	HyperPlasmaticEfficiency            = ReactorEfficiencyPoor
	HyperPlasmaticFuelUnits             = MakeFuelUnitsLevelFunc(HyperPlasmaticEfficiency, HyperPlasmaticReactorCapacity)
	HyperPlasmaticReactorComponentStats = ComposeComponentStats(
		CaslonReactorComponentStats,
		ComponentStats{
			"ReactorEnergyOutputPerSecond":  HyperPlasmaticReactorOutput,
			"ReactorEnergyStorageCapacity":  HyperPlasmaticReactorCapacity,
			"ReactorFuelUnitsForFullCharge": HyperPlasmaticFuelUnits,
		},
	)

	// ultra fusion
	UltraFusionReactorCapacity       = MakeScaledFuncLevelFunc(HighReactorCapacityRatio, UltraFusionReactorOutput)
	UltraFusionEfficiency            = ReactorEfficiencyGood
	UltraFusionFuelUnits             = MakeFuelUnitsLevelFunc(UltraFusionEfficiency, UltraFusionReactorCapacity)
	UltraFusionReactorComponentStats = ComposeComponentStats(
		CaslonReactorComponentStats,
		ComponentStats{
			"ReactorEnergyOutputPerSecond":  UltraFusionReactorOutput,
			"ReactorEnergyStorageCapacity":  UltraFusionReactorCapacity,
			"ReactorFuelUnitsForFullCharge": UltraFusionFuelUnits,
		},
	)

	// zero point
	ZeroPointReactorCapacity       = MakeScaledFuncLevelFunc(MedReactorCapacityRatio, ZeroPointReactorOutput)
	ZeroPointEfficiency            = ReactorEfficiencyFair
	ZeroPointFuelUnits             = MakeFuelUnitsLevelFunc(ZeroPointEfficiency, ZeroPointReactorCapacity)
	ZeroPointReactorComponentStats = ComposeComponentStats(
		CaslonReactorComponentStats,
		ComponentStats{
			"ReactorEnergyOutputPerSecond":  ZeroPointReactorOutput,
			"ReactorEnergyStorageCapacity":  ZeroPointReactorCapacity,
			"ReactorFuelUnitsForFullCharge": ZeroPointFuelUnits,
		},
	)
)
