package algorithm

import (
	"github.com/lucky-wolf/xml-tree/xmltree"
)

func MiningEngines(folder string) (err error) {

	// load all component definition files
	j, err := LoadJobFor(folder, "ComponentDefinitions*.xml")
	if err != nil {
		return
	}

	// apply this transformation
	err = j.applyMiningEngines()
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

func (j *Job) applyMiningEngines() (err error) {

	// apply stats for each component
	err = j.ApplyComponentAll(MiningEngineData)

	return
}

const (
	MiningEngineSizeSmall = 50
	MiningEngineSizeLarge = 100
)

var (
	MiningEngineData = ComponentLevelDataMap{
		"Mining Engine, Small": {
			values:         SmallMiningEngine,
			minLevel:       0,
			maxLevel:       6,
			componentStats: MiningEngineStatsSmall,
		},
		"Mining Engine, Large": {
			values:         LargeMiningEngine,
			minLevel:       0,
			maxLevel:       6,
			componentStats: MiningEngineStatsLarge,
		},
	}

	SmallMiningEngine = SimpleValuesTable{
		"Size": xmltree.CreateInt(MiningEngineSizeSmall),
	}
	LargeMiningEngine = SimpleValuesTable{
		"Size": xmltree.CreateInt(MiningEngineSizeLarge),
	}

	MiningEngineExtractionRate = MakeLinearLevelFunc(6, 1)

	MiningEngineStatsSmall = ComponentStats{
		"ComponentIonDefense":     StandardComponentIonDefense,
		"CrewRequirement":         MakeFixedLevelFunc(10),
		"StaticEnergyUsed":        MakeFixedLevelFunc(10),
		"ExtractionRate":          MiningEngineExtractionRate,
		"ExtractionRangeAsteroid": MakeFixedLevelFunc(0),
	}

	MiningEngineStatsLarge = ComponentStats{
		"ComponentIonDefense":     StandardComponentIonDefense,
		"CrewRequirement":         MakeFixedLevelFunc(20),
		"StaticEnergyUsed":        MakeFixedLevelFunc(20),
		"ExtractionRate":          MakeScaledFuncLevelFunc(2, MiningEngineExtractionRate),
		"ExtractionRangeAsteroid": MakeFixedLevelFunc(10000), // some rare asteroid fields are massive
	}
)
