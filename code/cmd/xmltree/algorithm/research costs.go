package algorithm

import (
	"fmt"
	"strings"
)

func ResearchCosts(folder string) (err error) {

	// load all research definition files
	j, err := LoadJobFor(folder, "ResearchProjectDefinitions*.xml")
	if err != nil {
		return
	}

	// apply this transformation
	err = j.applyResearchCosts()
	if err != nil {
		return
	}

	// save them all
	j.Save()

	return
}

// Research Costs         T0   T1   T2   T3    T4    T5     T6     T7     T8      T9     T10  T11(R)  T12(S)
// x3...x2                x3   x3   x3   x3    x3    x2     x2     x2   x2.5    x2.5    x2.5      x1  ~t7.5
// var ResearchSizes = []int{33, 100, 300, 900, 2700, 5400, 10800, 21600, 54000, 135000, 337500, 337500, 37800}

// Initiation costs                         T0   T1   T2   T3    T4    T5    T6    T7     T8     T9    T10   T11*   T12s (~t7.5)
var ResearchInitiationResourceCosts = []int{33, 100, 300, 600, 1200, 2400, 4800, 9600, 19200, 38400, 76800, 76800, 14400}

// Research costs         T0  T1  T2   T3   T4    T5    T6     T7     T8      T9      T10     T11*   T12s (~t7.5)
// var ResearchSizes = []int{17, 50, 150, 450, 1350, 2700, 5400, 10800, 21600, 43200, 86400, 86400, 8100}
var ResearchSizes = []int{17, 50, 150, 450, 1350, 4050, 12150, 36450, 109350, 328050, 328050, 328050, 72900}

// var ResearchSizes = []int{18, 55, 167, 500, 1500, 4500, 13500, 40500, 121500, 364500, 364500, 364500, 81000}

func (j *Job) applyResearchCosts() (err error) {

	fmt.Println("Research size, expense, and resource costs will be scaled by tech level (column)")

	for _, f := range j.xfiles {

		statistics := &f.stats

		// the root will result in a single ArrayOf[RootObjectType]
		for _, e := range f.root.Elements.Elements() {

			err = assertIs(e, "ArrayOfResearchProjectDefinition")
			if err != nil {
				return
			}

			for _, e := range e.Elements() {

				// each of these is a researchprojectdefinition
				err = assertIs(e, "ResearchProjectDefinition")
				if err != nil {
					return
				}

				// get our nominal column
				col := e.Child("Column").IntValue()
				if col >= len(ResearchSizes) {
					err = fmt.Errorf("Column %d exceeds maximum: %s", col, e.Child("Name").StringValue())
					return
				}

				// get the tech name
				techName := e.Child("Name").StringValue()
				if !Quiet {
					fmt.Println(techName)
				}

				// note: undefined has size 0 always
				var size int
				if !strings.HasPrefix(techName, "Undefined:") {
					// use that to see if there is a cost override / exception
					switch techName {
					// case "Wet Geoengineering", "Mixed Geoengineering", "Dry Geoengineering", "Hothouse Geoengineering":
					// 	col = 1
					case "Bombardment Weapons":
						col = 1
					case "Mysterious Plague":
						col = 2
					case "Guardian Vault Investigations":
						col = 2
					case "Cure Degenerate Gizureans", "Cure Shakturi Psionic Virus",
						"Puzzle Pirate Culture Research", "Shakturi Design and Behavior":
						col = 2
					case "Study Degenerate Gizureans", "Restore Gizurean Hive Mind":
						col = 3
					case "Basic Vault Systems", "Basic Vault Structures":
						col = 3
					case "Investigate Magnetic Pulses",
						"Study Shakturi Beacon", "Destroy Shakturi Beacon",
						"Unlock Vault of Concealment", "Unlock Vault of Creation",
						"Unlock Vault of Aggression", "Unlock Vault of the Shakturi Mind":
						col = 4
					default:
						if strings.HasPrefix(techName, "Guardian") {
							col = 3
						} else if strings.HasPrefix(techName, "Xeno Studies:") && col == 0 {
							col = 1
						}
					}
					size = ResearchSizes[col]
				}

				// set size from that
				e.Child("Size").SetValue(size)

				// set our initiation costs (if present)
				if col == 0 {
					e.RemoveByTag("InitiationCost")
				} else {
					cost := e.Child("InitiationCost")
					if cost != nil {
						cost.Child("Money").SetValue(ResearchSizes[col] * 5)
						resources := cost.Child("Resources")
						if resources != nil {
							for _, e := range resources.Elements() {
								amount := ScaleResourceCost(e.Child("ResourceId").IntValue(), float64(ResearchInitiationResourceCosts[col]))
								e.Child("Amount").SetValue(amount)
							}
						}
					}
				}

				statistics.changed++
				statistics.elements++
				statistics.objects++
			}
		}
	}
	err = nil
	return
}
