module lucky-wolf/DW2-XL/code

go 1.23.3

// replace github.com/lucky-wolf/xml-tree => ../../xml-tree

require github.com/gocarina/gocsv v0.0.0-20240520201108-78e41c74b4b1

require github.com/lucky-wolf/xml-tree v1.0.5

require golang.org/x/exp v0.0.0-20241108182801-04b207964beb // indirect
