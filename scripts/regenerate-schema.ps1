<# regenerates all of the xsd files #>

# simply ask dw2 to generate the needed files
#
$game = "C:\Steam\steamapps\common\Distant Worlds 2"
$source = "$game\data\schema"
$target = "$pwd"

if (Test-Path -Path "$source") {
	Write-Host "removing old schema files..."
	Remove-Item -Force -Recurse -Path "$source"
}

Write-Host "launching game to regenerate schema files..."
$app = Start-Process -FilePath "$game\DistantWorlds2.exe" -ArgumentList "--tool-mode --gen-xsd" -WorkingDirectory "$game" -PassThru
if ($? -ne $True) {
	exit $?
}

Write-Host "allowing 25s to write out the schema files"
Start-Sleep -Seconds 25

Write-Host "killing game"
$app.Kill()

if (Test-Path -Path "$target\schema") {
	Write-Host "removing old schema files..."
	Remove-Item -Force -Recurse -Path "$target\schema"
}

Write-Host "moving new schema files..."
Move-Item -Force -Path "$source" -Destination "$target" -PassThru | ForEach-Object { Write-Host $_.Name. }
if ($? -ne $True) {
	exit $?
}
Write-Host "schema copied successfully"

Write-Host "fix minOccurs=1 -> minOccurs=0"
Get-ChildItem "$target\schema" -Filter "*.xsd" |
Foreach-Object {
	(Get-Content $_.FullName).replace('minOccurs="1"', 'minOccurs="0"') | Set-Content $_.FullName
}
