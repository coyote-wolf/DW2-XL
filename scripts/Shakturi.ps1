param(
	[parameter(Mandatory = $false)][string]$build = "true",
	[parameter(Mandatory = $false)][string]$path = "C:\Steam\steamapps\common\Distant Worlds 2"
)

Write-Error "This script is retired in favor of manually updating our copy of ResearchProjectDefinitions_Shakturi.xml."
exit 1

# ensure we have a temp folder
if (-not (Test-Path "temp")) {
	New-Item -ItemType Directory -Force -Path "temp"
}

# this script fixes this file
$target = "XL\ResearchProjectDefinitions_Shakturi.xml"

# backup & copy the shakturi research projects
Write-Host "Copying Shakturi projects definitions from vanilla to XL"
Move-Item "$target" -Destination "temp\ResearchProjectDefinitions_Shakturi.xml.bak" -Force
Copy-Item "$path\data\ResearchProjectDefinitions_Shakturi.xml" -Destination "$target" -Force

# adjust the location of research project boxes to fit XL
Write-Host "Moving Extreme Plasma Charge above firestorm row 31 -> row 60"
.\scripts\AdjRow.ps1 31 31 +29 XL/ResearchProjectDefinitions_Shakturi.xml
Write-Host "Move Wormhole Theory row 79 -> 211"
.\scripts\AdjRow.ps1 79 79 +132 XL/ResearchProjectDefinitions_Shakturi.xml
Write-Host "Biowarfare row 175..204 -> 560..569"
.\scripts\AdjRow.ps1 175 204 +385 XL/ResearchProjectDefinitions_Shakturi.xml
Write-Host "Story techs row 270..276 -> 540..546"
.\scripts\AdjRow.ps1 270 279 +270 XL/ResearchProjectDefinitions_Shakturi.xml
Write-Host "Planet Destroyer Ships -> 295"
.\scripts\AdjRow.ps1 123 123 +172 XL/ResearchProjectDefinitions_Shakturi.xml
Write-Host "Planet Destroyer Ships -> Column 8"
.\scripts\AdjColumns.ps1 295 295 10 10 -2 XL/ResearchProjectDefinitions_Shakturi.xml

# rebuild everything
if ($build -ne "false") {
	Write-Host "Rebuilding All"
	scripts\xmltree.ps1 All
	if ($? -ne $True) {
		exit 1
	}
}
