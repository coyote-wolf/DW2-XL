<# pushes this project to the dw2/mods/xl folder and then runs dw2 #>
param(
	[parameter(Mandatory = $false)][string]$beta = "true",
	[parameter(Mandatory = $false)][string]$build = "true",
	[parameter(Mandatory = $false)][string]$launch = "",
	[parameter(Mandatory = $false)][string]$path = "C:\Users\steve\Downloads\DW2 Unstable"
)

.\scripts\test.ps1 -beta $beta -build $build -launch $launch -path $path