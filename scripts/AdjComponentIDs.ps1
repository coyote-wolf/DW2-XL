# usage:
# ./AdjComponentIDs.ps1 start end offset
param(
	[parameter(Mandatory = $true)][int]$startid,
	[parameter(Mandatory = $true)][int]$endid,
	[parameter(Mandatory = $true)][int]$offset
)

# ensure we have a temp folder
if (-not (Test-Path "temp")) {
	New-Item -ItemType Directory -Force -Path "temp"
}

# update the components themselves
$files = Get-ChildItem "XL/ComponentDefinitions*.xml"
foreach ($Item in $files) {
	$Item = $Item.Name
	$target = "XL/$Item"
	$source = "temp/$Item"
	Copy-Item $target $source
	perl "scripts/AdjustComponentId.pl" $source $startid $endid $offset $target
}

# update research references to those components
# hack: because I'm unskilled with powershell, we'll do the ugly but workable thing
# todo: really need a way to combine the list of files up-front
# todo: or a way to call a sub-function to do the work so we're not repeating ourselves!
$files = Get-ChildItem "XL/ResearchProjectDefinitions*.xml"
foreach ($Item in $files) {
	$Item = $Item.Name
	$target = "XL/$Item"
	$source = "temp/$Item"
	Copy-Item $target $source
	perl "scripts/AdjustComponentId.pl" $source $startid $endid $offset $target
}
