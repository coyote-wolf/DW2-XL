# grab all research files that we'll adj
param(
	[parameter(Mandatory = $true)][int]$startid,
	[parameter(Mandatory = $true)][int]$endid,
	[parameter(Mandatory = $true)][int]$offset
)

# ensure we have a temp folder
if (-not (Test-Path "temp")) {
	New-Item -ItemType Directory -Force -Path "temp"
}

$files = Get-ChildItem "XL/ResearchProjectDefinitions*.xml"
foreach ($Item in $files) {
	$Item = $Item.Name
	$target = "XL/$Item"
	$source = "temp/$Item"
	Copy-Item $target $source
	perl "scripts/AdjustResearchId.pl" $source $startid $endid $offset $target
}
