# require some parameters for this
param(
	[parameter(Mandatory = $true)][string]$filenamepattern,
	[parameter(Mandatory = $true)][string]$key,
	[parameter(Mandatory = $true)][float]$min,
	[parameter(Mandatory = $true)][float]$max,
	[parameter(Mandatory = $true)][float]$scale
)

# ensure we have a temp folder
if (-not (Test-Path "temp")) {
	New-Item -ItemType Directory -Force -Path "temp"
}

$files = Get-ChildItem "XL/${filenamepattern}.xml"
foreach ($Item in $files) {
	$Item = $Item.Name
	$target = "XL/$Item"
	$source = "temp/$Item"
	# Write-Host $target $source
	Copy-Item $target $source
	perl "scripts/ScaleNumericValue.pl" $source $key $min $max $scale $target
}
