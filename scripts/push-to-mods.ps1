<# pushes this project to the dw2/mods/xl folder #>
param(
	[parameter(Mandatory = $false)][string]$beta="false"
)

# we have to use a different folder 'cause that's what's registered to steam
if ($beta -eq "true") {
	$name = "XL Beta"
} elseif ($beta -eq "false") {
	$name = "XL"
} else {
	Write-Host "you must specify -beta true | false"
	exit 1
}

$source = "$pwd\XL"
$target = "C:\Steam\steamapps\common\Distant Worlds 2\mods\$name"

if (Test-Path "$target.bak.bak") {
	Write-Host "removing old backups from $target.bak.bak..."
	Remove-Item -Force -Recurse -Path "$target.bak.bak"
	if ($? -ne $True) {
		Write-Host "Failed to remove old backups: exiting"
		exit 1
	}
	Get-ChildItem "C:\Steam\steamapps\common\Distant Worlds 2\mods\XL*"
}

if (Test-Path "$target.bak") {
	Write-Host "moving old backups from .bak to .bak.bak ..."
	Rename-Item -Path "$target.bak" -NewName "$target.bak.bak"
	if ($? -ne $True) {
		Write-Host "Failed to remove old backups: exiting"
		exit 1
	}
	Get-ChildItem "C:\Steam\steamapps\common\Distant Worlds 2\mods\XL*"
}

if (Test-Path "$target") {
	Rename-Item -Path "$target" -NewName "$target.bak"
	if ($? -ne $True) {
		Write-Host "Failed to backup: exiting"
		exit 1
	}
	Write-Host "backed up to $target.bak..."
	Get-ChildItem "C:\Steam\steamapps\common\Distant Worlds 2\mods\XL*"
}

Write-Host "copying mod to $target"
Copy-Item -Force -Recurse -Container -Path "$source\" -Destination "$target\"
#  -PassThru | ForEach-Object { Write-Host $_.Name. }
if ($? -ne $True) {
	Write-Host "Copy Failure: exiting"
	exit 1
}

if ($beta -eq "true") {
	Move-Item -Force -Path "$target\beta.json" "$target\mod.json"
}

Get-ChildItem "C:\Steam\steamapps\common\Distant Worlds 2\mods\XL*"
Write-Host "successfully updated $target"

exit 0