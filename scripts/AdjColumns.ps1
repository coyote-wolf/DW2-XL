# grab all research files that we'll adj
param(
	[parameter(Mandatory = $true)][int]$rowstart,
	[parameter(Mandatory = $true)][int]$rowend,
	[parameter(Mandatory = $true)][int]$colstart,
	[parameter(Mandatory = $true)][int]$colend,
	[parameter(Mandatory = $true)][int]$offset,
	[parameter(Mandatory = $false)][string]$filter = "XL\ResearchProjectDefinitions*.xml"
)

# ensure we have a temp folder
if (-not (Test-Path "temp")) {
	New-Item -ItemType Directory -Force -Path "temp"
}

$files = Get-ChildItem $filter
foreach ($Item in $files) {
	$Item = $Item.Name
	$target = "XL/$Item"
	$source = "temp/$Item"
	Copy-Item $target $source
	# input:	source-filename rowstart rowend colsstart coleend coffset output-filename
	perl "scripts/AdjustCol.pl" $source $rowstart $rowend $colstart $colend $offset $target
}
