# POWERSHELL IS A HOT MESS
# can't really capture and forward args
# better to just call the exe yourself
# as this thing totally fucks it all up

# require some parameters for this
param(
	[parameter()][string]$algorithm="all",
	[parameter()][double]$scale=1.0,
	[parameter()][string]$scope="Star",
	[parameter()][string]$ids="all"
)

# note: this is global, not just this file...
# Set-PSDebug -Trace 1

# ensure we have a temp folder
if (-not (Test-Path "temp")) {
	New-Item -ItemType Directory -Force -Path "temp"
}

# ensure our code is compiled
scripts\make.ps1
if ($? -ne $True) {
	exit 1
}

# allow shorthand for all habitable worlds
if ($scope -eq "all") {
	$scope = "-"
}

# allow shorthand for all habitable worlds
if ($ids -eq "all") {
	$ids = "7,8,9,10,11,12,17,18,19,20,21,22,23,27,29,30"
}

# run it and pass through all arguments
Write-Host "bin/xmltree.exe -algorithm $algorithm -scale $scale -scope $scope -ids $ids"
bin/xmltree.exe -algorithm $algorithm -scale $scale -scope $scope -ids $ids

if ($LastExitCode -ne 0) {
	Write-Host "xmltree failed: $LastExitCode"
	exit $LastExitCode
}
