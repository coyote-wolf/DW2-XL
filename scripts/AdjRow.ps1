# grab all research files that we'll adj
param(
	[parameter(Mandatory = $true)][int]$start,
	[parameter(Mandatory = $true)][int]$end,
	[parameter(Mandatory = $true)][int]$offset,
	[parameter(Mandatory = $false)][string]$filter = "XL\ResearchProjectDefinitions*.xml"
)

# ensure we have a temp folder
if (-not (Test-Path "temp")) {
	New-Item -ItemType Directory -Force -Path "temp"
}

$files = Get-ChildItem $filter
foreach ($Item in $files) {
	$Item = $Item.Name
	$target = "XL/$Item"
	$source = "temp/$Item"
	Copy-Item $target $source
	perl "scripts/AdjustRow.pl" $source $start $end $offset $target
}
