<# pushes this project to the dw2/mods/xl folder and then runs dw2 #>
param(
	[parameter(Mandatory = $false)][string]$beta = "false",
	[parameter(Mandatory = $false)][string]$build = "false",
	[parameter(Mandatory = $false)][string]$launch = "",
	[parameter(Mandatory = $false)][string]$path = "C:\Steam\steamapps\common\Distant Worlds 2",
	[parameter(Mandatory = $false)][string]$copy = "true"
)

# rebuild everything
if ($build -ne "false") {
	Write-Host "Rebuilding All"
	scripts\xmltree.ps1 All
	if ($? -ne $True) {
		exit 1
	}
}

# push to mod folder
if ($copy -eq "true") {
	Write-Host "Pushing to Mods"
	scripts\push-to-mods.ps1 -beta $beta
	if ($? -ne $True) {
		exit 1
	}
}

Write-Host "Backing up GameSettings"
Remove-Item "$path\data\deleteGameSettings" -ErrorAction SilentlyContinue
Copy-Item "$path\data\GameSettings.bak" "$path\data\GameSettings.bak.bak"
Copy-Item "$path\data\GameStartSettings.bak" "$path\data\GameStartSettings.bak.bak"
Copy-Item "$path\data\GameSettings" "$path\data\GameSettings.bak"
Copy-Item "$path\data\GameStartSettings" "$path\data\GameStartSettings.bak"
Get-ChildItem "$path\data\GameS*"

#  "--new-game"
#  "--gen-xsd"
if ($launch -ne "") {
	Start-Process -FilePath "$path\DistantWorlds2.exe" "$launch" -WorkingDirectory "$path"
} else {
	Start-Process -FilePath "$path\DistantWorlds2.exe" -WorkingDirectory "$path"
}
Write-Host "launching $path..."
