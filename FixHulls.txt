// literal replacement
// ./code/cmd/replace/main.go -script FixHulls.txt -target XL/ShipHulls_<race>.xml
// whole line comments and blank lines are ignored
// NOTE: we no longer need this at all - ship hulls.go and fighter hulls.go will handle all naming for us

// a helpful idea is to use Extract Ship Hulls after adding the ShipHull*.xml for the new race(s)
// this gives you an updated ShipHulls.csv and all changes will flag what may be wrong / off

// example:
<Name>Small Colony Ship</Name>		<Name>Colonizer, Early</Name>
